package com.top.top12user.FCM;//package com.smart.tatweer.multisport.fcm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.top.top12user.Pereferences.LanguagePreferences;
import com.top.top12user.Pereferences.SharedPrefManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.greenrobot.eventbus.EventBus;

//import com.aait.productstation.models.BaseResponse;


/**
 *created by ahmed el_sayed 1/11/2018
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "FCM Service";

    SharedPrefManager sharedPrefManager;

    LanguagePreferences mLanguagePrefManager;

    String api_key;


    @Override
    // refresh device token if the session got expired
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        Log.e(TAG, "sendRegistrationToServer: " + refreshedToken);

        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        mLanguagePrefManager = new LanguagePreferences(getApplicationContext());
        sendRegistrationToServer(refreshedToken);
        Log.e("eee",refreshedToken);
        EventBus.getDefault().post(refreshedToken);
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    // send token to our server
    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        UpdateDeviceToken(token);
    }

    // store device token in shred prefrence
    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }

    public static String getToken(Context context) {
       // FirebaseApp.initializeApp(context);

        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        SharedPreferences sharedPreferences = context.getSharedPreferences(Config.SHARED_PREF, 0);
        return sharedPreferences.getString("regId", "").equals("") ? deviceToken
                : sharedPreferences.getString("regId", "");
    }

    // Update device token
    private void UpdateDeviceToken(String devcie_token) {
        if (sharedPrefManager.getLoginStatus() != null) {
            if (sharedPrefManager.getLoginStatus() == true) {
//                updateDeviceToken(mLanguagePrefManager.getAppLanguage(),
//                        sharedPrefManager.getUserData().getData().getJwt_token(),
//                        devcie_token);
            }
        }
    }

    //  networking method to update device token
    void updateDeviceToken(String lang, String apiToken, final String deviceToken) {
//        RetroWeb.getClient().create(ServiceApi.class).updateToken(lang, apiToken, deviceToken)
//                .enqueue(new retrofit2.Callback<BaseResponse>() {
//                    @Override
//                    public void onResponse(Call<BaseResponse> call,
//                            Response<BaseResponse> response) {
//                        if (response.isSuccessful()) {
//                            CommonUtil.PrintLogE("Android Token server update :" + deviceToken);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<BaseResponse> call, Throwable t) {
//                        t.printStackTrace();
//                    }
//                });
    }

}