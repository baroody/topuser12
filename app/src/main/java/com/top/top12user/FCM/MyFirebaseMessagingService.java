package com.top.top12user.FCM;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;


import com.top.top12user.Listeners.AddSearchClicked;
import com.top.top12user.Models.OrderModel;
import com.top.top12user.Pereferences.LanguagePreferences;
import com.top.top12user.Pereferences.SharedPrefManager;
import com.top.top12user.R;
import com.top.top12user.UI.Activities.ChatsActivity;
import com.top.top12user.UI.Activities.NotificationActivity;
import com.top.top12user.Utils.AudioPlayer;
import com.top.top12user.Utils.CommonUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import me.leolin.shortcutbadger.ShortcutBadger;


/**
 *created by ahmed el_sayed 1/11/2018
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    SharedPreferences sharedPreferences;

    private static final String TAG = "FCM Messaging";
    public static int count = 0;

    String notificationTitle;

    String notificationMessage;

    String notificationData;

    SharedPrefManager mSharedPrefManager;

    String notificationType;

    OrderModel mOrderModel;

    LanguagePreferences mLanguagePrefManager;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        sharedPreferences = getSharedPreferences("home", MODE_PRIVATE);

        Log.e("gghhh0",new Gson().toJson(remoteMessage.getData()));


//        CommonUtil.onPrintLog(remoteMessage.getNotification());
//        CommonUtil.onPrintLog(remoteMessage.getData());
        mSharedPrefManager = new SharedPrefManager(getApplicationContext());
        mLanguagePrefManager = new LanguagePreferences(getApplicationContext());

        if (!mSharedPrefManager.getNotificationStatus()) {
            return;
        }

        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.play(getApplicationContext(), R.raw.notification);

        notificationType = remoteMessage.getData().get("type");
        notificationTitle = remoteMessage.getData().get("title");
        notificationMessage = remoteMessage.getData().get("description");
        notificationType = remoteMessage.getData().get("notification_type");
        // if the notification contains data payload
        if (remoteMessage == null) {
            return;
        }

        EventBus.getDefault().post("reload order");
        // if the user not logged in never do any thing
        if (!mSharedPrefManager.getLoginStatus()) {
            return;
        } else {

            // notificationData = remoteMessage.getData().get("data");
            CommonUtil.PrintLogE("Data : " + notificationData);
            mOrderModel = new OrderModel();
            CommonUtil.onPrintLog(mOrderModel);
            Intent mIntent = null;
            Bundle mBundle = new Bundle();
            if (notificationType.equals("chat")) {

                Log.e("qqqq",sharedPreferences.getString("senderID","0"));
                //Log.e("www",remoteMessage.getData().get("user"));
                Log.e("sss",remoteMessage.getData().get("notification_type")+"");

                if (sharedPreferences.getString("senderID","0").equals(remoteMessage.getData().get("user"))) {
                    Intent intent = new Intent("new_message");
                    intent.putExtra("msg", remoteMessage.getData().get("msg"));
                    Log.e("msg",remoteMessage.getData().get("msg"));
                    intent.putExtra("avatar", remoteMessage.getData().get("avatar"));
                    Log.e("avatar",remoteMessage.getData().get("avatar"));
                    intent.putExtra("user", remoteMessage.getData().get("user"));
                    Log.e("user",remoteMessage.getData().get("user"));
                    intent.putExtra("type", remoteMessage.getData().get("type"));
                    Log.e("type",remoteMessage.getData().get("type"));
                    intent.putExtra("sent_at", remoteMessage.getData().get("sent_at"));
                    Log.e("sent_at",remoteMessage.getData().get("sent_at"));
                    intent.putExtra("id", remoteMessage.getData().get("id"));
                    Log.e("id",remoteMessage.getData().get("id"));
                    intent.putExtra("order", remoteMessage.getData().get("order"));
                    Log.e("order",remoteMessage.getData().get("order"));
                    intent.putExtra("username", remoteMessage.getData().get("username"));
                    Log.e("username",remoteMessage.getData().get("username"));
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                } else {
                    Intent intent = new Intent(this, ChatsActivity.class);
                    intent.putExtra("notification", "notification");
                    showNotification(remoteMessage, intent);


                }
            }else if (notificationType.equals("notification")){
                count = Integer.parseInt(remoteMessage.getData().get("notification_count"));
                ShortcutBadger.applyCount(getApplicationContext(), Integer.valueOf(remoteMessage.getData().get("notification_count"))); //for 1.1.4+
                Intent intent = new Intent(this, NotificationActivity.class);
                intent.putExtra("notification", "notification");
                showNotification(remoteMessage, intent);
            }

        }
    }


    //}

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     */

    private void handleNotification(String notificationTitle, String notificationMessage, Intent mIntent) {
        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.play(getApplicationContext(), R.raw.notification);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils
                .showNotificationMessage(notificationTitle, notificationMessage, System.currentTimeMillis(), mIntent);
    }

    private void showNotification(RemoteMessage message, Intent intent) {

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder myNotification = new NotificationCompat.Builder(this)
                .setContentTitle(message.getData().get("title"))
                .setContentText(message.getData().get("msg"))
                .setTicker("Notification!")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.logo);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.DKGRAY);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            myNotification.setChannelId("10001");
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(0 /* Request Code */, myNotification.build());
    }
}



