package com.top.top12user.Models;

public class ShopDetailsResponse extends BaseResponse {
    private ShopProductsModel data;

    public ShopProductsModel getData() {
        return data;
    }

    public void setData(ShopProductsModel data) {
        this.data = data;
    }
}
