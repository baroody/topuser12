package com.top.top12user.Models;

public class BasketResponse extends BaseResponse {
    private CartResponse data;

    public CartResponse getData() {
        return data;
    }

    public void setData(CartResponse data) {
        this.data = data;
    }
}
