package com.top.top12user.Models;

import java.util.ArrayList;

public class HeaderResponse extends BaseResponse {
    ArrayList<String> data;

    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }
}
