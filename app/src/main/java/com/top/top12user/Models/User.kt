package com.top.top12user.Models

import java.io.Serializable

data class User(
    val active: Int,
    val address: Any,
    val avatar: String,
    val banned: String,
    val checked: String,
    val city_id: Int,
    val civil_number: Any,
    val code: Any,
    val created_at: String,
    val description: Any,
    val e_wallet: String,
    val email: String,
    val id: Int,
    val ind: String,
    val lang: String,
    val lat: String,
    val long: String,
    val name: String,
    val nationality_id: Int,
    val phone: String,
    val type: String,
    val updated_at: String
):Serializable