package com.top.top12user.Models;

public class ForgotPassResponse extends BaseResponse {
    private ForgotPassModel data;

    public ForgotPassModel getData() {
        return data;
    }

    public void setData(ForgotPassModel data) {
        this.data = data;
    }
}
