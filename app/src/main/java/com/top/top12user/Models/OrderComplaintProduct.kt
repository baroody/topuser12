package com.top.top12user.Models

import java.io.Serializable

data class OrderComplaintProduct(
        val created_at: String?,
        val id: Int?,
        val order_compaint_id: Int?,
        val price: String?,
        val product: Product,
        val product_id: Int?,
        var is_checked:Boolean,
        val productImage:String?,
        val quantity:String?,
        val order_user_city:String?
):Serializable