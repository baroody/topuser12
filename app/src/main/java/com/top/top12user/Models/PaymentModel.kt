package com.top.top12user.Models

data class PaymentModel(
    val amount: Double,
    val api_version: String,
    val card_threeDSecure: Boolean,
    val currency: String,
    val customer: Customer,
    val description: String,
    val id: String,
    val live_mode: Boolean,
    val metadata: Metadata,
    val method: String,
    val `object`: String,
    val post: Post,
    val receipt: Receipt,
    val redirect: Redirect,
    val reference: Reference,
    val response: Response,
    val save_card: Boolean,
    val source: Source,
    val statement_descriptor: String,
    val status: String,
    val threeDSecure: Boolean,
    val transaction: Transaction
)