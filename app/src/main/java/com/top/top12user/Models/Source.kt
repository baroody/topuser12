package com.top.top12user.Models

data class Source(
    val id: String,
    val `object`: String
)