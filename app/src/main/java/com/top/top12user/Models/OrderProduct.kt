package com.top.top12user.Models

data class OrderProduct(
    val product_count: Int,
    val product_id: Int,
    val product_image: String,
    val product_name: String,
    val product_price: Int
)