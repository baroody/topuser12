package com.top.top12user.Models

data class Response(
    val code: String,
    val message: String
)