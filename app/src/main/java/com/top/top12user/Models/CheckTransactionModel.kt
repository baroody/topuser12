package com.top.top12user.Models

data class CheckTransactionModel(
    val transaction_id: String?,
    val transaction_status: String?
)