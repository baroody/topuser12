package com.top.top12user.Models;

public class AppInfoResponse extends BaseResponse {
    private AppInfoModel data;

    public AppInfoModel getData() {
        return data;
    }

    public void setData(AppInfoModel data) {
        this.data = data;
    }
}
