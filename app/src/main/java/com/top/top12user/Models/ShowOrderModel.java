package com.top.top12user.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowOrderModel implements Serializable {
    private int order_id;
    private String shop_name;
    private String image;
    private String city_name;
    private float rate;
    private String provider_accept;
    private String provider_refused;
    public String have_delegate;
    private String delegate_accept;
    private String client_confirmed;
    private String client_refused;
    private String order_ready;
    private String delegate_received;
    private String on_way;
    public  Integer  shop_id;
    private String order_received_to_client;
    private String order_finish;
    public Long  order_created_at;
    public Long order_delivery_time;
    public String  order_failed;
    public String  order_finish_unixtimestamp;


    public ArrayList<Product> order_products;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getProvider_accept() {
        return provider_accept;
    }

    public void setProvider_accept(String provider_accept) {
        this.provider_accept = provider_accept;
    }

    public String getProvider_refused() {
        return provider_refused;
    }

    public void setProvider_refused(String provider_refused) {
        this.provider_refused = provider_refused;
    }

    public String getDelegate_accept() {
        return delegate_accept;
    }

    public void setDelegate_accept(String delegate_accept) {
        this.delegate_accept = delegate_accept;
    }

    public String getClient_confirmed() {
        return client_confirmed;
    }

    public void setClient_confirmed(String client_confirmed) {
        this.client_confirmed = client_confirmed;
    }

    public String getClient_refused() {
        return client_refused;
    }

    public void setClient_refused(String client_refused) {
        this.client_refused = client_refused;
    }

    public String getOrder_ready() {
        return order_ready;
    }

    public void setOrder_ready(String order_ready) {
        this.order_ready = order_ready;
    }

    public String getDelegate_received() {
        return delegate_received;
    }

    public void setDelegate_received(String delegate_received) {
        this.delegate_received = delegate_received;
    }

    public String getOn_way() {
        return on_way;
    }

    public void setOn_way(String on_way) {
        this.on_way = on_way;
    }

    public String getOrder_received_to_client() {
        return order_received_to_client;
    }

    public void setOrder_received_to_client(String order_received_to_client) {
        this.order_received_to_client = order_received_to_client;
    }

    public String getOrder_finish() {
        return order_finish;
    }

    public void setOrder_finish(String order_finish) {
        this.order_finish = order_finish;
    }
}
