package com.top.top12user.Models

data class WalletResponse(
    val eWalletAction: List<EWalletAction>,
    val status: Int,
    val wallet: Int
)