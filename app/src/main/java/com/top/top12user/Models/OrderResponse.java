package com.top.top12user.Models;

import java.util.ArrayList;

public class OrderResponse extends BaseResponse {
    private ArrayList<OrderModel> data;

    public ArrayList<OrderModel> getData() {
        return data;
    }

    public void setData(ArrayList<OrderModel> data) {
        this.data = data;
    }
}
