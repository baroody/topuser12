package com.top.top12user.Models;

public class ShowOrderResponse extends BaseResponse {
    private ShowOrderModel data;

    public ShowOrderModel getData() {
        return data;
    }

    public void setData(ShowOrderModel data) {
        this.data = data;
    }
}
