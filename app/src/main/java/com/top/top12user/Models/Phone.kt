package com.top.top12user.Models

data class Phone(
    val country_code: String,
    val number: String
)