package com.top.top12user.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderProductsModel implements Serializable {
    private int product_id;
    private String product_name;
    private int product_count;
    private float product_price;
    private ArrayList<OrderProductAdditions> product_additions;

    public ArrayList<OrderProductAdditions> getProduct_additions() {
        return product_additions;
    }

    public void setProduct_additions(ArrayList<OrderProductAdditions> product_additions) {
        this.product_additions = product_additions;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getProduct_count() {
        return product_count;
    }

    public void setProduct_count(int product_count) {
        this.product_count = product_count;
    }

    public float getProduct_price() {
        return product_price;
    }

    public void setProduct_price(float product_price) {
        this.product_price = product_price;
    }
}
