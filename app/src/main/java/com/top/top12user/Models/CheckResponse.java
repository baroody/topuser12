package com.top.top12user.Models;

public class CheckResponse extends BaseResponse {
    private CheckCodeModel data;

    public CheckCodeModel getData() {
        return data;
    }

    public void setData(CheckCodeModel data) {
        this.data = data;
    }
}
