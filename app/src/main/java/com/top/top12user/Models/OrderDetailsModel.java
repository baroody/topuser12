package com.top.top12user.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDetailsModel implements Serializable {
    private int order_id;
    private int order_user_id;
    private String order_user_name;
    private String order_user_avatar;
    private String order_user_phone;
    private String order_user_city;
    private String order_user_lat;
    private String order_user_lng;
    private int order_provider_id;
    private String order_provider_name;
    private String order_provider_avatar;
    private String order_provider_phone;
    private String order_provider_city;
    private String order_provider_lat;
    private String order_provider_lng;
    private int order_delegate_id;
    private String order_delegate_name;
    private String order_delegate_phone;
    private String order_delegate_city;
    private String order_delegate_lat;
    private String order_delegate_lng;
    private String order_created_at;
    private String order_status;
   public String order_delivery_price;
    private String order_delegate_price;
    private String order_have_delegate;
    private String order_refused_reason;
    private float total_price;
    private float delegate_price;
    private float disc_price;
    private float net_price;

    public float getTotal_price() {
        return total_price;
    }

    public void setTotal_price(float total_price) {
        this.total_price = total_price;
    }

    public float getDelegate_price() {
        return delegate_price;
    }

    public void setDelegate_price(float delegate_price) {
        this.delegate_price = delegate_price;
    }

    public float getDisc_price() {
        return disc_price;
    }

    public void setDisc_price(float disc_price) {
        this.disc_price = disc_price;
    }

    public float getNet_price() {
        return net_price;
    }

    public void setNet_price(float net_price) {
        this.net_price = net_price;
    }

    private ArrayList<OrderProductsModel> order_products;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getOrder_user_id() {
        return order_user_id;
    }

    public void setOrder_user_id(int order_user_id) {
        this.order_user_id = order_user_id;
    }

    public String getOrder_user_name() {
        return order_user_name;
    }

    public void setOrder_user_name(String order_user_name) {
        this.order_user_name = order_user_name;
    }

    public String getOrder_user_avatar() {
        return order_user_avatar;
    }

    public void setOrder_user_avatar(String order_user_avatar) {
        this.order_user_avatar = order_user_avatar;
    }

    public String getOrder_user_phone() {
        return order_user_phone;
    }

    public void setOrder_user_phone(String order_user_phone) {
        this.order_user_phone = order_user_phone;
    }

    public String getOrder_user_city() {
        return order_user_city;
    }

    public void setOrder_user_city(String order_user_city) {
        this.order_user_city = order_user_city;
    }

    public String getOrder_user_lat() {
        return order_user_lat;
    }

    public void setOrder_user_lat(String order_user_lat) {
        this.order_user_lat = order_user_lat;
    }

    public String getOrder_user_lng() {
        return order_user_lng;
    }

    public void setOrder_user_lng(String order_user_lng) {
        this.order_user_lng = order_user_lng;
    }

    public int getOrder_provider_id() {
        return order_provider_id;
    }

    public void setOrder_provider_id(int order_provider_id) {
        this.order_provider_id = order_provider_id;
    }

    public String getOrder_provider_name() {
        return order_provider_name;
    }

    public void setOrder_provider_name(String order_provider_name) {
        this.order_provider_name = order_provider_name;
    }

    public String getOrder_provider_avatar() {
        return order_provider_avatar;
    }

    public void setOrder_provider_avatar(String order_provider_avatar) {
        this.order_provider_avatar = order_provider_avatar;
    }

    public String getOrder_provider_phone() {
        return order_provider_phone;
    }

    public void setOrder_provider_phone(String order_provider_phone) {
        this.order_provider_phone = order_provider_phone;
    }

    public String getOrder_provider_city() {
        return order_provider_city;
    }

    public void setOrder_provider_city(String order_provider_city) {
        this.order_provider_city = order_provider_city;
    }

    public String getOrder_provider_lat() {
        return order_provider_lat;
    }

    public void setOrder_provider_lat(String order_provider_lat) {
        this.order_provider_lat = order_provider_lat;
    }

    public String getOrder_provider_lng() {
        return order_provider_lng;
    }

    public void setOrder_provider_lng(String order_provider_lng) {
        this.order_provider_lng = order_provider_lng;
    }

    public int getOrder_delegate_id() {
        return order_delegate_id;
    }

    public void setOrder_delegate_id(int order_delegate_id) {
        this.order_delegate_id = order_delegate_id;
    }

    public String getOrder_delegate_name() {
        return order_delegate_name;
    }

    public void setOrder_delegate_name(String order_delegate_name) {
        this.order_delegate_name = order_delegate_name;
    }

    public String getOrder_delegate_phone() {
        return order_delegate_phone;
    }

    public void setOrder_delegate_phone(String order_delegate_phone) {
        this.order_delegate_phone = order_delegate_phone;
    }

    public String getOrder_delegate_city() {
        return order_delegate_city;
    }

    public void setOrder_delegate_city(String order_delegate_city) {
        this.order_delegate_city = order_delegate_city;
    }

    public String getOrder_delegate_lat() {
        return order_delegate_lat;
    }

    public void setOrder_delegate_lat(String order_delegate_lat) {
        this.order_delegate_lat = order_delegate_lat;
    }

    public String getOrder_delegate_lng() {
        return order_delegate_lng;
    }

    public void setOrder_delegate_lng(String order_delegate_lng) {
        this.order_delegate_lng = order_delegate_lng;
    }

    public String getOrder_created_at() {
        return order_created_at;
    }

    public void setOrder_created_at(String order_created_at) {
        this.order_created_at = order_created_at;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_delegate_price() {
        return order_delegate_price;
    }

    public void setOrder_delegate_price(String order_delegate_price) {
        this.order_delegate_price = order_delegate_price;
    }

    public String getOrder_have_delegate() {
        return order_have_delegate;
    }

    public void setOrder_have_delegate(String order_have_delegate) {
        this.order_have_delegate = order_have_delegate;
    }

    public String getOrder_refused_reason() {
        return order_refused_reason;
    }

    public void setOrder_refused_reason(String order_refused_reason) {
        this.order_refused_reason = order_refused_reason;
    }

    public ArrayList<OrderProductsModel> getOrder_products() {
        return order_products;
    }

    public void setOrder_products(ArrayList<OrderProductsModel> order_products) {
        this.order_products = order_products;
    }
}
