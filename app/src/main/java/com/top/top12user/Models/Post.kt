package com.top.top12user.Models

data class Post(
    val status: String,
    val url: String
)