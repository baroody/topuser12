package com.top.top12user.Models

import java.io.Serializable

data class ComplaintModel(
    val complaintId: Int,
    val createdAt: String,
    val description: String,
    val description_detail: String,
    val orderId:String,
    val orderComplaintImages: ArrayList<String>,
    val orderComplaintProduct: ArrayList<OrderComplaintProduct>,
    val provider: Provider,
    val status: String,
    val user: User
):Serializable