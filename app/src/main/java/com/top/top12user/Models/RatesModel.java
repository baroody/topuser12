package com.top.top12user.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class RatesModel implements Serializable {
    private int user_rate_before;
    private String user_rate;
    private String shop_rate;
    private ArrayList<CommentsModel> comments;

    public int getUser_rate_before() {
        return user_rate_before;
    }

    public void setUser_rate_before(int user_rate_before) {
        this.user_rate_before = user_rate_before;
    }

    public String getUser_rate() {
        return user_rate;
    }

    public void setUser_rate(String user_rate) {
        this.user_rate = user_rate;
    }

    public String getShop_rate() {
        return shop_rate;
    }

    public void setShop_rate(String shop_rate) {
        this.shop_rate = shop_rate;
    }

    public ArrayList<CommentsModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentsModel> comments) {
        this.comments = comments;
    }
}
