package com.top.top12user.Models

data class Expiry(
    val period: Int,
    val type: String
)