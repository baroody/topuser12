package com.top.top12user.Models

import java.io.Serializable

data class Product(
    val category_id: Int,
    val created_at: String,
    val disc: String,
    val enough_to: String,
    val extra_info: Any,
    val id: Int,
    val is_active: Int,
    val name_ar: String,
    val name_en: String,
    val offer_price: Any,
    val order_type: String,
    val price: String,
    val shop_id: Int,
    val time: String,
    val updated_at: String,
    val url :String,
    val quantity:Int
):Serializable