package com.top.top12user.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CartAdditions implements Serializable {
    private int cart_product_addition_id;
    @SerializedName(value = "addition_id" ,alternate = {"id"})
    private int addition_id;
    @SerializedName(value = "addition_count" ,alternate = {"count"})
    private int addition_count;
    private String addition_name;
    private float addition_price;

    public int getCart_product_addition_id() {
        return cart_product_addition_id;
    }

    public void setCart_product_addition_id(int cart_product_addition_id) {
        this.cart_product_addition_id = cart_product_addition_id;
    }

    public int getAddition_id() {
        return addition_id;
    }

    public void setAddition_id(int addition_id) {
        this.addition_id = addition_id;
    }

    public int getAddition_count() {
        return addition_count;
    }

    public void setAddition_count(int addition_count) {
        this.addition_count = addition_count;
    }

    public String getAddition_name() {
        return addition_name;
    }

    public void setAddition_name(String addition_name) {
        this.addition_name = addition_name;
    }

    public float getAddition_price() {
        return addition_price;
    }

    public void setAddition_price(float addition_price) {
        this.addition_price = addition_price;
    }
}
