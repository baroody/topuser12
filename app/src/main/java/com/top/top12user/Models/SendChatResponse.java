package com.top.top12user.Models;

public class SendChatResponse extends BaseResponse{
    private ChatModel data;

    public ChatModel getData() {
        return data;
    }

    public void setData(ChatModel data) {
        this.data = data;
    }
}
