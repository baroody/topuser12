package com.top.top12user.Models;

import java.io.Serializable;

public class CategoryModel implements Serializable {
    private int id;
    private String name;
    private String image;
    private boolean checkCategory;

    public boolean isCheckCategory() {
        return checkCategory;
    }

    public void setCheckCategory(boolean checkCategory) {
        this.checkCategory = checkCategory;
    }

    public CategoryModel(int id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
