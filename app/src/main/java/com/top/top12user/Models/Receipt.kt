package com.top.top12user.Models

data class Receipt(
    val email: Boolean,
    val sms: Boolean
)