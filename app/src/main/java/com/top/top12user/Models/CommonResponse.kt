package com.top.top12user.Models

data class CommonResponse (
        val status:Int,
        val message:String
)