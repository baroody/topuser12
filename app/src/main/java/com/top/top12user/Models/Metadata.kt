package com.top.top12user.Models

data class Metadata(
    val udf1: String,
    val udf2: String
)