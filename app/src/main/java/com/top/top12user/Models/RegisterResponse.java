package com.top.top12user.Models;

public class RegisterResponse extends BaseResponse {
    private RegisterModel data;

    public RegisterModel getData() {
        return data;
    }

    public void setData(RegisterModel data) {
        this.data = data;
    }
}
