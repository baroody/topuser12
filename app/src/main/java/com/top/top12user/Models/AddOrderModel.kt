package com.top.top12user.Models

data class AddOrderModel(
        val status: Int,
        val order_id: Int,
        val message: String
)