package com.top.top12user.Models;

import java.io.Serializable;

public class ShopProductsModel implements Serializable {
    private int product_id;
    private String product_name;
    private String product_price;
    private String product_disc;
    private boolean product_have_offer;
    private float product_offer_percent;
    private int product_order_count;
    private float product_offer_price;
    private String product_main_image;
    public boolean is_active;
    public String product_type;
    public   String   product_time;

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_disc() {
        return product_disc;
    }

    public void setProduct_disc(String product_disc) {
        this.product_disc = product_disc;
    }

    public boolean isProduct_have_offer() {
        return product_have_offer;
    }

    public void setProduct_have_offer(boolean product_have_offer) {
        this.product_have_offer = product_have_offer;
    }

    public float getProduct_offer_percent() {
        return product_offer_percent;
    }

    public void setProduct_offer_percent(float product_offer_percent) {
        this.product_offer_percent = product_offer_percent;
    }

    public int getProduct_order_count() {
        return product_order_count;
    }

    public void setProduct_order_count(int product_order_count) {
        this.product_order_count = product_order_count;
    }

    public float getProduct_offer_price() {
        return product_offer_price;
    }

    public void setProduct_offer_price(float product_offer_price) {
        this.product_offer_price = product_offer_price;
    }

    public String getProduct_main_image() {
        return product_main_image;
    }

    public void setProduct_main_image(String product_main_image) {
        this.product_main_image = product_main_image;
    }
}
