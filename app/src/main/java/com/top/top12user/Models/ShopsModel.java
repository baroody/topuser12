package com.top.top12user.Models;

import java.io.Serializable;

public class ShopsModel implements Serializable {
    private int shop_id;
    private int shop_user_id;
    private String shop_name;
    public Boolean shop_open;
    private String shop_image;
    public Boolean shop_featured;

    private int products_count;
    public Double price;
    public String rate;
    public Double shop_distance;
    public boolean have_delegate;
    public String category_name =null;


    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public int getShop_user_id() {
        return shop_user_id;
    }

    public void setShop_user_id(int shop_user_id) {
        this.shop_user_id = shop_user_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_image() {
        return shop_image;
    }

    public void setShop_image(String shop_image) {
        this.shop_image = shop_image;
    }

    public int getProducts_count() {
        return products_count;
    }

    public void setProducts_count(int products_count) {
        this.products_count = products_count;
    }
}
