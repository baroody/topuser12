package com.top.top12user.Models;

public class RateResponse extends BaseResponse {
    private RatesModel data;

    public RatesModel getData() {
        return data;
    }

    public void setData(RatesModel data) {
        this.data = data;
    }
}
