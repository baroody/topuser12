package com.top.top12user.Models;

import java.io.Serializable;

public class AdditionsModel implements Serializable {
    private int id;
    private String addition_name;
    private String price;
    private String name_ar;
    private String name_en;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }



    public String getAddition_name() {
        return addition_name;
    }

    public void setAddition_name(String addition_name) {
        this.addition_name = addition_name;
    }
}
