package com.top.top12user.Models

data class Reference(
    val order: String,
    val transaction: String
)