package com.top.top12user.Models

data class OrdersCountInProgressResponse(
    val ordersCountInProgress: String,
    val status: Int
)