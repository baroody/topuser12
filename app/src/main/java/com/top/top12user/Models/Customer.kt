package com.top.top12user.Models

data class Customer(
    val email: String,
    val first_name: String,
    val last_name: String,
    val middle_name: String,
    val phone: Phone
)