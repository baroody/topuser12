package com.top.top12user.Models;

import java.util.ArrayList;

public class CategoryResponse extends BaseResponse {
    private ArrayList<CategoryModel> data;

    public ArrayList<CategoryModel> getData() {
        return data;
    }

    public void setData(ArrayList<CategoryModel> data) {
        this.data = data;
    }
}
