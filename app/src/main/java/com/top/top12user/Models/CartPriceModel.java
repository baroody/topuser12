package com.top.top12user.Models;

import java.io.Serializable;

public class CartPriceModel implements Serializable {
    private float all_total_price;

    public float getAll_total_price() {
        return all_total_price;
    }

    public void setAll_total_price(float all_total_price) {
        this.all_total_price = all_total_price;
    }
}
