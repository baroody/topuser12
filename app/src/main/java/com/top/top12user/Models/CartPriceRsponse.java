package com.top.top12user.Models;

public class CartPriceRsponse extends BaseResponse {
    private CartPriceModel data;

    public CartPriceModel getData() {
        return data;
    }

    public void setData(CartPriceModel data) {
        this.data = data;
    }
}
