package com.top.top12user.Models

data class OrderDeliveryTime(
    val deliveryTime: String,
    val deliveryTimeFormat: String,
    val isEarly: Boolean
)