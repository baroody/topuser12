package com.top.top12user.Models;

import java.util.ArrayList;

public class GetChatResponse extends BaseResponse {
    private ArrayList<ChatModel> data;

    public ArrayList<ChatModel> getData() {
        return data;
    }

    public void setData(ArrayList<ChatModel> data) {
        this.data = data;
    }
}
