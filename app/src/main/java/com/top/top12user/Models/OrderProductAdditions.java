package com.top.top12user.Models;

import java.io.Serializable;

public class OrderProductAdditions implements Serializable {
    private int addition_id;
    private String addition_name;
    private int addition_count;
    private float addition_price;

    public int getAddition_id() {
        return addition_id;
    }

    public void setAddition_id(int addition_id) {
        this.addition_id = addition_id;
    }

    public String getAddition_name() {
        return addition_name;
    }

    public void setAddition_name(String addition_name) {
        this.addition_name = addition_name;
    }

    public int getAddition_count() {
        return addition_count;
    }

    public void setAddition_count(int addition_count) {
        this.addition_count = addition_count;
    }

    public float getAddition_price() {
        return addition_price;
    }

    public void setAddition_price(float addition_price) {
        this.addition_price = addition_price;
    }
}
