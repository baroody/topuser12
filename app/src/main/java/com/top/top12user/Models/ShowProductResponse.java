package com.top.top12user.Models;

public class ShowProductResponse extends BaseResponse {
    private ProductDetailsModel data;

    public ProductDetailsModel getData() {
        return data;
    }

    public void setData(ProductDetailsModel data) {
        this.data = data;
    }
}
