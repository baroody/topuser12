package com.top.top12user.Models

data class Redirect(
    val status: String,
    val url: String
)