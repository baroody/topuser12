package com.top.top12user.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class CartResponse implements Serializable{
    private ArrayList<CartProducts> carts;
    private float all_total_price;

    public ArrayList<CartProducts> getCarts() {
        return carts;
    }

    public void setCarts(ArrayList<CartProducts> carts) {
        this.carts = carts;
    }

    public float getAll_total_price() {
        return all_total_price;
    }

    public void setAll_total_price(float all_total_price) {
        this.all_total_price = all_total_price;
    }
}
