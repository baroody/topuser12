package com.top.top12user.Models

data class EWalletAction(
    val createAt: String,
    val id: Int,
    val orderId: Int,
    val status: String,
    val transAction: Int,
    val userId: Int,
    val isEbay:Boolean
)