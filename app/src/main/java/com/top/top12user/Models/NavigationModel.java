package com.top.top12user.Models;

/**
 *created by ahmed el_sayed 1/11/2018
 */
public class NavigationModel {
    private int natIcon;

    private String navTitle;

    public NavigationModel(String navTitle, int natIcon) {
        this.navTitle = navTitle;
        this.natIcon = natIcon;
    }

    public int getNatIcon() {
        return natIcon;
    }
    public String getNavTitle() {
        return navTitle;
    }
}
