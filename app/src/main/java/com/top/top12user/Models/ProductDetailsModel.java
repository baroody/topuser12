package com.top.top12user.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductDetailsModel implements Serializable {
    private int product_id;
    private String product_name;
    private String product_price;
    private String product_disc;
    private String product_extra_info;
    private String product_time;
    private boolean product_have_offer;
    private float product_offer_percent;
    private String product_enough_to;
    private float product_offer_price;
    private int product_category_id;
    private int product_shop_id;
    private String product_shop_name;
    private int product_user_id;
  public   String order_type;
    private String product_category_name;
    private String product_main_image;
    private ArrayList<ImagesModel> product_images;
    private ArrayList<AdditionsModel> product_additions;

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_disc() {
        return product_disc;
    }

    public void setProduct_disc(String product_disc) {
        this.product_disc = product_disc;
    }

    public String getProduct_extra_info() {
        return product_extra_info;
    }

    public void setProduct_extra_info(String product_extra_info) {
        this.product_extra_info = product_extra_info;
    }

    public String getProduct_time() {
        return product_time;
    }

    public void setProduct_time(String product_time) {
        this.product_time = product_time;
    }

    public boolean isProduct_have_offer() {
        return product_have_offer;
    }

    public void setProduct_have_offer(boolean product_have_offer) {
        this.product_have_offer = product_have_offer;
    }

    public float getProduct_offer_percent() {
        return product_offer_percent;
    }

    public void setProduct_offer_percent(float product_offer_percent) {
        this.product_offer_percent = product_offer_percent;
    }

    public String getProduct_enough_to() {
        return product_enough_to;
    }

    public void setProduct_enough_to(String product_enough_to) {
        this.product_enough_to = product_enough_to;
    }

    public float getProduct_offer_price() {
        return product_offer_price;
    }

    public void setProduct_offer_price(float product_offer_price) {
        this.product_offer_price = product_offer_price;
    }

    public int getProduct_category_id() {
        return product_category_id;
    }

    public void setProduct_category_id(int product_category_id) {
        this.product_category_id = product_category_id;
    }

    public int getProduct_shop_id() {
        return product_shop_id;
    }

    public void setProduct_shop_id(int product_shop_id) {
        this.product_shop_id = product_shop_id;
    }

    public String getProduct_shop_name() {
        return product_shop_name;
    }

    public void setProduct_shop_name(String product_shop_name) {
        this.product_shop_name = product_shop_name;
    }

    public int getProduct_user_id() {
        return product_user_id;
    }

    public void setProduct_user_id(int product_user_id) {
        this.product_user_id = product_user_id;
    }

    public String getProduct_category_name() {
        return product_category_name;
    }

    public void setProduct_category_name(String product_category_name) {
        this.product_category_name = product_category_name;
    }

    public String getProduct_main_image() {
        return product_main_image;
    }

    public void setProduct_main_image(String product_main_image) {
        this.product_main_image = product_main_image;
    }

    public ArrayList<ImagesModel> getProduct_images() {
        return product_images;
    }

    public void setProduct_images(ArrayList<ImagesModel> product_images) {
        this.product_images = product_images;
    }

    public ArrayList<AdditionsModel> getProduct_additions() {
        return product_additions;
    }

    public void setProduct_additions(ArrayList<AdditionsModel> product_additions) {
        this.product_additions = product_additions;
    }
}
