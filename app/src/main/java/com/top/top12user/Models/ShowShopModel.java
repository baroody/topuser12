package com.top.top12user.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowShopModel implements Serializable {
    private int shop_id;
    private String shop_name;
    private String shop_have_delegate;
    private int shop_order_count;
    private String shop_min_price;
    private String shop_delegate_phone;
    private String shop_work_at;
    private String shop_work_to;
    private float shop_rate;
    private int user_id;
    private String user_name;
    private String user_image;
    private String user_city;
    private ArrayList<UserCategoriesModel> user_categories;
    private ArrayList<ShopProductsModel> products;

    public String getShop_work_at() {
        return shop_work_at;
    }

    public void setShop_work_at(String shop_work_at) {
        this.shop_work_at = shop_work_at;
    }

    public String getShop_work_to() {
        return shop_work_to;
    }

    public void setShop_work_to(String shop_work_to) {
        this.shop_work_to = shop_work_to;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_have_delegate() {
        return shop_have_delegate;
    }

    public void setShop_have_delegate(String shop_have_delegate) {
        this.shop_have_delegate = shop_have_delegate;
    }

    public int getShop_order_count() {
        return shop_order_count;
    }

    public void setShop_order_count(int shop_order_count) {
        this.shop_order_count = shop_order_count;
    }

    public String getShop_min_price() {
        return shop_min_price;
    }

    public void setShop_min_price(String shop_min_price) {
        this.shop_min_price = shop_min_price;
    }

    public String getShop_delegate_phone() {
        return shop_delegate_phone;
    }

    public void setShop_delegate_phone(String shop_delegate_phone) {
        this.shop_delegate_phone = shop_delegate_phone;
    }

    public float getShop_rate() {
        return shop_rate;
    }

    public void setShop_rate(float shop_rate) {
        this.shop_rate = shop_rate;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_city() {
        return user_city;
    }

    public void setUser_city(String user_city) {
        this.user_city = user_city;
    }

    public ArrayList<UserCategoriesModel> getUser_categories() {
        return user_categories;
    }

    public void setUser_categories(ArrayList<UserCategoriesModel> user_categories) {
        this.user_categories = user_categories;
    }

    public ArrayList<ShopProductsModel> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ShopProductsModel> products) {
        this.products = products;
    }
}
