package com.top.top12user.Models

data class Transaction(
    val asynchronous: Boolean,
    val created: String,
    val expiry: Expiry,
    val timezone: String,
    val url: String
)