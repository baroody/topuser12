package com.top.top12user.UI.Fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;

import com.top.top12user.Listeners.OrderHasDelivery;
import com.top.top12user.R;

import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Mahmoud on 1/19/18.
 */

public class ChooseDeliveryDialogFragment extends DialogFragment {

    OrderHasDelivery listner;

    public ChooseDeliveryDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                listner.orderHasDelivery(true);
             //   dismiss();
            }
        }, 1500);
    }

    public void setListner(OrderHasDelivery listner) {
        this.listner = listner;
    }

    public static ChooseDeliveryDialogFragment newInstance() {
        ChooseDeliveryDialogFragment frag = new ChooseDeliveryDialogFragment();
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_choose_delivery, container);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((LayoutParams) params);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation_2);
        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.tv_ok)
    void onConfirmClick() {

         listner.orderHasDelivery(true);

        dismiss();
    }



}

