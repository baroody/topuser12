package com.top.top12user.UI.Adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.top.top12user.R;
import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.ListModel;

import java.util.List;

import butterknife.BindView;

public class ListDialogAdapter extends ParentRecyclerAdapter<ListModel> {

    public ListDialogAdapter(final Context context, final List<ListModel>  data) {
        super(context, data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.recycler_dialog_list_row_family, parent, false);
        viewHolder = new ListAdapter(viewItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        ListAdapter listAdapter = (ListAdapter) holder;
        ListModel listModel = data.get(position);
        listAdapter.tv_row_title.setText(listModel.getName());
        if (position % 2 == 0) {
            listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorWhite));
        } else {
            listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorWhite));

        }

        listAdapter.tv_row_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });
    }


    protected class ListAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_row_title)
        TextView tv_row_title;

        public ListAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }
}

