package com.top.top12user.UI.Adapters;

import android.content.Context;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.OrderModel;
import com.top.top12user.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class NewOrderAdapter extends ParentRecyclerAdapter<OrderModel> {

    private String errorMsg;

    public NewOrderAdapter(final Context context, final List<OrderModel> data) {
        super(context, data);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;

                View viewItem = inflater.inflate(R.layout.recycler_order, parent, false);
                viewHolder = new NewOrderViewHolder(viewItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {


                NewOrderViewHolder newOrderViewHolder = (NewOrderViewHolder) holder;
                OrderModel orderModel = data.get(position);
                Glide.with(mcontext).load(orderModel.getOrder_provider_avatar())
                        .into(newOrderViewHolder.ivUserImage);
                newOrderViewHolder.family_name.setText(orderModel.getOrder_provider_name());

                newOrderViewHolder.time.setText(orderModel.getOrder_created_at() + "");
                newOrderViewHolder.status.setText(orderModel.getOrder_status());
                newOrderViewHolder.product.setText(orderModel.getOrder_products_count()+mcontext.getResources().getString(R.string.products));
                if (orderModel.getOrder_status().equals("user waiting")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.order_waititng_for_confirm));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }else if (orderModel.getOrder_status().equals("provider accept")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.order_accepted));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }else if (orderModel.getOrder_status().equals("client confirmed")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.order_processed));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }else if (orderModel.getOrder_status().equals("delegate accept")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.delegate_accept));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }else if (orderModel.getOrder_status().equals("order ready")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.order_ready));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }else if (orderModel.getOrder_status().equals("delegate received")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.delegate_recive_order));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }else if (orderModel.getOrder_status().equals("on way")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.on_way));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }else if (orderModel.getOrder_status().equals("order received to client")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.confirm_deliver_order));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }else if(orderModel.getOrder_status().equals("order finish")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.finished));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorOrange));
                }
                else if(orderModel.getOrder_status().equals("client refused")){
                    newOrderViewHolder.status.setText(mcontext.getResources().getString(R.string.order_refused));
                    newOrderViewHolder.status.setTextColor(mcontext.getResources().getColor(R.color.colorOrange));
                }




    }



    @Override
    public int getItemCount() {
        return data.size();
    }


    /**
     * Main list's content ViewHolder
     */

    protected class NewOrderViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.img)
        ImageView ivUserImage;

        @BindView(R.id.family_name)
        TextView family_name;

        @BindView(R.id.product)
        TextView product;



        @BindView(R.id.status)
        TextView status;


        @BindView(R.id.time)
        TextView time;



        public NewOrderViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    itemClickListener.onItemClick(view, getPosition());
                }
            });
        }


    }





}

