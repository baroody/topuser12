package com.top.top12user.UI.Adapters;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Listeners.RefreshCart;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.CartProducts;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Activities.BasketActivity;
import com.top.top12user.UI.Activities.EditOrderItemActivity;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardBasketAdapter extends ParentRecyclerAdapter<CartProducts> {
    TextView ordersPrice;
    TextView payTotal;
    RefreshCart f =  null;
    public CardBasketAdapter(final Context context, final List<CartProducts> data, final int layoutId,
                             TextView ordersPrice, TextView payTotal, BasketActivity mActivity) {
        super(context, data, layoutId);
        this.ordersPrice=ordersPrice;
        f=(RefreshCart)mActivity;
        this.payTotal=payTotal;
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        final CartProducts basketModel = data.get(position);

        Glide.with(mcontext).load(basketModel.getProvider_image()).placeholder(R.mipmap.bguser)
                .into(viewHolder.civFamilyImage);
        viewHolder.tvFamilyName.setText(basketModel.shop_name);
        viewHolder.tv_total_coast
                .setText( basketModel.getTotal_price()+
                        mcontext.getResources().getString(R.string.Sar));
        viewHolder.familyOrderDetailsAdapter.setData(basketModel.getCart_products());
        viewHolder.familyOrderDetailsAdapter.setRowIndex(position);
        viewHolder.familyOrderDetailsAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (view.getId()==R.id.iv_edit){
                    EditOrderItemActivity.startActivity((AppCompatActivity)mcontext,basketModel.getCart_products().get(position));
                }else if (view.getId() == R.id.iv_delete){

                                    delete(basketModel.getCart_products().get(position).getCart_product_id());
                                    viewHolder.familyOrderDetailsAdapter.Delete(position);


                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {
        private FamilyOrderDetailsAdapter familyOrderDetailsAdapter;

        @BindView(R.id.civ_family_image)
        CircleImageView civFamilyImage;

        @BindView(R.id.tv_family_name)
        TextView tvFamilyName;



        @BindView(R.id.product_recycler)
        RecyclerView product_recycler;



        @BindView(R.id.tv_total_coast)
        TextView tv_total_coast;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
            product_recycler.setLayoutManager(new LinearLayoutManager(mcontext,LinearLayoutManager.VERTICAL,false));
            familyOrderDetailsAdapter = new  FamilyOrderDetailsAdapter(mcontext,ordersPrice,payTotal);
            product_recycler.setAdapter(familyOrderDetailsAdapter);
        }

    }
    private void delete(int product){
        RetroWeb.getClient().create(ServiceApi.class).deleteCart(mSharedPrefManager.getUserData().getUser_id()
                ,product).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                      //  CommonUtil.makeToast(mcontext,mcontext.getString(R.string.Done));

                        f.refreshCart();

                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

                CommonUtil.handleException(mcontext,t);
                t.printStackTrace();
            }
        });
    }
}

