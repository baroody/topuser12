package com.top.top12user.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.R;
import com.pnikosis.materialishprogress.ProgressWheel;

import butterknife.BindView;

public class ViewImageActivity extends ParentActivity {

    @BindView(R.id.progress_wheel)
    ProgressWheel progressWheel;



    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_images_preview;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}

