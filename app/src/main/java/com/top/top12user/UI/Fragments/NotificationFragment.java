package com.top.top12user.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.top.top12user.Base.BaseFragment;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.NotificationModel;
import com.top.top12user.Models.NotificationResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Activities.DeliveryPriceActivity;
import com.top.top12user.UI.Activities.ShowOrderActivity;
import com.top.top12user.UI.Adapters.NotificationAdapter;
import com.top.top12user.Utils.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends BaseFragment {
    @BindView(R.id.rv_recycle)
    RecyclerView orders_recycler;
    LinearLayoutManager linearLayoutManager;
    NotificationAdapter notificationAdapter;
    ArrayList<NotificationModel> notificationModels = new ArrayList<>();
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;

    public static NotificationFragment newInstance() {
        Bundle args = new Bundle();
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onClick(View v) {

    }
    private void getNotification(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).showNotification(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, final Response<NotificationResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }else {
                            notificationAdapter.updateAll(response.body().getData());
                            notificationAdapter.setOnItemClickListener(new OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    if ((response.body().getData().get(position).getOrder_status().equals("delegate accept")||response.body().getData().get(position).getOrder_status().equals("provider accept"))){
//                                        Intent intent1 = new Intent(getContext(), DeliveryPriceActivity.class);
//                                        intent1.putExtra("id",response.body().getData().get(position).getOrder_id()+"");
//                                        startActivity(intent1);

                                        Intent intent = new Intent(getContext(), ShowOrderActivity.class);
                                        intent.putExtra("id", response.body().getData().get(position).getOrder_id() + "");
                                        startActivity(intent);

                                    }else {
                                        Intent intent = new Intent(getContext(), ShowOrderActivity.class);
                                        intent.putExtra("id", response.body().getData().get(position).getOrder_id() + "");
                                        startActivity(intent);
                                    }
                                }
                            });
                        }
                    }else {
                        CommonUtil.makeToast(getContext(),response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                CommonUtil.handleException(getContext(),t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_notification;
    }

    @Override
    protected void initializeComponents(View view) {

        linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        notificationAdapter = new NotificationAdapter(getContext(),notificationModels,R.layout.recycler_notification);
        orders_recycler.setLayoutManager(linearLayoutManager);
        orders_recycler.setAdapter(notificationAdapter);
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNotification();
            }});
        getNotification();
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }
}
