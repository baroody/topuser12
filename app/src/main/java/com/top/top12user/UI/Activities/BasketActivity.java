package com.top.top12user.UI.Activities;

import android.content.Intent;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Listeners.HasPrior;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Listeners.OrderHasDelivery;
import com.top.top12user.Listeners.RefreshCart;
import com.top.top12user.Models.AddOrderModel;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.BasketResponse;
import com.top.top12user.Models.CartProducts;
import com.top.top12user.Models.GetChatResponse;
import com.top.top12user.Models.PaymentModel;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.Pereferences.LanguagePreferences;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.CardBasketAdapter;
import com.top.top12user.UI.Fragments.ChooseDeliveryDialogFragment;
import com.top.top12user.UI.Views.IsEarlyOrderDialog;
import com.top.top12user.UI.Views.PaymentWebviewDialog;
import com.top.top12user.UI.Views.PeriorOrderDialog;
import com.top.top12user.UI.Views.Toaster;
import com.top.top12user.Utils.CommonUtil;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BasketActivity extends ParentActivity implements OnItemClickListener,
        OrderHasDelivery, RefreshCart, HasPrior {

    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;

    @OnClick(R.id.act_back)
    void onBack() {
        onBackPressed();
    }

    @BindView(R.id.basket_recycler)
    RecyclerView basket_recycler;

    LinearLayoutManager linearLayoutManager;
    ArrayList<CartProducts> cartProducts = new ArrayList<>();
    CardBasketAdapter cardBasketAdapter;
    @BindView(R.id.btn_confirm_order)
    Button btn_confirm_order;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.delivery_price_tv)
    TextView deliveryPrice;
    @BindView(R.id.orders_tv)
    TextView ordersPrice;
    @BindView(R.id.pay_details_tv)
    TextView payTotal;
    @BindView(R.id.delivery_address_tv)
    TextView deliveryAddress;


    @BindView(R.id.online)
    TextView online;

    @BindView(R.id.payment_type_tv)
    TextView payment_type_tv;

    @BindView(R.id.wallet_tv)
    TextView wallet_tv;

    @BindView(R.id.early_value)
    TextView early_value;

    @BindView(R.id.early)
    TextView early;

    @BindView(R.id.payment_type_tx)
    TextView payment_type_tx;


    Boolean has_prior = false;
    String payment_type = "cash";
    Boolean iswallet = null;
    String orderId = null;
    String type = "";

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.basket));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        cardBasketAdapter = new CardBasketAdapter(mContext,
                cartProducts, R.layout.recycle_card_basket, ordersPrice, payTotal, this);
        basket_recycler.setLayoutManager(linearLayoutManager);

        basket_recycler.setNestedScrollingEnabled(false);
        basket_recycler.setAdapter(cardBasketAdapter);
        if (getIntent().hasExtra("type")) {
            type = getIntent().getStringExtra("type");
        }
        if (Objects.equals(type, "order_details")) {
            orderId = getIntent().getStringExtra("orderId");
            online.setVisibility(View.GONE);
            payment_type_tv.setVisibility(View.GONE);
            wallet_tv.setVisibility(View.GONE);
            payment_type_tx.setVisibility(View.GONE);
            btn_confirm_order.setVisibility(View.GONE);
        }

        MyCart(mSharedPrefManager.getUserData().getUser_id());

        online.setOnClickListener(view -> {
            payment_type = "e_pay";
            online.setBackground(getDrawable(R.drawable.bg_change));
            payment_type_tv.setBackground(null);
            wallet_tv.setBackground(null);
        });

        payment_type_tv.setOnClickListener(view -> {
            payment_type = "cash";
            iswallet = null;
            payment_type_tv.setBackground(getDrawable(R.drawable.bg_change));
            online.setBackground(null);
            wallet_tv.setBackground(null);
        });
        wallet_tv.setOnClickListener(view -> {
            payment_type = "e_pay";
            wallet_tv.setBackground(getDrawable(R.drawable.bg_change));
            online.setBackground(null);
            payment_type_tv.setBackground(null);
            iswallet = true;
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_basket;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {


    }

    @OnClick(R.id.btn_confirm_order)
    void onConfirmClick() {
        if (!cartProducts.isEmpty()) {
            if (cartProducts.get(0).orderDeliveryTime.isEarly()) {
                new IsEarlyOrderDialog(this, cartProducts.get(0).orderDeliveryTime.getDeliveryTimeFormat()).show();
            } else if (has_prior) {
                new PeriorOrderDialog(this).show();
            } else {
                //charge(3);
                doOrder();
            }
        }
    }

    void showEditProduct(FragmentManager mFragmentManager) {
        ChooseDeliveryDialogFragment chooseDeliveryDialogFragment = ChooseDeliveryDialogFragment.newInstance();
        chooseDeliveryDialogFragment.show(mFragmentManager, "Choose Deivery");
        chooseDeliveryDialogFragment.setListner(this);
    }

    Float total_price;


    private void MyCart(int user_id) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).myCart(mLanguagePrefManager.getAppLanguage(),
                user_id, orderId).enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(Call<BasketResponse> call, final Response<BasketResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getCarts() != null ||
                                !response.body().getData().getCarts().isEmpty()) {
                            try {
                                has_prior = response.body().getData().getCarts().get(0).has_prior;
                            } catch (Exception e) {
                            }

                        }
                        if (response.body().getData().getCarts().size() == 0) {
                            deliveryPrice.setText(0 + "");
                            ordersPrice.setText(0 + "");
                            deliveryAddress.setText("");
                            payTotal.setText(0 + "");
                            btn_confirm_order.setVisibility(View.GONE);
                            text.setVisibility(View.GONE);
                            CommonUtil.makeToast(mContext, getString(R.string.no_data));
                            startActivity(new Intent(mContext, MainActivity.class));
                            finishAffinity();
                        } else {
                            total_price = response.body().getData().getAll_total_price();
                            deliveryPrice.setText(Math.round(response.body().getData().getCarts().get(0).total_delivery_price) + "");
                            ordersPrice.setText(response.body().getData().getAll_total_price() + "");
                            deliveryAddress.setText(response.body().getData().getCarts().get(0).user_address + "");
                            if (response.body().getData().getCarts().get(0).orderDeliveryTime.isEarly()) {
                                early.setVisibility(View.VISIBLE);
                            } else {
                                early.setVisibility(View.GONE);
                            }
                            early_value.setText(response.body().getData().getCarts().get(0).orderDeliveryTime.getDeliveryTimeFormat());
                            payTotal.setText((response.body().getData().getAll_total_price() +
                                    Math.round(response.body().getData().getCarts().get(0).total_delivery_price)) + "");
                            cardBasketAdapter.updateAll(response.body().getData().getCarts());
                            if (!response.body().getData().getCarts().get(0).have_delegate.equals("yes")) {
                                payment_type_tv.setVisibility(View.INVISIBLE);
                                online.setBackground(getDrawable(R.drawable.bg_change));
                                payment_type = "e_pay";
                            } else {
                                if (type.equals("order_details")) {
                                    payment_type_tv.setVisibility(View.GONE);
                                } else {
                                    payment_type_tv.setVisibility(View.VISIBLE);
                                }
                            }

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BasketResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


    private void doOrder() {
        // "e_pay"
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).doOrder(mLanguagePrefManager.getAppLanguage(),
                mSharedPrefManager.getUserData().getUser_id(),
                Float.parseFloat(deliveryPrice.getText().toString()), payment_type, iswallet)
                .enqueue(new Callback<AddOrderModel>() {
                    @Override
                    public void onResponse(Call<AddOrderModel> call, Response<AddOrderModel> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus() == 200) {
                                if (payment_type.equals("cash") || (iswallet != null && iswallet)) {
                                    //   showEditProduct(getSupportFragmentManager());
                                    Intent intent = new Intent(mContext, ShowOrderActivity.class);
                                    intent.putExtra("id", response.body().getOrder_id() + "");
                                    intent.putExtra("show", "show");
                                    startActivity(intent);
                                    finish();
                                } else {
                                    charge(response.body().getOrder_id());
                                }
                            } else if (response.body().getStatus() == 403) {
                                CommonUtil.makeToast(mContext, "لا يوجد رصيد كافى");
                            } else if (response.body().getStatus() == 410) {
                                CommonUtil.makeToast(mContext, response.body().getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddOrderModel> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {
        Intent intent = new Intent(mContext, MainActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void charge(int orderId) {
        startActivity(new Intent(this, TapPaymentActivity.class)
                .putExtra("order_id", orderId)
                .putExtra("price", payTotal.getText().toString()));
       /* RetroWeb.getClient().create(ServiceApi.class).charge(total_price.toString(), OrderId).enqueue(new Callback<PaymentModel>() {
            @Override
            public void onResponse(Call<PaymentModel> call, Response<PaymentModel> response) {
                // hideProgressDialog();
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    new PaymentWebviewDialog(BasketActivity.this,
                            response.body().getTransaction().getUrl(), OrderId).show();
                } else {
                    CommonUtil.makeToast(mContext, "faild");
                }
            }


            @Override
            public void onFailure(Call<PaymentModel> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
            }
        });*/
    }

    @Override
    public void orderHasDelivery(boolean hasDelivery) {
        startActivity(new Intent(mContext, MainActivity.class));

    }

    @Override
    public void refreshCart() {
        MyCart(mSharedPrefManager.getUserData().getUser_id());
    }

    @Override
    public void hasPrior() {
        doOrder();
    }
}
