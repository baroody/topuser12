package com.top.top12user.UI.Activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Listeners.AddButtonClicked;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.ShowOrderModel;
import com.top.top12user.Models.ShowOrderResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import cn.iwgang.countdownview.CountdownView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowOrderActivity extends ParentActivity {

    @OnClick(R.id.act_back)
    void onBack() {
        onBackPressed();
    }

    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.osra_image)
    ImageView osra_image;

    @BindView(R.id.order_details)
    Button order_details;

    @BindView(R.id.complaint)
    Button complaint;


    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.city)
    TextView city;
    @BindView(R.id.rb_rating)
    AppCompatRatingBar rb_rating;
    @BindView(R.id.time_one)
    TextView time_one;
    @BindView(R.id.step_one)
    TextView step_one;
    @BindView(R.id.time_two)
    TextView time_two;
    @BindView(R.id.step_two)
    TextView step_two;
    @BindView(R.id.time_three)
    TextView time_three;
    @BindView(R.id.step_three)
    TextView step_three;
    @BindView(R.id.time_four)
    TextView time_four;
    @BindView(R.id.step_four)
    TextView step_four;
    @BindView(R.id.time_five)
    TextView time_five;
    @BindView(R.id.time_six)
    TextView time_six;
    @BindView(R.id.time_seven)
    TextView time_seven;
    @BindView(R.id.step_five)
    TextView step_five;
    @BindView(R.id.step_seven)
    TextView step_seven;
    @BindView(R.id.step_six)
    TextView step_six;
    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.image2)
    ImageView image2;
    @BindView(R.id.image3)
    ImageView image3;
    @BindView(R.id.image4)
    ImageView image4;
    @BindView(R.id.image5)
    ImageView image5;
    @BindView(R.id.image6)
    ImageView image6;
    @BindView(R.id.image7)
    ImageView image7;
    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.constraintLayout10)
    ConstraintLayout constraintLayout10;

    @BindView(R.id.relativeLayou11)
    ConstraintLayout relativeLayou11;


    @BindView(R.id.date)
    CountdownView date;


    @BindView(R.id.btn_cancel)
    TextView btn_cancel;

    @BindView(R.id.cancel)
    ImageView cancel;
    @BindView(R.id.constrain)
    ConstraintLayout constrain;
    @BindView(R.id.ttt)
    TextView ttt;


    String id, shop, provider, show;
    ShowOrderModel showOrderModel;

    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        shop = getIntent().getStringExtra("shop");
        provider = getIntent().getStringExtra("provider");
        if (getIntent().hasExtra("show")) {
            show = getIntent().getStringExtra("show");
        } else {
            show = "";
        }
        getOrder();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getOrder();
    }

    @OnClick(R.id.chat)
    void onChat() {
        Intent intent = new Intent(mContext, ChatActivity.class);
        intent.putExtra("order", id);
        intent.putExtra("provider", provider);
        startActivity(intent);
    }
//
//    @OnClick(R.id.complaint)
//    void onComplaint() {
//        Intent intent = new Intent(mContext, ChatActivity.class);
//        intent.putExtra("order", id);
//        intent.putExtra("provider", provider);
//        startActivity(intent);
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (show.equals("show")) {
            startActivity(new Intent(mContext, MainActivity.class));
        } else {
            show = "show";
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_show_order;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    void setData(ShowOrderModel showOrderModel) {
        act_title.setText(mContext.getResources().getString(R.string.order_number) + showOrderModel.getOrder_id());
        Glide.with(mContext).load(showOrderModel.getImage()).into(image);
        Glide.with(mContext).load(showOrderModel.getImage()).into(osra_image);
        name.setText(showOrderModel.getShop_name());
        city.setText(showOrderModel.getCity_name());
        rb_rating.setRating(showOrderModel.getRate());
        time_one.setText(showOrderModel.getProvider_accept());
        time_two.setText(showOrderModel.getOrder_ready());
        time_three.setText(showOrderModel.getDelegate_received());
        time_four.setText(showOrderModel.getOn_way());
        time_five.setText(showOrderModel.getOrder_finish());
        order_details.setOnClickListener(view ->
                startActivity(new Intent(mContext, BasketActivity.class)
                        .putExtra("type", "order_details")
                        .putExtra("orderId", String.valueOf(showOrderModel.getOrder_id()))));
        shop = showOrderModel.shop_id.toString();
        if (((showOrderModel.order_created_at * 1000) > System.currentTimeMillis()) &&
                showOrderModel.getProvider_accept().isEmpty()) {
            date.setVisibility(View.VISIBLE);
            btn_cancel.setVisibility(View.VISIBLE);
            date.start((showOrderModel.order_created_at * 1000) -
                    System.currentTimeMillis());
        } else {
            date.setVisibility(View.GONE);
            btn_cancel.setVisibility(View.GONE);
        }


        date.setOnCountdownEndListener(cv -> {
            date.setVisibility(View.GONE);
            btn_cancel.setVisibility(View.GONE);
            date.setVisibility(View.GONE);
            btn_cancel.setVisibility(View.GONE);
            ttt.setVisibility(View.VISIBLE);
            constrain.setVisibility(View.GONE);
            cancel.setVisibility(View.VISIBLE);
        });
        complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ShowOrderActivity.this, ComplaintActivity.class)
                        .putExtra("order", showOrderModel));

            }
        });
        time_one.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
        step_one.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
        // step_two.setVisibility(View.INVISIBLE);
        image1.setImageResource(R.mipmap.reply);
        if (showOrderModel.have_delegate.equals("yes")) {
            constraintLayout10.setVisibility(View.GONE);
            relativeLayou11.setVisibility(View.GONE);
        } else {
            constraintLayout10.setVisibility(View.VISIBLE);
            relativeLayou11.setVisibility(View.VISIBLE);
        }
        if (showOrderModel.getProvider_accept().equals("")) {

            time_two.setText("");
        } else {
            time_two.setText(showOrderModel.getProvider_accept());
            time_two.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
            step_two.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
            image2.setImageResource(R.mipmap.inprogress);
            //   step_two.setVisibility(View.VISIBLE);
            image1.setImageResource(R.drawable.ic_check);
            if (((showOrderModel.order_delivery_time * 1000) > System.currentTimeMillis())) {
                date.setVisibility(View.VISIBLE);
                btn_cancel.setVisibility(View.VISIBLE);
                date.start((showOrderModel.order_delivery_time * 1000) - System.currentTimeMillis());
            } else {
                date.setVisibility(View.GONE);
            }
            date.setOnCountdownEndListener(cv -> {
                date.setVisibility(View.GONE);
            });

        }


        if (showOrderModel.getOrder_ready().equals("")) {
            //  step_four.setVisibility(View.INVISIBLE);
        } else {
            time_four.setText(showOrderModel.getOrder_ready());
            time_four.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
            step_four.setVisibility(View.VISIBLE);
            step_four.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
            image4.setImageResource(R.mipmap.route);
            image2.setImageResource(R.drawable.ic_check);
            image1.setImageResource(R.drawable.ic_check);
            //   image4.setImageResource(R.drawable.ic_check);
            date.setVisibility(View.GONE);
            btn_cancel.setVisibility(View.GONE);
        }
        if (!showOrderModel.getDelegate_received().equals("")
                && showOrderModel.have_delegate.equals("no")) {
            relativeLayou11.setVisibility(View.VISIBLE);
            time_six.setText(showOrderModel.getOrder_ready());
            step_six.setTextColor(ActivityCompat
                    .getColor(this, R.color.colorAccent));

            time_six.setTextColor(ActivityCompat
                    .getColor(this, R.color.colorAccent));

            step_six.setVisibility(View.VISIBLE);
            image6.setImageResource(R.mipmap.car);
            image2.setImageResource(R.drawable.ic_check);
            image1.setImageResource(R.drawable.ic_check);
            image4.setImageResource(R.drawable.ic_check);

        }

        if (showOrderModel.getOn_way().equals("")) {
            // step_three.setVisibility(View.INVISIBLE);
        } else {
            time_three.setText(showOrderModel.getOn_way());
            step_three.setVisibility(View.VISIBLE);
            step_three.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
            time_three.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
            image3.setImageResource(R.mipmap.car);
            image2.setImageResource(R.drawable.ic_check);
            image1.setImageResource(R.drawable.ic_check);
            image4.setImageResource(R.drawable.ic_check);
            image6.setImageResource(R.drawable.ic_check);

            //image7.setImageResource(R.drawable.ic_check);
            //  image6.setImageResource(R.drawable.ic_check);
        }
        if (!showOrderModel.getOrder_received_to_client().equals("") && showOrderModel.have_delegate.equals("no")) {

            constraintLayout10.setVisibility(View.VISIBLE);
            time_seven.setText(showOrderModel.getOrder_ready());
            step_seven.setTextColor(ActivityCompat.
                    getColor(this, R.color.colorAccent));
            time_seven.setTextColor(ActivityCompat.
                    getColor(this, R.color.colorAccent));
            step_seven.setVisibility(View.VISIBLE);
            image7.setImageResource(R.mipmap.car);
            image2.setImageResource(R.drawable.ic_check);
            image1.setImageResource(R.drawable.ic_check);
            image4.setImageResource(R.drawable.ic_check);
            image3.setImageResource(R.drawable.ic_check);
            image6.setImageResource(R.drawable.ic_check);
        }


        if (!showOrderModel.getOrder_finish().equals("")) {
            time_five.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
            step_five.setTextColor(ActivityCompat.getColor(this, R.color.colorAccent));
            step_five.setText(getString(R.string.finished));
            step_five.setVisibility(View.VISIBLE);
            image5.setImageResource(R.mipmap.finish);
            image2.setImageResource(R.drawable.ic_check);
            image5.setImageResource(R.drawable.ic_check);
            image1.setImageResource(R.drawable.ic_check);
            image4.setImageResource(R.drawable.ic_check);
            image3.setImageResource(R.drawable.ic_check);
            image7.setImageResource(R.drawable.ic_check);
            if (show.equals("show")) {
                Intent intent = new Intent(mContext, RatesActivity.class);
                intent.putExtra("shop_id", shop);
                intent.putExtra("type", "2");
                startActivity(intent);
                finish();
            }

            if (((Long.parseLong(showOrderModel.order_finish_unixtimestamp)*1000 + 600000) > System.currentTimeMillis())) {
                complaint.setVisibility(View.VISIBLE);
            } else {
                complaint.setVisibility(View.GONE);
            }

            date.setVisibility(View.GONE);
            btn_cancel.setVisibility(View.GONE);
        }

        if (!showOrderModel.getProvider_refused().equals("") ||
                !showOrderModel.order_failed.equals("")) {
            time_five.setVisibility(View.INVISIBLE);
            // step_five.setVisibility(View.INVISIBLE);
            time_four.setVisibility(View.INVISIBLE);
            //step_four.setVisibility(View.INVISIBLE);
            time_three.setVisibility(View.INVISIBLE);
            //step_three.setVisibility(View.INVISIBLE);
            time_two.setVisibility(View.INVISIBLE);
            //step_two.setVisibility(View.INVISIBLE);
            time_one.setVisibility(View.INVISIBLE);
            //step_one.setVisibility(View.INVISIBLE);
            confirm.setVisibility(View.INVISIBLE);
            btn_cancel.setVisibility(View.INVISIBLE);
            date.setVisibility(View.GONE);
            btn_cancel.setVisibility(View.GONE);
            ttt.setVisibility(View.VISIBLE);
            constrain.setVisibility(View.GONE);
            cancel.setVisibility(View.VISIBLE);
            //     CommonUtil.makeToast(mContext,getString(R.string.order_refused));
        }

        if (!showOrderModel.getOrder_finish().equals("")) {
            confirm.setVisibility(View.INVISIBLE);
            btn_cancel.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.confirm)
    void onConfirmClick() {
        orderfinished1();
    }

    @OnClick(R.id.btn_cancel)
    void onCancelClick() {
        refuse();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    Integer x = 0;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onButtonClicked(String order) {
        x = 1;
        getOrder();
    }


    private void getOrder() {
        if (x == 0)
            showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrder(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getUser_id(), Integer.parseInt(id)).enqueue(new Callback<ShowOrderResponse>() {
            @Override
            public void onResponse(Call<ShowOrderResponse> call, Response<ShowOrderResponse> response) {
                if (x == 0)
                    hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        showOrderModel = response.body().getData();
                        setData(response.body().getData());

                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShowOrderResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void orderfinished1() {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).ChageStatus(mSharedPrefManager.getUserData().getUser_id(), mLanguagePrefManager.getAppLanguage(), Integer.parseInt(id), "order finish").enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1 && show.equals("show")) {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                        Intent intent = new Intent(mContext, RatesActivity.class);
                        intent.putExtra("shop_id", shop);
                        intent.putExtra("type", "2");
                        startActivity(intent);
                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


    private void refuse() {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class)
                .ChageStatus(mSharedPrefManager.getUserData().getUser_id()
                        , mLanguagePrefManager.getAppLanguage(), Integer.parseInt(id), "client refused").enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                        startActivity(new Intent(mContext, MainActivity.class));
                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


}
