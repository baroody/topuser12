package com.top.top12user.UI.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.OrderDetailsModel;
import com.top.top12user.Models.OrderDetailsResponse;
import com.top.top12user.Models.OrderProductsModel;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.OrderDetailsAdapter;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryPriceActivity extends ParentActivity {
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.civ_family_image)
    CircleImageView civ_family_image;
    @BindView(R.id.tv_family_name)
    TextView tv_family_name;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.tv_total_coast)
    TextView tv_total_coast;
    String id,provider;
    @BindView(R.id.chat)
    ImageView chat;
    @BindView(R.id.product_recycler)
    RecyclerView product_recycler;
    LinearLayoutManager linearLayoutManager;
    OrderDetailsAdapter orderDetailsAdapter;
    ArrayList<OrderProductsModel> orderProductsModels = new ArrayList<>();
    @Override
    protected void initializeComponents() {
        id  = getIntent().getStringExtra("id");
        provider = getIntent().getStringExtra("provider");
        act_title.setText(getString(R.string.delivery_price));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        orderDetailsAdapter = new OrderDetailsAdapter(mContext,orderProductsModels,R.layout.recycler_oder_details);
        product_recycler.setLayoutManager(linearLayoutManager);
        product_recycler.setAdapter(orderDetailsAdapter);
        getOrder();

    }
    @OnClick(R.id.chat)
    void onChat(){
        Intent intent = new Intent(mContext,ChatActivity.class);
        intent.putExtra("order",id);
        intent.putExtra("provider",provider);
        startActivity(intent);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_delivery_price;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void setData(OrderDetailsModel orderDetailsModel){
        Glide.with(mContext).load(orderDetailsModel.getOrder_provider_avatar())
                .placeholder(R.mipmap.bguser).into(civ_family_image);
        tv_family_name.setText(orderDetailsModel.getOrder_provider_name());
        price.setText(orderDetailsModel.order_delivery_price+getString(R.string.Sar));
//        tv_total_coast.setText((orderDetailsModel.getTotal_price()+
//                Integer.parseInt(orderDetailsModel.getOrder_delegate_price()))+getString(R.string.Sar));
        tv_total_coast.setText((orderDetailsModel.getTotal_price()
                +Float.parseFloat(orderDetailsModel.order_delivery_price))+getString(R.string.Sar));
    }
    @OnClick(R.id.btn_accept)
    void onAccept(){
        accept();
    }
    @OnClick(R.id.btn_cancel)
    void onCancel(){
        refuse();
    }
    private void getOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrderDetail(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id)).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setData(response.body().getData());
                        orderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                       // mFamilyOrderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void accept(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).ChageStatus(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage(),Integer.parseInt(id),"client confirmed").enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void refuse(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).ChageStatus(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage(),Integer.parseInt(id),"client refused").enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
