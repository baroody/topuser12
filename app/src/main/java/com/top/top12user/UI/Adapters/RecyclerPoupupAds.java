package com.top.top12user.UI.Adapters;

import android.content.Context;
import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.top.top12user.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class RecyclerPoupupAds extends PagerAdapter {

    Context context;
    List<String> data;
    // private ImageView.ScaleType scaleType;
    private LayoutInflater layoutInflater;

    public RecyclerPoupupAds(Context context, List<String >data) {
        this.context = context;

        this.data=data;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View imageLayout = layoutInflater.inflate(R.layout.recycler_header,null);
        // assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.iv_header);

        Glide.with(context).load(data.get(position)).into(imageView);
        ViewPager vp =(ViewPager) container;
        vp.addView(imageLayout,0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}

