package com.top.top12user.UI.Views

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.top.top12user.Listeners.AddCart
import com.top.top12user.Listeners.HasPrior
import com.top.top12user.R
import kotlinx.android.synthetic.main.dailog_order_iseary.*


class IsEarlyOrderDialog(
        val activity: Activity, val time: String

) : Dialog(activity),
        View.OnClickListener {
    val addCart: HasPrior = activity as HasPrior
    override fun onClick(v: View) {
        dismiss()
    }


    override fun onBackPressed() {
        dismiss()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.dailog_order_iseary)
        window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
        )
        alert_message.text = activity.getString(R.string.is_early_text) + time

        accept.setOnClickListener {
            addCart.hasPrior()
        }
        cancel.setOnClickListener {
            dismiss()
        }

        window?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.decorView?.rootView?.setOnTouchListener { p0, p1 ->
            dismiss()
            true
        }

    }
}