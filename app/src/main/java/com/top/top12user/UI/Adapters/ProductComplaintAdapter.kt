package com.top.top12user.UI.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.top.top12user.Listeners.ItemComplaintChooser
import com.top.top12user.Models.OrderComplaintProduct
import com.top.top12user.R
import kotlinx.android.synthetic.main.item_product_complaint.view.*


class ProductComplaintAdapter(private val mActivity: Activity,
                              private val complaintList: ArrayList<OrderComplaintProduct>, val type: Int) :
        RecyclerView.Adapter<ProductComplaintAdapter.ViewHolder>() {
    private val itemComplaintChooser: ItemComplaintChooser = mActivity as ItemComplaintChooser
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(mActivity).inflate(R.layout.item_product_complaint,
                    parent, false))


    override fun getItemCount(): Int = complaintList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (type == 0) {
            holder.radioButton.visibility = View.GONE
            Glide.with(holder.foodImage.context).load(complaintList[position].productImage).into(holder.foodImage)
        } else {
            holder.radioButton.visibility = View.VISIBLE
            Glide.with(holder.foodImage.context).load(complaintList[position].product.url).into(holder.foodImage)
        }

        holder.foodName.text = complaintList[position].product.name_ar

        holder.radioButton.isChecked = complaintList[position].is_checked
        holder.coutOrder.text = "الكمية" + " " + complaintList[position].product.quantity.toString()
        holder.normalPrice.text = complaintList[position].product.price
        holder.discountPrice.text = "رس"
        holder.card.setOnClickListener {
            if (complaintList[position].is_checked) {
                complaintList[position].is_checked = false
                itemComplaintChooser.itemComplaintChooser(complaintList[position], "remove")
                notifyDataSetChanged()
            } else {
                complaintList[position].is_checked = true
                itemComplaintChooser.itemComplaintChooser(complaintList[position], "add")
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val foodName: TextView = view.tv_food_name
        val foodImage: ImageView = view.iv_food_image
        val coutOrder: TextView = view.tv_cout_order
        val radioButton: RadioButton = view.radioButton3
        val normalPrice: TextView = view.tv_normal_price
        val discountPrice: TextView = view.tv_discount_price
        val card: CardView = view.card

    }
}