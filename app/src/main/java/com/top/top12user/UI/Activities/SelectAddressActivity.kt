package com.top.top12user.UI.Activities

import android.content.Intent
import android.os.Bundle
import com.top.top12user.Base.ParentActivity
import com.top.top12user.R
import kotlinx.android.synthetic.main.activity_select_address.*

class SelectAddressActivity : ParentActivity() {
    override fun initializeComponents() {
        home.setOnClickListener {
            startActivity(Intent(mContext, MainActivity::class.java))
            finishAffinity()
        }
        select.setOnClickListener {
            startActivity(Intent(mContext, DetectLocationActivity::class.java))

        }

        address.text=mSharedPrefManager.userData.address
    }

    override fun isFullScreen(): Boolean {
        return false
    }

    override fun isEnableBack(): Boolean {
        return false
    }

    override fun getLayoutResource(): Int {
        return  R.layout.activity_select_address
    }

    override fun isRecycle(): Boolean {
        return false
    }

    override fun isEnableToolbar(): Boolean {
        return false
    }


    override fun hideInputType(): Boolean {
        return false
    }
}
