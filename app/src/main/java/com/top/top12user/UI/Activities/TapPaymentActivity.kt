package com.top.top12user.UI.Activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.top.top12user.Models.BaseResponse
import com.top.top12user.Models.CheckTransactionModel
import com.top.top12user.Network.RetroWeb
import com.top.top12user.Network.ServiceApi
import com.top.top12user.Pereferences.LanguagePreferences
import com.top.top12user.Pereferences.SharedPrefManager
import com.top.top12user.R
import com.top.top12user.Utils.CommonUtil
import company.tap.gosellapi.GoSellSDK
import company.tap.gosellapi.internal.activities.BaseActivity
import company.tap.gosellapi.internal.api.callbacks.GoSellError
import company.tap.gosellapi.internal.api.models.Authorize
import company.tap.gosellapi.internal.api.models.Charge
import company.tap.gosellapi.internal.api.models.PhoneNumber
import company.tap.gosellapi.internal.api.models.Token
import company.tap.gosellapi.open.controllers.SDKSession
import company.tap.gosellapi.open.controllers.ThemeObject
import company.tap.gosellapi.open.delegate.SessionDelegate
import company.tap.gosellapi.open.enums.AppearanceMode
import company.tap.gosellapi.open.enums.TransactionMode
import company.tap.gosellapi.open.models.CardsList
import company.tap.gosellapi.open.models.Customer
import company.tap.gosellapi.open.models.TapCurrency
import kotlinx.android.synthetic.main.activity_tap_payment.*
import kotlinx.android.synthetic.main.app_bar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal

class TapPaymentActivity : BaseActivity(), SessionDelegate {
    private val SDK_REQUEST_CODE = 1001
    private lateinit var sdkSession: SDKSession
    private var progress: ProgressDialog? = null
    var mSharedPrefManager: SharedPrefManager? = null
    var mLanguagePrefManager: LanguagePreferences? = null
    var orderId: Int = 0
    var price: String = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tap_payment)
        mLanguagePrefManager = LanguagePreferences(this)
        act_back.setOnClickListener {
            onBackPressed()
        }
        act_title.text = getString(R.string.pay)
        mSharedPrefManager = SharedPrefManager(this)

        orderId = intent.getIntExtra("order_id", 0)
        if (intent.getStringExtra("price") != null) {
            price = intent.getStringExtra("price")
        }
        // start tap goSellSDK
        startSDK()
    }

    /**
     * Integrating SDK.
     */
    private fun startSDK() {
        /**
         * Required step.
         * Configure SDK with your Secret API key and App Bundle name registered with tap company.
         */
        configureApp()
        /**
         * Optional step
         * Here you can configure your app theme (Look and Feel).
         */
        configureSDKThemeObject()
        /**
         * Required step.
         * Configure SDK Session with all required data.
         */
        configureSDKSession()
        /**
         * Required step.
         * Choose between different SDK modes
         */
        configureSDKMode()
        /**
         * If you included Tap Pay Button then configure it first, if not then ignore this step.
         */
        initPayButton()
    }

    private fun configureApp() {
        GoSellSDK.init(
                this,
                "sk_live_lqpkLVQKOIo0c3ifzGFdaJjP",
                "com.top.top12user"
        ) // to be replaced by merchant
        GoSellSDK.setLocale(mLanguagePrefManager?.appLanguage) //  language to be set by merchant
    }

    /**
     * Configure SDK Theme
     */
    private fun configureSDKThemeObject() {
        ThemeObject.getInstance()
                .setAppearanceMode(AppearanceMode.WINDOWED_MODE)
                .setSdkLanguage(mLanguagePrefManager?.appLanguage)
                .setHeaderFont(Typeface.createFromAsset(assets, "fonts/roboto_light.ttf"))
                .setHeaderTextColor(ContextCompat.getColor(this@TapPaymentActivity, R.color.black1))
                .setHeaderTextSize(17)
                .setHeaderBackgroundColor(
                        ContextCompat.getColor(
                                this@TapPaymentActivity,
                                R.color.french_gray_new
                        )
                )
                .setCardInputFont(Typeface.createFromAsset(assets, "fonts/roboto_light.ttf"))
                .setCardInputTextColor(ContextCompat.getColor(this@TapPaymentActivity, R.color.black))
                .setCardInputInvalidTextColor(ContextCompat.getColor(this@TapPaymentActivity, R.color.red))
                .setCardInputPlaceholderTextColor(
                        ContextCompat.getColor(
                                this@TapPaymentActivity,
                                R.color.gray
                        )
                )
                .setSaveCardSwitchOffThumbTint(
                        ContextCompat.getColor(
                                this@TapPaymentActivity,
                                R.color.french_gray_new
                        )
                )
                .setSaveCardSwitchOnThumbTint(
                        ContextCompat.getColor(
                                this@TapPaymentActivity,
                                R.color.vibrant_green
                        )
                )
                .setSaveCardSwitchOffTrackTint(
                        ContextCompat.getColor(
                                this@TapPaymentActivity,
                                R.color.french_gray
                        )
                )
                .setSaveCardSwitchOnTrackTint(
                        ContextCompat.getColor(
                                this@TapPaymentActivity,
                                R.color.vibrant_green_pressed
                        )
                )
                //.setScanIconDrawable(resources.getDrawable(R.drawable.btn_card_scanner_normal))
                .setPayButtonResourceId(R.drawable.btn_pay_selector) //btn_pay_merchant_selector
                .setPayButtonFont(Typeface.createFromAsset(assets, "fonts/roboto_light.ttf"))
                .setPayButtonDisabledTitleColor(
                        ContextCompat.getColor(
                                this@TapPaymentActivity,
                                R.color.white
                        )
                )
                .setPayButtonEnabledTitleColor(ContextCompat.getColor(this@TapPaymentActivity, R.color.white))
                .setPayButtonTextSize(14)
                .setPayButtonLoaderVisible(true)
                .setPayButtonSecurityIconVisible(true)
                .setPayButtonText(getString(R.string.pay)) // **Optional**
                // setup dialog textcolor and textsize
                .setDialogTextColor(
                        ContextCompat.getColor(
                                this@TapPaymentActivity,
                                R.color.black1
                        )
                ).dialogTextSize =
                17 // **Optional**
    }

    /**
     * Configure SDK Session
     */
    private fun configureSDKSession() {

        // Instantiate SDK Session
        if (this::sdkSession.isLateinit)
            sdkSession = SDKSession() //** Required **

        // pass your activity as a session delegate to listen to SDK internal payment process follow
        sdkSession.addSessionDelegate(this) //** Required **

        // initiate PaymentDataSource
        sdkSession.instantiatePaymentDataSource() //** Required **

        // set transaction currency associated to your account
        sdkSession.setTransactionCurrency(TapCurrency("SAR")) //** Required **

        // Using static CustomerBuilder method available inside TAP Customer Class you can populate TAP Customer object and pass it to SDK
        sdkSession.setCustomer(getCustomer()) //** Required **

        // Set Total Amount. The Total amount will be recalculated according to provided Taxes and Shipping
        sdkSession.setAmount(BigDecimal(price.toDouble())) //** Required **

        // Enable or Disable 3DSecure
        sdkSession.isRequires3DSecure(true)

    }


    /**
     * Configure SDK Theme
     */
    private fun configureSDKMode() {
        /**
         * You have to choose only one Mode of the following modes:
         * Note:-
         * - In case of using PayButton, then don't call sdkSession.start(this); because the SDK will start when user clicks the tap pay button.
         */
        //////////////////////////////////////////////////////    SDK with UI //////////////////////
        /**
         * 1- Start using  SDK features through SDK main activity (With Tap CARD FORM)
         */
        startSDKWithUI()
    }

    /**
     * Start using  SDK features through SDK main activity
     */
    private fun startSDKWithUI() {
        if (sdkSession != null) {
            val trx_mode = TransactionMode.PURCHASE
            sdkSession.transactionMode = trx_mode //** Required **
        }
    }

    /**
     * Include pay button in merchant page
     */
    private fun initPayButton() {
        if (ThemeObject.getInstance().payButtonFont != null)
            payButtonId.setupFontTypeFace(
                    ThemeObject.getInstance().payButtonFont
            )
        if (ThemeObject.getInstance()
                        .payButtonDisabledTitleColor != 0 && ThemeObject.getInstance()
                        .payButtonEnabledTitleColor != 0
        )
            payButtonId.setupTextColor(
                    ThemeObject.getInstance().payButtonEnabledTitleColor,
                    ThemeObject.getInstance().payButtonDisabledTitleColor
            )
        if (ThemeObject.getInstance().payButtonTextSize != 0) payButtonId.payButton.textSize =
                ThemeObject.getInstance().payButtonTextSize.toFloat()
        //
        if (ThemeObject.getInstance()
                        .isPayButtSecurityIconVisible
        ) payButtonId.securityIconView.visibility = if (ThemeObject.getInstance()
                        .isPayButtSecurityIconVisible
        ) View.VISIBLE else View.INVISIBLE
        if (ThemeObject.getInstance()
                        .payButtonResourceId != 0
        ) payButtonId.setBackgroundSelector(ThemeObject.getInstance().payButtonResourceId)
        if (sdkSession != null) {
            val trx_mode =
                    sdkSession.transactionMode
            if (trx_mode != null) {
                if (TransactionMode.SAVE_CARD == trx_mode || TransactionMode.SAVE_CARD_NO_UI == trx_mode) {
                    payButtonId.payButton.text =
                            getString(company.tap.gosellapi.R.string.save_card)
                } else if (TransactionMode.TOKENIZE_CARD == trx_mode || TransactionMode.TOKENIZE_CARD_NO_UI == trx_mode) {
                    payButtonId.payButton.text =
                            getString(company.tap.gosellapi.R.string.tokenize)
                } else {
                    payButtonId.payButton.text =
                            getString(company.tap.gosellapi.R.string.pay)
                }
            } else {
                configureSDKMode()
            }
            sdkSession.setButtonView(payButtonId, this, SDK_REQUEST_CODE)
        }
    }


    //    //////////////////////////////////////////////////////  Overridden section : Session Delegation ////////////////////////
    override fun paymentSucceed(charge: Charge) {

        RetroWeb.getClient().create(ServiceApi::class.java).completePayment(orderId, charge.source.id, charge.status.name, charge.id, charge.isLiveMode, charge.amount, charge.currency)
                .enqueue(object : Callback<BaseResponse> {
                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        if (response.body()?.status == 200) {
                            getPaymentStatus(orderId)

                        }
                    }

                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        Log.d("onResponse: ", t.message)
                    }

                })

        configureSDKSession()
        showDialog(
                charge.id,
                charge.response.message,
                company.tap.gosellapi.R.drawable.ic_checkmark_normal
        )
    }

    override fun paymentFailed(charge: Charge?) {
        println("Payment Failed : " + charge!!.status)
        println("Payment Failed : " + charge.description)
        println("Payment Failed : " + charge.response.message)
        showDialog(
                charge.id,
                charge.response.message,
                company.tap.gosellapi.R.drawable.icon_failed
        )
    }

    override fun authorizationSucceed(authorize: Authorize) {
        println("Authorize Succeeded : " + authorize.status)
        println("Authorize Succeeded : " + authorize.response.message)
        if (authorize.card != null) {
            println(
                    "Payment Authorized Succeeded : first six : " + authorize.card!!.firstSix
            )
            println("Payment Authorized Succeeded : last four: " + authorize.card!!.last4)
            println(
                    "Payment Authorized Succeeded : card object : " + authorize.card!!.getObject()
            )
        }
        println("##############################################################################")
        if (authorize.acquirer != null) {
            println(
                    "Payment Authorized Succeeded : acquirer id : " + authorize.acquirer!!.id
            )
            println(
                    "Payment Authorized Succeeded : acquirer response code : " + authorize.acquirer!!
                            .response.code
            )
            println(
                    "Payment Authorized Succeeded : acquirer response message: " + authorize.acquirer!!
                            .response.message
            )
        }
        println("##############################################################################")
        if (authorize.source != null) {
            println("Payment Authorized Succeeded : source id: " + authorize.source.id)
            println(
                    "Payment Authorized Succeeded : source channel: " + authorize.source
                            .channel
            )
            println(
                    "Payment Authorized Succeeded : source object: " + authorize.source.getObject()
            )
            println(
                    "Payment Authorized Succeeded : source payment method: " + authorize.source
                            .paymentMethodStringValue
            )
            println(
                    "Payment Authorized Succeeded : source payment type: " + authorize.source
                            .paymentType
            )
            println(
                    "Payment Authorized Succeeded : source type: " + authorize.source.type
            )
        }
        println("##############################################################################")
        if (authorize.expiry != null) {
            println(
                    "Payment Authorized Succeeded : expiry type :" + authorize.expiry!!.type
            )
            println(
                    "Payment Authorized Succeeded : expiry period :" + authorize.expiry!!.period
            )
        }
        configureSDKSession()
        showDialog(
                authorize.id,
                authorize.response.message,
                company.tap.gosellapi.R.drawable.ic_checkmark_normal
        )
    }

    override fun authorizationFailed(authorize: Authorize) {
        println("Authorize Failed : " + authorize.status)
        println("Authorize Failed : " + authorize.description)
        println("Authorize Failed : " + authorize.response.message)
        showDialog(
                authorize.id,
                authorize.response.message,
                company.tap.gosellapi.R.drawable.icon_failed
        )
    }


    override fun cardSaved(charge: Charge) {

    }

    override fun cardSavingFailed(charge: Charge) {

    }

    override fun cardTokenizedSuccessfully(token: Token) {
        println("Card Tokenized Succeeded : ")
        println(
                "Token card : " + token.card.firstSix + " **** " + token.card
                        .lastFour
        )
        println(
                "Token card : " + token.card.fingerprint + " **** " + token.card
                        .funding
        )
        println("Token card : " + token.card.id + " ****** " + token.card.name)
        println(
                "Token card : " + token.card.address + " ****** " + token.card
                        .getObject()
        )
        println(
                "Token card : " + token.card.expirationMonth + " ****** " + token.card
                        .expirationYear
        )
        showDialog(token.id, "Token", company.tap.gosellapi.R.drawable.ic_checkmark_normal)
    }

    override fun savedCardsList(cardsList: CardsList) {

    }


    override fun sdkError(goSellError: GoSellError?) {
        if (progress != null) progress!!.dismiss()
        if (goSellError != null) {
            println("SDK Process Error : " + goSellError.errorBody)
            println("SDK Process Error : " + goSellError.errorMessage)
            println("SDK Process Error : " + goSellError.errorCode)
            showDialog(
                    goSellError.errorCode.toString() + "",
                    goSellError.errorMessage,
                    company.tap.gosellapi.R.drawable.icon_failed
            )
        }
    }


    override fun sessionIsStarting() {
        println(" Session Is Starting.....")
    }

    override fun sessionHasStarted() {
        println(" Session Has Started .......")
    }


    override fun sessionCancelled() {
        Log.d("TapPaymentActivity", "Session Cancelled.........")
    }


    override fun sessionFailedToStart() {
        Log.d("TapPaymentActivity", "Session Failed to start.........")
    }


    override fun invalidCardDetails() {
        println(" Card details are invalid....")
    }

    override fun backendUnknownError(message: String) {
        println("Backend Un-Known error.... : $message")
    }

    override fun invalidTransactionMode() {
        println(" invalidTransactionMode  ......")
    }

    override fun invalidCustomerID() {
        if (progress != null) progress!!.dismiss()
        println("Invalid Customer ID .......")
    }

    override fun userEnabledSaveCardOption(saveCardEnabled: Boolean) {
        println("userEnabledSaveCardOption :  $saveCardEnabled")
    }


    private fun showDialog(
            chargeID: String,
            msg: String?,
            icon: Int
    ) {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val popupWindow: PopupWindow
        try {
            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            if (inflater != null) {
                val layout: View = inflater.inflate(
                        R.layout.charge_status_layout,
                        findViewById(
                                R.id.popup_element
                        )
                )
                popupWindow = PopupWindow(layout, width, 250, true)
                val status_icon =
                        layout.findViewById<ImageView>(company.tap.gosellapi.R.id.status_icon)
                val statusText =
                        layout.findViewById<TextView>(company.tap.gosellapi.R.id.status_text)
                val chargeText =
                        layout.findViewById<TextView>(company.tap.gosellapi.R.id.charge_id_txt)
                status_icon.setImageResource(icon)
                //                status_icon.setVisibility(View.INVISIBLE);
                chargeText.text = chargeID
                statusText.text = if (msg != null && msg.length > 30) msg.substring(
                        0,
                        29
                ) else msg
                popupWindow.showAtLocation(layout, Gravity.TOP, 0, 50)
                popupWindow.contentView
                        .startAnimation(AnimationUtils.loadAnimation(this, R.anim.popup_show))
                setupTimer(popupWindow)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setupTimer(popupWindow: PopupWindow) {
        // Hide after some seconds
        val handler = Handler()
        val runnable = Runnable {
            if (popupWindow.isShowing) {
                popupWindow.dismiss()
            }
        }
        popupWindow.setOnDismissListener {
            handler.removeCallbacks(
                    runnable
            )
        }
        handler.postDelayed(runnable, 4000)
    }


    fun cancelSession(view: View?) {
        sdkSession.cancelSession(this)
    }


    private fun getCustomer(): Customer {

        val phoneNumber = PhoneNumber("966", mSharedPrefManager?.userData?.phone)
        return Customer.CustomerBuilder(null).email("test@gmail").firstName("test")
                .lastName("test").metadata("")
                .phone(PhoneNumber(phoneNumber.countryCode, phoneNumber.number)).build()


    }

    fun getPaymentStatus(OrderId: Int) {
        RetroWeb.getClient().create(ServiceApi::class.java).getPaymentStatus(OrderId).enqueue(object : Callback<CheckTransactionModel?> {
            override fun onResponse(call: Call<CheckTransactionModel?>, response: Response<CheckTransactionModel?>) {
                // hideProgressDialog();
                if (response.isSuccessful)
                    if (response.body()?.transaction_status == "CAPTURED") {
                        val intent = Intent(this@TapPaymentActivity, ShowOrderActivity::class.java)
                        intent.putExtra("id", orderId.toString() + "")
                        startActivity(intent)
                        Toast.makeText(this@TapPaymentActivity, "تم", Toast.LENGTH_LONG).show()
                        finish()
                    } else {
                        val intent = Intent(this@TapPaymentActivity, MainActivity::class.java)
                        startActivity(intent)
                        Toast.makeText(this@TapPaymentActivity, "فشل", Toast.LENGTH_LONG).show()
                        finish()
                    }


            }

            override fun onFailure(call: Call<CheckTransactionModel?>, t: Throwable) {
                CommonUtil.handleException(this@TapPaymentActivity, t)
                t.printStackTrace()
            }
        })
    }

    /*
    * order_id

    transaction_id

    transaction_status

    charge_id

    live_mode

    amount

    currency*/
}