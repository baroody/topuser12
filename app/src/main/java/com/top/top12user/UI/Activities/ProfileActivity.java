package com.top.top12user.UI.Activities;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.ListModel;
import com.top.top12user.Models.ListResponse;
import com.top.top12user.Models.LoginResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Views.ListDialog;
import com.top.top12user.Utils.CommonUtil;
import com.top.top12user.Utils.PermissionUtils;
import com.top.top12user.Utils.ProgressRequestBody;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.top.top12user.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ProfileActivity extends ParentActivity implements OnItemClickListener,ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.civ_profile)
    CircleImageView civ_profile;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.city)
    EditText city;
    @BindView(R.id.email)
    EditText email;
    ListModel cityModel;
    @BindView(R.id.save)
    Button save;
    @OnClick(R.id.edit)
    void onEditClick(){
      civ_profile.setClickable(true);
      save.setVisibility(View.VISIBLE);

    }
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    ArrayList<ListModel> listModels = new ArrayList<>();
    ListDialog listDialog;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.profile));
        civ_profile.setClickable(false);
        getProfile();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        cityModel = listModels.get(position);
        city.setText(cityModel.getName());
        CommonUtil.PrintLogE("City id : " + cityModel.getId());
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    @OnClick(R.id.save)
    void onSaveClick(){
        if (ImageBasePath==null && password.getText().toString().equals("")){
            update(null);
        }else if (ImageBasePath==null && !(password.getText().toString().equals(""))){
            update(password.getText().toString());
        }else if (ImageBasePath !=null && password.getText().toString().equals("")){
            update(null,ImageBasePath);
        }else if (ImageBasePath != null && !(password.getText().toString().equals(""))){
            update(ImageBasePath,password.getText().toString());
        }
    }
    @OnClick(R.id.civ_profile)
    void onProvileClick(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        @SuppressLint("ResourceType") TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civ_profile.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(true)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    @OnClick(R.id.city)
    void onCityClick(){
        getCities();
    }
    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        listModels = response.body().getData();
                        listDialog = new ListDialog(mContext,ProfileActivity.this,listModels,getString(R.string.city));
                        listDialog.show();

                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(getApplicationContext(),t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProfile(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        Glide.with(mContext).load(response.body().getData().getAvatar()).placeholder(R.mipmap.bguser).into(civ_profile);
                        name.setText(response.body().getData().getName());
                        email.setText(response.body().getData().getEmail());
                        phone.setText(response.body().getData().getPhone());
                        city.setText(response.body().getData().getCity_name());
                        cityModel = new ListModel(Integer.parseInt(response.body().getData().getCity_id()),response.body().getData().getCity_name());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void update(String password){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).UpdateProfile(
                mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),name.getText().toString(),password,email.getText().toString(),phone.getText().toString(),cityModel.getId()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mSharedPrefManager.setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Glide.with(mContext).load(response.body().getData().getAvatar()).placeholder(R.mipmap.bguser).into(civ_profile);
                        name.setText(response.body().getData().getName());
                        email.setText(response.body().getData().getEmail());
                        phone.setText(response.body().getData().getPhone());
                        city.setText(response.body().getData().getCity_name());

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void update(String password,String PathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).UpdateProfilewithAvatar(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),name.getText().toString(),password,email.getText().toString(),phone.getText().toString(),cityModel.getId(),filePart).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mSharedPrefManager.setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Glide.with(mContext).load(response.body().getData().getAvatar()).placeholder(R.mipmap.bguser).into(civ_profile);
                        name.setText(response.body().getData().getName());
                        email.setText(response.body().getData().getEmail());
                        phone.setText(response.body().getData().getPhone());
                        city.setText(response.body().getData().getCity_name());

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
