package com.top.top12user.UI.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.R;
import androidx.appcompat.widget.PopupMenu.OnMenuItemClickListener;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingsActivity extends ParentActivity implements OnMenuItemClickListener {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.ed_lang)
    EditText ed_lang;

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.settings));
        setLanguageData();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.ed_lang)
    void onLangClick(){
        androidx.appcompat.widget.PopupMenu popup = new androidx.appcompat.widget.PopupMenu(this, ed_lang);
        popup.setOnMenuItemClickListener(this);// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.lang_popup_client, popup.getMenu());
        popup.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.lang_arabic:
                mLanguagePrefManager.setAppLanguage("ar");
                startActivity(new Intent(mContext,SplashActivity.class));

                return true;
            case R.id.lang_english:
                mLanguagePrefManager.setAppLanguage("en");
                startActivity(new Intent(mContext,SplashActivity.class));

                return true;
            default:
                return false;
        }
    }
    void setLanguageData() {
        if (mLanguagePrefManager.getAppLanguage().equals("en")) {
            ed_lang.setText(R.string.english);
        } else {
            ed_lang.setText(R.string.arabic);
        }

    }
}
