package com.top.top12user.UI.Activities;

import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Models.ConditionsResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutAppActivity extends ParentActivity {
    @BindView(R.id.tv_about_app)
    TextView tv_about_app;
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.act_back)
    ImageView act_back;
    @OnClick(R.id.act_back)
    void onBackClick(){
        onBackPressed();
    }



    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.about_app));
      getAbout();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about_app;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }



    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    private void getAbout(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAbout(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ConditionsResponse>() {
            @Override
            public void onResponse(Call<ConditionsResponse> call, Response<ConditionsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().equals(""))
                        {
                            tv_about_app.setText("");
                        }else {
                            tv_about_app.setText(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ConditionsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }



}
