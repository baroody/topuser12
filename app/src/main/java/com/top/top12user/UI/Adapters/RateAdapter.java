package com.top.top12user.UI.Adapters;

import android.content.Context;
import androidx.appcompat.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.CommentsModel;
import com.top.top12user.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class RateAdapter extends ParentRecyclerAdapter<CommentsModel> {

    public RateAdapter(final Context context, final List<CommentsModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CommentsModel rateModel = data.get(position);
        Glide.with(mcontext).load(rateModel.getComment_user_img()).skipMemoryCache(true)
                .placeholder(R.mipmap.bguser)
                .into(viewHolder.civImage);
        viewHolder.tvName.setText(rateModel.getComment_username());
        viewHolder.tvRate.setText(rateModel.getComment());
         viewHolder.ratingBar.setRating(rateModel.getStars());
        viewHolder.tvDate.setText(rateModel.getComment_date());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.civ_image)
        CircleImageView civImage;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_date)
        TextView tvDate;

        @BindView(R.id.tv_rate)
        TextView tvRate;
        @BindView(R.id.rb_rating)
        AppCompatRatingBar ratingBar;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}


