package com.top.top12user.UI.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Build;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.top.top12user.App.Constant;
import com.top.top12user.Base.ParentActivity;
import com.top.top12user.GPS.GPSTracker;
import com.top.top12user.GPS.GpsTrakerListener;
import com.top.top12user.Models.ListModel;
import com.top.top12user.Models.ListResponse;
import com.top.top12user.Models.LoginResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Views.ListDialog;
import com.top.top12user.Utils.CommonUtil;
import com.top.top12user.Utils.DialogUtil;
import com.top.top12user.Utils.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetectLocationActivity extends ParentActivity
        implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GpsTrakerListener {

    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.address_description)
    TextView addressDescription;



    @BindView(R.id.btn_detect)
    Button btn_detect;

    GoogleMap googleMap;

    Marker myMarker;

    GPSTracker gps;
    @BindView(R.id.txt_location)
    TextView txt_location;

    public String mLang, mLat;
    Geocoder geocoder;

    private AlertDialog mAlertDialog;

    boolean startTracker = false;

    String mResult;
    Double mlat, mlang;

    ArrayList<ListModel> data = new ArrayList<ListModel>();

    @Override
    protected void initializeComponents() {
        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        } else {
            showGPSDisabledAlertToUser();
        }
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getCities();
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_map_detect_location;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }


    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    Integer city_id = 0;
    Marker m = null;

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        getLocationWithPermission();
        try {
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    mlat = latLng.latitude;
                    mlang = latLng.longitude;
                    getAddress(latLng);
                    googleMap.clear();
                    m = googleMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.locationclient)).position(latLng).title(getResources().getString(R.string.my_location)));
                }
            });
        } catch (Exception e) {
        }
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
//                if (m != null)
//                m.remove();
//                m = googleMap.addMarker(new MarkerOptions()
//                        .icon(BitmapDescriptorFactory.
//                                fromResource(R.mipmap.locationclient)).
//                                position(googleMap.getCameraPosition().target).
//                                title(getResources().getString(R.string.my_location)));
                getAddress(googleMap.getCameraPosition().target);
            }
        });
    }

    private void getAddress(LatLng latLng) {
        List<Address> addresses;
        geocoder = new Geocoder(DetectLocationActivity.this, new Locale("ar"));
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.isEmpty()) {
                Toast.makeText(DetectLocationActivity.this, getResources().getString(R.string.select_real_location), Toast.LENGTH_SHORT).show();
            } else {
                mResult = addresses.get(0).getAddressLine(0);
                txt_location.setText(mResult);
                mLat = Double.toString(latLng.latitude);
                mLang = Double.toString(latLng.longitude);
                Log.e("LatLng:", "Lat: " + mLat + " Lng: " +
                        mLang + addresses.get(0).getLocality());
            }

            city_id=0;
            for (int i = 0; i < data.size(); i++) {
                try {
                    if (data.get(i).getName().equals(addresses.get(0).getLocality())) {
                        city_id = data.get(i).getId();
                    }
                }catch (Exception e){}
            }
            if(city_id==0){
                btn_detect.setText(R.string.unactive_location);
                btn_detect.setBackground(getDrawable(R.drawable.rounded_red_button));
                btn_detect.setEnabled(false);
            }else {
                btn_detect.setText(getString(R.string.detect_location));
                btn_detect.setBackground(getDrawable(R.drawable.rounded_green_button));
                btn_detect.setEnabled(true);
            }
        }
        catch (IOException e) {
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.e("LatLng", latLng.toString());
        mLang = Double.toString(latLng.latitude);
        mLat = Double.toString(latLng.longitude);
        if (myMarker != null) {
            myMarker.remove();
            putMapMarker(latLng.latitude, latLng.longitude);
        } else {
            putMapMarker(latLng.latitude, latLng.longitude);
        }
    }

    @OnClick(R.id.btn_detect)
    void onDetectedSuccess() {
        Log.e("Location", "Lat:" + mLat + " Lng:" + mLang);
        if (mLang != null && mLat != null) {
            Update(mLat, mLang, mResult);
        }

    }

    private void Update(String lat, String lng, String address) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updateupdateLat(mLanguagePrefManager.getAppLanguage()
                , mSharedPrefManager.getUserData().getUser_id(), lat, lng
                , address, addressDescription.getText().toString(), city_id).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        mSharedPrefManager.setUserData(response.body().getData());
                        startActivity(new Intent(mContext, MainActivity.class));
                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//        MarkerOptions marker = new MarkerOptions().position(
//                new LatLng(lat, log));
//        marker.icon(null);
//        marker.title("موقعي");
//        myMarker = googleMap.addMarker(marker);
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;
        //showProgressDialog(getString(R.string.detecting_location));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Code", "requestCode: " + requestCode);
        switch (requestCode) {
            case Constant.RequestCode.GET_LOCATION: {
                if (resultCode == RESULT_OK) {
                    Log.e("Code", "request GPS Enabled True");
                    getCurrentLocation();
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 800: {
                if (grantResults.length > 0) {
                    boolean Locationpermission = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    Log.e("Permission", "All permission are granted");
                    getCurrentLocation();
                    for (int i = 0; i < grantResults.length; i++) {
                        Log.e("Permission", grantResults[0] + "");
                    }
                } else {
                    Log.e("Permission", "permission arn't granted");
                }
                return;
            }
        }
    }


    public void getLocationWithPermission() {
        gps = new GPSTracker(this, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(DetectLocationActivity.this, PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }


    private void getCities() {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        data.addAll(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(getApplicationContext(), t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(DetectLocationActivity.this,
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
//                mlat = gps.getLatitude();
//                mlang = gps.getLongitude();
                List<Address> addresses;
                geocoder = new Geocoder(DetectLocationActivity.this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()) {
                        Toast.makeText(DetectLocationActivity.this, getResources().getString(R.string.select_real_location), Toast.LENGTH_SHORT).show();
                    } else {
                        mResult = addresses.get(0).getAddressLine(0);
                        Log.e("LatLng:", "Lat: " + mLat + " Lng: " + mLang);
                        txt_location.setText(mResult);
                    }

                } catch (IOException e) {
                }
                googleMap.clear();
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                // googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.locationclient)).position(mLang).title(getResources().getString(R.string.my_location)));
            }

        }
    }


    public static void startActivityForResult(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, DetectLocationActivity.class);
        appCompatActivity.startActivityForResult(intent, Constant.RequestCode.GET_LOCATION);
    }


    public static void startActivity(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, DetectLocationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        appCompatActivity.startActivity(intent);
    }

}

