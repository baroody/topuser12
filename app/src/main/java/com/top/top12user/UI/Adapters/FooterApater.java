package com.top.top12user.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.ShopsModel;
import com.top.top12user.R;
import com.bumptech.glide.Glide;

import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;

public class FooterApater  extends ParentRecyclerAdapter<String> {

    public FooterApater(final Context context, final List<String> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        FooterApater.ViewHolder holder = new FooterApater.ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final FooterApater.ViewHolder viewHolder = (FooterApater.ViewHolder) holder;
        String categoryModel = data.get(position);

        Glide.with(mcontext).load(categoryModel).placeholder(R.mipmap.bguser)
                .into(viewHolder.ivCatImage);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.iv_footer)
        ImageView ivCatImage;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}


