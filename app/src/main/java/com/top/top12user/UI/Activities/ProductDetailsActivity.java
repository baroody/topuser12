package com.top.top12user.UI.Activities;

import android.content.Intent;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Listeners.AddCart;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.AdditionsModel;
import com.top.top12user.Models.AddtionModel;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.BasketResponse;
import com.top.top12user.Models.CommonResponse;
import com.top.top12user.Models.ProductDetailsModel;
import com.top.top12user.Models.ShowProductResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.SpecialAdditiveAdapte;
import com.top.top12user.UI.Views.CartDialog;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsActivity extends ParentActivity implements OnItemClickListener, AddCart {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.basket)
    TextView basket;
    @BindView(R.id.iv_image)
    ImageView ivImage;

    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_normal_price)
    TextView tvNormalPrice;


    @BindView(R.id.tv_description)
    TextView tvDescription;

    @BindView(R.id.iv_minus)
    ImageView ivMinus;

    @BindView(R.id.iv_add)
    ImageView ivAdd;

    @BindView(R.id.ed_order_number)
    EditText edOrderNumber;

    @BindView(R.id.is_early)
    BetterSpinner early;


    @BindView(R.id.add_layout)
    LinearLayout add_layout;

    @BindView(R.id.include)
    ConstraintLayout include;

    @BindView(R.id.item_order_time)
    ConstraintLayout itemOrderTime;


    @BindView(R.id.radioButton)
    RadioButton radioButton;

    @BindView(R.id.radioButton2)
    RadioButton radioButton2;

    @BindView(R.id.tv_notes)
    TextView tv_notes;
    @BindView(R.id.txt_for)
    TextView txt_for;
    @BindView(R.id.txt_take)
    TextView txt_take;

    @BindView(R.id.rv_Recycle)
    RecyclerView rvRecycle;


    LinearLayoutManager linearLayoutManager;

    SpecialAdditiveAdapte mSpecialAdditiveAdapter;

    List<AdditionsModel> mSpecialAdditiveModels = new ArrayList<>();

    String selectedTime = null;
    private ArrayList<String> durationList = new ArrayList<>();

    ProductDetailsModel mFoodModel;

    @OnClick(R.id.act_back)
    void onBackClick() {
        onBackPressed();
    }

    @OnClick(R.id.basket)
    void onBasketClick() {

        startActivity(new Intent(mContext, BasketActivity.class));
    }

    String shop_id;
    int product_id;


    @Override
    protected void initializeComponents() {
        product_id = getIntent().getIntExtra("product_id", 0);
        shop_id = getIntent().getStringExtra("shop_id");
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mSpecialAdditiveAdapter = new SpecialAdditiveAdapte(mContext, mSpecialAdditiveModels, R.layout.recycler_special);
        mSpecialAdditiveAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(mSpecialAdditiveAdapter);
        getProduct(mSharedPrefManager.getUserData().getUser_id(), product_id);
        MyCart(mSharedPrefManager.getUserData().getUser_id());
        itemOrderTime.setVisibility(View.GONE);
        durationList.add("1 مساءً");
        durationList.add("2 مساءً");
        durationList.add("3 مساءً");
        durationList.add("4 مساءً");
        durationList.add("5 مساءً");
        durationList.add("6 مساءً");
        durationList.add("8 مساءً");
        durationList.add("9 مساءً");
        durationList.add("10 مساءً");

        radioButton.setOnClickListener(view -> {
            radioButton.setChecked(true);
            radioButton2.setChecked(false);
            selectedTime = "true";
            itemOrderTime.setVisibility(View.VISIBLE);


        });
        radioButton2.setOnClickListener(view -> {
            radioButton.setChecked(false);
            radioButton2.setChecked(true);
            selectedTime = null;
            itemOrderTime.setVisibility(View.GONE);
        });
        spinnerAdapter();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_product_details_one;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId() == R.id.iv_minus) {
            if (mSpecialAdditiveModels.get(position).getCount() == 0) {

            } else {
                mSpecialAdditiveModels.get(position).setCount(mSpecialAdditiveModels.get(position).getCount() - 1);
                mSpecialAdditiveAdapter.notifyDataSetChanged();
            }
        } else if (view.getId() == R.id.iv_add) {
            mSpecialAdditiveModels.get(position).setCount(mSpecialAdditiveModels.get(position).getCount() + 1);
            mSpecialAdditiveAdapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.iv_minus)
    void onMinusClick() {
        if (Integer.parseInt(edOrderNumber.getText().toString()) == 1) {

        } else {
            edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) - 1) + "");
            int price = Integer.parseInt(mFoodModel.getProduct_price());
            String price1 = price * Integer.parseInt(edOrderNumber.getText().toString()) + "";
            tvNormalPrice.setText(price1 + " " + getResources().getString(R.string.Sar));
        }
    }

    @OnClick(R.id.iv_add)
    void onAddClick() {
        edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) + 1) + "");
        String price = mFoodModel.getProduct_price();
        String price1 = Integer.parseInt(price) * Integer.parseInt(edOrderNumber.getText().toString()) + "";
        tvNormalPrice.setText(price1 + " " + getResources().getString(R.string.Sar));
    }

    ArrayList<AddtionModel> addtionModel = new ArrayList<>();

    @OnClick(R.id.btn_add_to_card)
    void onBtnAddToCardClick() {

        for (int i = 0; i < mSpecialAdditiveModels.size(); i++) {
            AddtionModel addtionModel1;

            addtionModel1 = new AddtionModel(mSpecialAdditiveModels.get(i).getId(), mSpecialAdditiveModels.get(i).getCount());
            addtionModel.add(addtionModel1);

        }
        CommonUtil.PrintLogE(new Gson().toJson(addtionModel));
        if (!mSharedPrefManager.getUserData().getPhone().isEmpty()) {
            if (mSharedPrefManager.getSave_cart_shop_id() == Integer.parseInt(shop_id)
                    || mSharedPrefManager.getSave_cart_shop_id() == 0
            ) {
                AddToCart(mFoodModel.getProduct_id(), Integer.parseInt(edOrderNumber.getText().toString()),
                        mSharedPrefManager.getUserData().getUser_id(), new Gson().toJson(addtionModel), "add");
            } else {
                new CartDialog(this).show();

            }
        } else {
            Intent intent = new Intent(mContext, RequestPhoneActivity.class);
            intent.putExtra("user_id", mSharedPrefManager.getUserData().getUser_id() + "");
            intent.putExtra("shop_id", shop_id);
            intent.putExtra("product_id", product_id);
            startActivity(intent);
        }
    }

    void setData(ProductDetailsModel productDetailsModel) {
        act_title.setText(productDetailsModel.getProduct_name());
        Glide.with(mContext)
                .load(productDetailsModel
                        .getProduct_main_image())
                .placeholder(R.mipmap.bguser)
                .into(ivImage);

        tvFoodName.setText(productDetailsModel.getProduct_name());
        tvNormalPrice.setText(productDetailsModel.getProduct_price() + getString(R.string.Sar));
        tvDescription.setText(productDetailsModel.getProduct_disc());
        tv_notes.setText(productDetailsModel.getProduct_extra_info());
        txt_for.setText(getString(R.string.for_to) + productDetailsModel.getProduct_enough_to() + getString(R.string.people));
        txt_take.setText(getString(R.string.takes) + " : " + productDetailsModel.getProduct_time());
        if (!productDetailsModel.order_type.equals("instant")) {
            include.setVisibility(View.VISIBLE);
        }

    }

    @OnClick(R.id.share)
    void onShare() {
        CommonUtil.ShareProductName(mContext, mFoodModel.getProduct_main_image() + "\n" + getString(R.string.product) +
                ": " + mFoodModel.getProduct_name() + "\n" + mFoodModel.getProduct_shop_name() +
                "  / " + mFoodModel.getProduct_disc());
    }

    private void getProduct(int user_id, int product_id) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showProduct(mLanguagePrefManager.getAppLanguage(), user_id, product_id).enqueue(new Callback<ShowProductResponse>() {
            @Override
            public void onResponse(Call<ShowProductResponse> call, Response<ShowProductResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getProduct_additions().size() == 0) {
                            add_layout.setVisibility(View.GONE);
                        } else {
                            add_layout.setVisibility(View.VISIBLE);
                            mSpecialAdditiveAdapter.updateAll(response.body().getData().getProduct_additions());
                        }
                        mFoodModel = response.body().getData();
                        setData(response.body().getData());

                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShowProductResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }

    private void AddToCart(int Product_id, int cout, int user_id, String additions, String status) {
        if (!radioButton.isChecked()) {
            selectedTime = null;
        }

        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).
                addToCart(mLanguagePrefManager.getAppLanguage()
                        , user_id, Product_id, cout, additions, status, selectedTime).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        CommonUtil.makeToast(mContext, getString(R.string.added_to_cart));
                        mSharedPrefManager.setSave_cart_shop_id(Integer.parseInt(shop_id));
                        Intent intent = new Intent(mContext, ShopActivity.class);
                        intent.putExtra("shop_id", Integer.parseInt(shop_id));
                        startActivity(intent);
                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });

    }

//    private void clearCartProducts(ArrayList<AddtionModel> addtionModel ,int user) {
//        showProgressDialog(getString(R.string.please_wait));
//        RetroWeb.getClient().create(ServiceApi.class).clearCartProducts(user,0).enqueue(new Callback<CommonResponse>() {
//            @Override
//            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
//                hideProgressDialog();
//                if (response.body().getStatus() == 200) {
//                    AddToCart(mFoodModel.getProduct_id(), Integer.parseInt(edOrderNumber.getText().toString()),
//                            mSharedPrefManager.getUserData().getUser_id(), new Gson().toJson(addtionModel));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CommonResponse> call, Throwable t) {
//                CommonUtil.handleException(mContext, t);
//                t.printStackTrace();
//                hideProgressDialog();
//            }
//        });
//
//    }


    private void spinnerAdapter() {
        ArrayAdapter<String> adapterRequest = new ArrayAdapter<String>(
                ProductDetailsActivity.this,
                R.layout.item_spinner, durationList);
        early.setAdapter(adapterRequest);
        early.setOnItemClickListener((parent, view, position, id) -> {
            String mSelectedText = parent.getItemAtPosition(position).toString();
            if (mSelectedText.equals("1 مساءً")) {
                selectedTime = "13:00:00";
            } else if (mSelectedText.equals("2 مساءً")) {
                selectedTime = "14:00:00";
            } else if (mSelectedText.equals("3 مساءً")) {
                selectedTime = "15:00:00";
            } else if (mSelectedText.equals("4 مساءً")) {
                selectedTime = "16:00:00";
            } else if (mSelectedText.equals("5 مساءً")) {
                selectedTime = "17:00:00";
            } else if (mSelectedText.equals("6 مساءً")) {
                selectedTime = "18:00:00";
            } else if (mSelectedText.equals("7 مساءً")) {
                selectedTime = "19:00:00";
            } else if (mSelectedText.equals("8 مساءً")) {
                selectedTime = "20:00:00";
            } else if (mSelectedText.equals("9 مساءً")) {
                selectedTime = "21:00:00";
            } else if (mSelectedText.equals("10 مساءً")) {
                selectedTime = "22:00:00";
            }
        });


    }

    private void MyCart(int user_id) {
        //  showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).myCart(mLanguagePrefManager.getAppLanguage(), user_id,
                null).enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(Call<BasketResponse> call, final Response<BasketResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getCarts().size() == 0) {

                            basket.setText("");
                            basket.setVisibility(View.GONE);
                        } else {
                            int x = 0;
                            int y = 0;
                            for (int i = 0; i < response.body().getData().getCarts().size(); i++) {
                                y = y + response.body().getData().getCarts().get(i).getCart_products().size();
                                x = y;
                                Log.e("xx", y + "");
                            }

                            Log.e("yy", x + "");
                            basket.setText(y + "");
                            basket.setVisibility(View.VISIBLE);

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BasketResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


    public void addcart() {
        AddToCart(mFoodModel.getProduct_id(), Integer.parseInt(edOrderNumber.getText().toString()),
                mSharedPrefManager.getUserData().getUser_id(), new Gson().toJson(addtionModel), "delete");
    }

}
