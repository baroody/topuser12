package com.top.top12user.UI.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.R;
import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.CartAdditions;
import com.top.top12user.Models.CartAdditions;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class SpecialAdditiveAdapter1 extends ParentRecyclerAdapter<CartAdditions> {

    public SpecialAdditiveAdapter1(final Context context, final List<CartAdditions> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CartAdditions specialAdditiveModel = data.get(position);
        float price = specialAdditiveModel.getAddition_price()/specialAdditiveModel.getAddition_count();
        Log.e("price1",price+"");
        viewHolder.tvPrice
                .setText((price*specialAdditiveModel.getAddition_count()) + " " + mcontext.getResources().getString(R.string.Sar));
        viewHolder.tvSpecialAdditiveName.setText(specialAdditiveModel.getAddition_name());

        viewHolder.edOrderNumber.setText(specialAdditiveModel.getAddition_count() + "");
        viewHolder.ivAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);

            }
        });
        viewHolder.ivMinus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.tv_special_additive_name)
        TextView tvSpecialAdditiveName;

        @BindView(R.id.iv_minus)
        ImageView ivMinus;

        @BindView(R.id.ed_order_number)
        EditText edOrderNumber;

        @BindView(R.id.iv_add)
        ImageView ivAdd;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
