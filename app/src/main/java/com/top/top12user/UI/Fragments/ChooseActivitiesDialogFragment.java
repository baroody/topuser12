package com.top.top12user.UI.Fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;


import com.top.top12user.App.Constant;
import com.top.top12user.Listeners.ChooseCategoryListner;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.CategoryModel;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.CategoryChooseAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Mahmoud on 1/19/18.
 */

public class ChooseActivitiesDialogFragment extends DialogFragment implements OnItemClickListener {

    @BindView(R.id.recycler_category)
    RecyclerView recycler_category;
    @BindView(R.id.filter)
    Button filter;
    @BindView(R.id.clear)
    Button clear;

    ChooseCategoryListner mChooseCategoryListner;

    GridLayoutManager layoutManager;

    CategoryChooseAdapter mCategoryChooseAdapter;

    ArrayList<CategoryModel> mCategoryModels = new ArrayList<>();

    ArrayList<CategoryModel> userChooseCategries = new ArrayList<>();

    public static ChooseActivitiesDialogFragment newInstance(ArrayList<CategoryModel> mCategoryModels) {
        Bundle bundle = new Bundle();
        ChooseActivitiesDialogFragment fragment = new ChooseActivitiesDialogFragment();
        bundle.putSerializable(Constant.BundleData.CATEGORY, mCategoryModels);
        fragment.setArguments(bundle);
        return fragment;
    }

    void getBundleData() {
        mCategoryModels = (ArrayList<CategoryModel>) getArguments().getSerializable(Constant.BundleData.CATEGORY);
    }

    public ChooseActivitiesDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_category, container);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBundleData();

        layoutManager = new GridLayoutManager(getActivity(),5);
        recycler_category.setLayoutManager(layoutManager);
        mCategoryChooseAdapter = new CategoryChooseAdapter(getActivity(), mCategoryModels,
                R.layout.recycler_category);
        mCategoryChooseAdapter.setOnItemClickListener(this);
        recycler_category.setAdapter(mCategoryChooseAdapter);

    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((LayoutParams) params);
       // getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation_2);
        // Call super onResume after sizing
        super.onResume();
    }

    public void setListner(ChooseCategoryListner chooseCategoryListner) {
        this.mChooseCategoryListner = chooseCategoryListner;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.filter)
    void onConfirmClick() {
        filterCorrectData();
        mChooseCategoryListner.ChooseCategories(userChooseCategries);
        dismiss();
    }

    @OnClick(R.id.clear)
    void onCancelClick() {
        dismiss();
    }


    @Override
    public void onItemClick(final View view, final int position) {
        if (mCategoryModels.get(position).isCheckCategory()) {
            mCategoryModels.get(position).setCheckCategory(false);
        } else {
            mCategoryModels.get(position).setCheckCategory(true);
        }
        mCategoryChooseAdapter.notifyDataSetChanged();
    }


    void filterCorrectData() {
        for (CategoryModel categoryModel : mCategoryModels) {
            if (categoryModel.isCheckCategory()) {
                userChooseCategries.add(categoryModel);
            }
        }
    }
}

