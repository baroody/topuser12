package com.top.top12user.UI.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.App.Constant;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.ChatAdapter;


import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.ChatModel;
import com.top.top12user.Models.GetChatResponse;
import com.top.top12user.Models.OrderModel;
import com.top.top12user.Models.OrderProductsModel;
import com.top.top12user.Models.SendChatResponse;
import com.top.top12user.Utils.CommonUtil;
import com.top.top12user.Utils.PermissionUtils;
import com.top.top12user.Utils.ProgressRequestBody;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.top.top12user.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ChatActivity extends ParentActivity  implements ProgressRequestBody.UploadCallbacks, OnItemClickListener {

    SharedPreferences sharedPreferences;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.chat_msg)
    EditText chatMsg;
    @BindView(R.id.fab_msg)
    ImageView fabMsg;
    @BindView(R.id.upload_photo)
    ImageView uploadPhoto;
    @BindView(R.id.chat_img)
    ImageView img;


    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager msglinearLayoutMander;

    int i =0;

    protected SwipeRefreshLayout swipeRefreshLayout;


   // Conversation conversation;


    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;

   //OrderModel morderModel;
//
    ChatModel mChatModel;
    String mAdresse, mLang, mLat = null;

    List<ChatModel> chatModels = new ArrayList<>();
    List<OrderProductsModel> foodModels = new ArrayList<>();
    ChatModel chatModel;


    ChatAdapter chatAdapter;
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }

    public static int friend_id = 0;
    public static boolean active = false;

    BroadcastReceiver MsgReciever;
    private static String Token;
    Thread t;
    OrderModel orderModel;

    String order,provider;




    @Override
    protected void initializeComponents() {
        sharedPreferences = getSharedPreferences("home", MODE_PRIVATE);

        act_title.setText(mContext.getResources().getString(R.string.chat));
        provider = getIntent().getStringExtra("provider");
        order = getIntent().getStringExtra("order");
        friend_id = Integer.parseInt(provider);

        Log.e("id",friend_id+"");



        msglinearLayoutMander = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false);
        chatAdapter = new ChatAdapter(mContext,chatModels,recycler);
        chatAdapter.setOnItemClickListener(this);
        recycler.setLayoutManager(msglinearLayoutMander);
        recycler.setAdapter(chatAdapter);


        // Toast.makeText(ChatActivity.this, "fdgfgfg", Toast.LENGTH_SHORT).show();
        final ChatModel  chatModel1 = new ChatModel();
        chatModel1.setAvatar("");
        chatModel1.setMsg("");
        chatModel1.setSent_at("");
        chatModel1.setType("");
        chatModel1.setUsername("");
        MsgReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
              //  Toast.makeText(ChatActivity.this, "broad", Toast.LENGTH_SHORT).show();

                if (intent.getAction().equalsIgnoreCase("new_message")){
                  //  Toast.makeText(context, "broad2", Toast.LENGTH_SHORT).show();


                    chatModel1.setAvatar(intent.getStringExtra("avatar"));
                    chatModel1.setMsg(intent.getStringExtra("msg"));
                    chatModel1.setSent_at(intent.getStringExtra("sent_at"));
                    chatModel1.setType(intent.getStringExtra("type"));
                    chatModel1.setId(Integer.parseInt(intent.getStringExtra("id")));
                    chatModel1.setUser(Integer.parseInt(intent.getStringExtra("user")));
                    chatModel1.setOrder(Integer.parseInt(intent.getStringExtra("order")));
                    chatModel1.setUsername(intent.getStringExtra("username"));
                    Log.e("chat",chatModel1.getMsg());
                    Log.e("chat",chatModel1.getAvatar());
                    Log.e("chat",chatModel1.getId()+"");
                    Log.e("chat",chatModel1.getUser()+"");
                    Log.e("chat",chatModel1.getOrder()+"");

                    Log.e("chat",new Gson().toJson(chatModel1));
                    chatAdapter.notifyDataSetChanged();
                    chatAdapter.onAddMessage(chatModel1);
                    friend_id = intent.getIntExtra("user",0);




                }
            }
        };

        getChat();


    }
    @OnClick(R.id.location)
    void onLocation(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse);
                    showProgressDialog(getString(R.string.please_wait));
                    ChatModel chatModel2 = new ChatModel();
                    chatModel2.setUsername(mSharedPrefManager.getUserData().getName());
                    chatModel2.setUser(mSharedPrefManager.getUserData().getUser_id());
                    chatModel2.setOrder(Integer.parseInt(order));
                    chatModel2.setType("text");
                    chatModel2.setSent_at(getString(R.string.now));
                    chatModel2.setMsg(chatMsg.getText().toString());
                    chatModel2.setAvatar(mSharedPrefManager.getUserData().getAvatar());
                    String message = "http://maps.google.com/maps?saddr="+mLat+","+mLang;
                    // chatAdapter.onAddMessage(chatModel2);
                    RetroWeb.getClient().create(ServiceApi.class).sendText(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(order),1,message).enqueue(new Callback<SendChatResponse>() {
                        @Override
                        public void onResponse(Call<SendChatResponse> call, Response<SendChatResponse> response) {
                            hideProgressDialog();
                            if (response.isSuccessful()){
                                if (response.body().getStatus()==1){
                                    // chatModels.add(response.body().getData());
                                    chatAdapter.onAddMessage(response.body().getData());
                                    // getChat();
                                    // chatAdapter.update(chatModels.size()+1,response.body().getData());
                                    chatMsg.setText("");

                                }else {
                                    CommonUtil.makeToast(mContext,response.body().getMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<SendChatResponse> call, Throwable t) {
                            CommonUtil.handleException(mContext,t);
                            t.printStackTrace();
                            hideProgressDialog();

                        }
                    });
                }
            }
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_chat;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @OnClick(R.id.upload_photo)
    void onUploadPhotoClick(){
        getPickImageWithPermission();
    }

    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        img.setImageURI(Uri.parse(ImageBasePath));
                        SendImage(ImageBasePath);

                    }
                })
                //.setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }





    @OnClick(R.id.fab_msg)
    void onSendMessageClick(){
        sendMessage();
    }

    private void sendMessage(){
        showProgressDialog(getString(R.string.please_wait));
        ChatModel chatModel2 = new ChatModel();
        chatModel2.setUsername(mSharedPrefManager.getUserData().getName());
        chatModel2.setUser(mSharedPrefManager.getUserData().getUser_id());
        chatModel2.setOrder(Integer.parseInt(order));
        chatModel2.setType("text");
        chatModel2.setSent_at(getString(R.string.now));
        chatModel2.setMsg(chatMsg.getText().toString());
        chatModel2.setAvatar(mSharedPrefManager.getUserData().getAvatar());
       // chatAdapter.onAddMessage(chatModel2);
        RetroWeb.getClient().create(ServiceApi.class).sendText(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(order),1,chatMsg.getText().toString()).enqueue(new Callback<SendChatResponse>() {
            @Override
            public void onResponse(Call<SendChatResponse> call, Response<SendChatResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                       // chatModels.add(response.body().getData());
                        chatAdapter.onAddMessage(response.body().getData());
                       // getChat();
                       // chatAdapter.update(chatModels.size()+1,response.body().getData());
                        chatMsg.setText("");

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SendChatResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void SendImage(String  PathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ChatActivity.this);
        filePart = MultipartBody.Part.createFormData("msg", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).sendImage(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),
                Integer.parseInt(order),2,filePart).enqueue(new Callback<SendChatResponse>() {
            @Override
            public void onResponse(Call<SendChatResponse> call, Response<SendChatResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                       // chatModels.add(response.body().getData());
                        chatAdapter.onAddMessage(response.body().getData());
                       // getChat();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SendChatResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getChat(){
        //showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getChat(mLanguagePrefManager.getAppLanguage(),Integer.parseInt(order),mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<GetChatResponse>() {
            @Override
            public void onResponse(Call<GetChatResponse> call, Response<GetChatResponse> response) {
               // hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                           // recycler.setVisibility(View.GONE);
                        }else {
                            //hideProgressDialog();
                            chatModels = response.body().getData();

                            chatAdapter.updateAll(chatModels);
                            if(chatModels.size()>0){
                                recycler.scrollToPosition(chatModels.size()- 1);
                                Log.e("ccc",new Gson().toJson(chatModels.size()-1));

                            }
                            Log.e("www",new Gson().toJson(response.body().getData()));
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetChatResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
               // hideProgressDialog();

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences.Editor editor = getSharedPreferences("home", MODE_PRIVATE).edit();
        editor.putString("senderID", String.valueOf(friend_id));
        editor.apply();
        LocalBroadcastManager.getInstance(this).registerReceiver(MsgReciever,new IntentFilter("new_message"));}


    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = getSharedPreferences("home", MODE_PRIVATE).edit();
        editor.putString("senderID", "0");
        editor.apply();


    }



    @Override
    public void onItemClick(View view, int position){
        if (view.getId()==R.id.chat_msgs){
            if (chatModels.get(position).getMsg().startsWith("http")){
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(chatModels.get(position).getMsg())));
            }
        }




    }
}
