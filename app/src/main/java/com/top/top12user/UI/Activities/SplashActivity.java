package com.top.top12user.UI.Activities;


import android.content.Intent;

import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.FCM.MyFirebaseInstanceIDService;
import com.top.top12user.Models.LoginResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends ParentActivity {

    @BindView(R.id.lay_splash)
    LinearLayout mLinearLayout;

    Animation fade;


    @Override
    protected void initializeComponents() {

//        if (mSharedPrefManager.getLoginStatus()){
//           // startActivity(new Intent(mContext,MainActivity.class));
//            startActivity(new Intent(mContext, DetectLocationActivity.class));
//
//        }else {
//            startActivity(new Intent(mContext,LoginActivity.class));
//        }
//        if(!mSharedPrefManager.getUserData().address.isEmpty()){}else{
//            visitor();
//        }

//        divceId=  MyFirebaseInstanceIDService.getToken(this);
//        if(divceId)
        if (mSharedPrefManager.getUserData() != null) {
            user_id = mSharedPrefManager.getUserData().getUser_id() + "";
        }
        if (user_id != null) {
            visitor();
        }

        //  SplashActivity.this.finish();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }


    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {
        visitor();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fade = AnimationUtils.loadAnimation(this, R.anim.alpha);
        mLinearLayout.clearAnimation();
        mLinearLayout.startAnimation(fade);
        fade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(final Animation animation) {

            }

            @Override
            public void onAnimationEnd(final Animation animation) {

                //       startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                // visitor();
                //    SplashActivity.this.finish();

            }

            @Override
            public void onAnimationRepeat(final Animation animation) {
            }
        });
    }

    String user_id = null;

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    private void visitor() {

//        if (MyFirebaseInstanceIDService.getToken(this) == null) {
//            startActivity(new Intent(mContext, DetectLocationActivity.class));
//            SplashActivity.this.finish();
//        } else {
        //  showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).visitor("2",
                FirebaseInstanceId.getInstance().getToken(),
                user_id).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().getType().equals("client")) {
                            mSharedPrefManager.setUserData(response.body().getData());
                            mSharedPrefManager.setLoginStatus(true);
                            if (response.body().getData().address.isEmpty()) {
                                startActivity(new Intent(mContext, DetectLocationActivity.class));
                                SplashActivity.this.finish();
                            } else {
                                mSharedPrefManager.setUserData(response.body().getData());
                                mSharedPrefManager.setLoginStatus(true);
                                startActivity(new Intent(mContext, SelectAddressActivity.class));
                                SplashActivity.this.finish();

                            }
                        } else {

                            CommonUtil.makeToast(mContext, getString(R.string.you_not_client));
                        }
                    } else if (response.body().getStatus() == 2) {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                        Intent intent = new Intent(mContext, ConfirmCodeActivity.class);
                        intent.putExtra("user_id", response.body().getData().getUser_id() + "");
                        startActivity(intent);

                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
//}
