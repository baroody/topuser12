package com.top.top12user.UI.Activities

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.top.top12user.Models.ComplaintModel
import com.top.top12user.Models.ComplaintResponse
import com.top.top12user.Network.RetroWeb
import com.top.top12user.Network.ServiceApi
import com.top.top12user.Pereferences.LanguagePreferences
import com.top.top12user.Pereferences.SharedPrefManager
import com.top.top12user.R
import com.top.top12user.UI.Adapters.ComplaintAdapter
import com.top.top12user.Utils.CommonUtil
import company.tap.gosellapi.internal.activities.BaseActivity
import kotlinx.android.synthetic.main.activity_list_complaint.*
import kotlinx.android.synthetic.main.activity_tap_payment.*
import kotlinx.android.synthetic.main.app_bar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ComplaintListActivity : BaseActivity() {
    var page = 0
    var pastVisibleItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var loadMore = false
    private var loading = true
    var mSharedPrefManager: SharedPrefManager? = null
    var mLanguagePrefManager: LanguagePreferences? = null
    lateinit var complaintAdapter: ComplaintAdapter
    val complaintList: ArrayList<ComplaintModel> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_complaint)
        mLanguagePrefManager = LanguagePreferences(this)
        act_back.setOnClickListener {
            onBackPressed()
        }
        mSharedPrefManager = SharedPrefManager(this)

        act_title.text = getString(R.string.complaint)
        val mLayoutManager = LinearLayoutManager(
                this,
                RecyclerView.VERTICAL, false
        )
        rv.layoutManager = mLayoutManager
        complaintAdapter = ComplaintAdapter(this, complaintList)
        rv.adapter = complaintAdapter
        rv.addOnScrollListener(object :
                RecyclerView.OnScrollListener() {
            override fun onScrolled(
                    recyclerView: RecyclerView,
                    dx: Int,
                    dy: Int
            ) {
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.childCount
                    totalItemCount = mLayoutManager.itemCount
                    pastVisibleItems =
                            mLayoutManager.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                            loading = false
                            loadMore = true
                            page++
                            getOrderComPlaint()
                        }
                    }
                }
            }
        })
        getOrderComPlaint()
    }

    private fun getOrderComPlaint() {
        RetroWeb.getClient().create(ServiceApi::class.java).getOrderComPlaint(page, mSharedPrefManager!!.userData.user_id,
                mLanguagePrefManager?.appLanguage)
                .enqueue(object : Callback<ComplaintResponse> {
                    override fun onResponse(call: Call<ComplaintResponse>, response: Response<ComplaintResponse>) {
                        if (response.isSuccessful) {
                            complaintList.clear()
                            response.body()?.let { complaintList.addAll(it.orderComplaint) }
                            complaintAdapter.notifyDataSetChanged()
                            loading = false
                        }
                    }

                    override fun onFailure(call: Call<ComplaintResponse>, t: Throwable) {
                        CommonUtil.handleException(this@ComplaintListActivity, t)
                        t.printStackTrace()
                    }
                })
    }
}