package com.top.top12user.UI.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.R;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.ChatModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends ParentRecyclerAdapter<ChatModel> {
    // private final SharedPrefManager mPrefs;
    //private Context mContext;
    //  private List<ChatModel> model;RecyclerView recyclerView;

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;

    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 4;

    public ChatAdapter(Context context, List<ChatModel> data, final RecyclerView recyclerView) {
        super(context, data, recyclerView);
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        // ParentRecyclerViewHolder view = null;
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            View viewitem = inflater.inflate(R.layout.item_chat_sender, parent, false);
            return new ChatTextHolder(viewitem);
        } else {
            View viewitem = inflater.inflate(R.layout.item_chat_user, parent, false);
            return new ChatTextHolder(viewitem);

        }
    }



    @Override
    public int getItemViewType(int position) {

          if (data.get(position).getUser() ==  mSharedPrefManager.getUserData().getUser_id()) {
            return VIEW_TYPE_MESSAGE_SENT;

          } else {

            return VIEW_TYPE_MESSAGE_RECEIVED;

          }


    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder viewHolder,final int position) {
        final ChatModel model = data.get(position);
        ChatTextHolder holder = (ChatTextHolder) viewHolder;
        if (model.getType().equals("text")  && !model.getMsg().trim().isEmpty()){


            holder.chat_img.setVisibility(View.GONE);
            holder.msg.setVisibility(View.VISIBLE);
            holder.msg.setText(model.getMsg());
            holder.chat_time.setText(model.getSent_at());
            holder.msg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(v,position);
                }
            });

            if (model.getAvatar()!=null) {
                if (!model.getAvatar().equals("")) {


                    Glide.with(mcontext).load(model.getAvatar()).placeholder(R.mipmap.bguser).into(((ChatTextHolder) viewHolder).avatar);


                }}
        }
         else if (model.getType().equals("image") && !model.getMsg().trim().isEmpty()){
            holder.msg.setVisibility(View.GONE);
            holder.chat_img.setVisibility(View.VISIBLE);
            Glide.with(mcontext).load(model.getMsg()).placeholder(R.mipmap.bguser)
                    .into(((ChatTextHolder) viewHolder).chat_img);
            // Picasso.with(mcontext).load(model.getImage()).error(R.mipmap.splash).into(holder.avatar);
            // Picasso.with(mcontext).load(model.getImage()).resize(300,300).into(holder.chat_img);
           // holder.msg.setText(model.getMessage());
            holder.chat_time.setText(model.getSent_at());


            if (model.getAvatar()!=null) {
                if (!model.getAvatar().equals("")) {

                    Glide.with(mcontext).load(model.getAvatar()).placeholder(R.mipmap.bguser).into(((ChatTextHolder) viewHolder).avatar);

                }
            }
        }





    }

    public void onAddMessage(ChatModel messages) {

        if (data.size()>0){
            this.data.add(data.size(),messages);
            notifyDataSetChanged();
            recyclerView.smoothScrollToPosition(data.size());
        }else {
            this.data.add(0, messages);
            notifyItemInserted(data.size());
            recyclerView.smoothScrollToPosition(data.size());
        }

//        this.data.add(data.size(), messages);
//        notifyItemInserted(data.size());
    //    recyclerView.smoothScrollToPosition(data.size());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ChatTextHolder extends ParentRecyclerViewHolder {
        @BindView(R.id.prof_img)
        CircleImageView avatar;
        @BindView(R.id.chat_msgs)
        TextView msg;
        @BindView(R.id.chat_time)
        TextView chat_time;
        @BindView(R.id.chat_img)
        ImageView chat_img;


        public ChatTextHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


}
