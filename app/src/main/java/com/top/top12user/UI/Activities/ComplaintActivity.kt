package com.top.top12user.UI.Activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.top.top12user.Listeners.ItemComplaintChooser
import com.top.top12user.Models.*
import com.top.top12user.Network.CreateOrderComplaintRequest
import com.top.top12user.Network.OrderComplaintDetail
import com.top.top12user.Network.RetroWeb
import com.top.top12user.Network.ServiceApi
import com.top.top12user.Pereferences.LanguagePreferences
import com.top.top12user.Pereferences.SharedPrefManager
import com.top.top12user.R
import com.top.top12user.UI.Adapters.ProductComplaintAdapter
import com.top.top12user.Utils.CommonUtil
import com.top.top12user.Utils.chooseEasyImage
import com.top.top12user.Utils.easyImageResult
import company.tap.gosellapi.internal.activities.BaseActivity
import kotlinx.android.synthetic.main.activity_complaint.*
import kotlinx.android.synthetic.main.activity_list_complaint.*
import kotlinx.android.synthetic.main.activity_tap_payment.*
import kotlinx.android.synthetic.main.app_bar.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class ComplaintActivity : BaseActivity(), ItemComplaintChooser {
    lateinit var productComplaintAdapter: ProductComplaintAdapter
    var mSharedPrefManager: SharedPrefManager? = null
    var mLanguagePrefManager: LanguagePreferences? = null
    private val complaintList: ArrayList<OrderComplaintProduct> = arrayListOf()
    lateinit var complaint: ComplaintModel
    lateinit var order: ShowOrderModel
    var descriptionDetails: String = ""
    private lateinit var easyImage: EasyImage
    private val complaintImage: ArrayList<String> = arrayListOf()
    private val orderComplaintDetails: ArrayList<OrderComplaintDetail> = arrayListOf()
    lateinit var orderId: String
    lateinit var userId: String
    lateinit var providerId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complaint)
        mSharedPrefManager = SharedPrefManager(this)
        mLanguagePrefManager = LanguagePreferences(this)
        if (intent.hasExtra("complaint")) {
            complaint = intent.getSerializableExtra("complaint") as ComplaintModel
            complaintList.addAll(complaint.orderComplaintProduct)
            complaint_note.setText(complaint.description)
            order_time.text = complaint.createdAt
            tv_city.text = complaint.orderComplaintProduct[0].order_user_city
            tv_order_number.text = complaint.orderId
            orderId = complaint.orderId
            userId = complaint.user.id.toString()
            providerId = complaint.provider.id.toString()
            when (complaint.description_detail) {
                "not_good" -> {
                    descriptionDetails = "not_good"
                    not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                    not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                    missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                }
                "not_matching" -> {
                    descriptionDetails = "not_matching"
                    not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                    not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                    missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                }
                "missing_item" -> {
                    descriptionDetails = "missing_item"
                    missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                    not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                    not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                }
            }
            not_good.setOnClickListener {
                descriptionDetails = "not_good"
                not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
            }
            not_matching.setOnClickListener {
                descriptionDetails = "not_matching"
                not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
            }
            missing_item.setOnClickListener {
                descriptionDetails = "missing_item"
                missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
            }
            productComplaintAdapter = ProductComplaintAdapter(this, complaintList,0)
            rv_recycle.adapter = productComplaintAdapter
            productComplaintAdapter.notifyDataSetChanged()
            complaint_et.visibility = View.GONE
        } else if (intent.hasExtra("order")) {
            order = intent.getSerializableExtra("order") as ShowOrderModel
            orderId = order.order_id.toString()
            userId = mSharedPrefManager?.userData?.user_id.toString()
            providerId = order.shop_id.toString()
            order_time.text = convertLongToTime(order.order_created_at * 1000)
            tv_city.text = order.city_name
            tv_order_number.text = order.order_id.toString()
            order.order_products.forEach {
                complaintList.add(OrderComplaintProduct(null, null,
                        null, it.price, it, it.id, false,
                        null, null, null))
            }
            not_good.setOnClickListener {
                descriptionDetails = "not_good"
                not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
            }
            not_matching.setOnClickListener {
                descriptionDetails = "not_matching"
                not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
            }
            missing_item.setOnClickListener {
                descriptionDetails = "missing_item"
                missing_item.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button_green_border)
                not_matching.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
                not_good.background = ActivityCompat.getDrawable(this, R.drawable.bg_order_details_button)
            }
            productComplaintAdapter = ProductComplaintAdapter(this, complaintList,1)
            rv_recycle.adapter = productComplaintAdapter
            productComplaintAdapter.notifyDataSetChanged()
            complaint_et.visibility = View.VISIBLE
        }

        mLanguagePrefManager = LanguagePreferences(this)
        easyImage = chooseEasyImage()
        act_back.setOnClickListener {
            onBackPressed()
        }
        act_title.text = "شكوي"
        act_back.setOnClickListener {
            onBackPressed()
        }
        choose_button.setOnClickListener {
            easyImage.openChooser(this)
        }

        complaint_et.setOnClickListener {
            createOrderComplaint()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage.handleActivityResult(requestCode, resultCode, data, this,
                object : DefaultCallback() {
                    override fun onMediaFilesPicked(
                            imageFiles: Array<MediaFile>,
                            source: MediaSource
                    ) {
                        chooseImageAndConvert64(imageFiles)
                    }
                })
    }

    private fun createOrderComplaint() {
        RetroWeb.getClient().create(ServiceApi::class.java).createOrderComplaint(
                CreateOrderComplaintRequest(complaint_note.text.toString(),
                        descriptionDetails, orderId,
                        orderComplaintDetails, complaintImage, providerId, userId))
                .enqueue(object : Callback<BaseResponse> {
                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        if (response.isSuccessful) {
                            if (response.body()?.status==200){
                                Toast.makeText(this@ComplaintActivity,"تم الارسال",Toast.LENGTH_LONG).show()
                            }
                            if(response.body()?.status==200){
                                Toast.makeText(this@ComplaintActivity,"تم عمل شكوي من قبل",Toast.LENGTH_LONG).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        CommonUtil.handleException(this@ComplaintActivity, t)
                        t.printStackTrace()
                    }
                })
    }

    override fun itemComplaintChooser(orderComplaintProduct: OrderComplaintProduct, status: String) {
        val orderComplaintDetail = orderComplaintProduct.price?.let { orderComplaintProduct.product_id?.let { it1 -> OrderComplaintDetail(it, it1) } }
        if (status == "add") {
            orderComplaintDetail?.let { orderComplaintDetails.add(it) }
        } else {
            orderComplaintDetails.remove(orderComplaintDetail)
        }
    }

    fun chooseImageAndConvert64(imageFiles: Array<MediaFile>) {
        easyImageResult(
                url = imageFiles[0].file.absolutePath,
                imageView = null,
                image = {
                    complaintImage.add(0, it)
                    path.text = imageFiles[0].file.absolutePath.substringAfterLast("/")
                })
    }

    fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return format.format(date)
    }
}