package com.top.top12user.UI.Adapters;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.ShopProductsModel;
import com.top.top12user.Pereferences.LanguagePreferences;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class ProductAdapter extends ParentRecyclerAdapter<ShopProductsModel> {

    private String errorMsg;
    protected LanguagePreferences mLanguagePrefManager;

    public ProductAdapter(final Context context, final List<ShopProductsModel> data) {
        super(context, data);
        mLanguagePrefManager = new LanguagePreferences(context);
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;


        View viewItem = inflater.inflate(R.layout.recycler_products, parent, false);
        viewHolder = new OffersViewHolder(viewItem);


        return viewHolder;
    }

    String type = "";

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {

        final OffersViewHolder offersViewHolder = (OffersViewHolder) holder;
        ShopProductsModel foodModel = data.get(position);
        Glide.with(mcontext).load(foodModel.getProduct_main_image()).placeholder(R.mipmap.bguser)
                .into(offersViewHolder.ivFoodImage);
        if (data.get(position).is_active) {
//            offersViewHolder.constrain.setVisibility(View.INVISIBLE);
            offersViewHolder.close.setVisibility(View.GONE);

        } else {
//            offersViewHolder.constrain.setVisibility(View.VISIBLE);
            offersViewHolder.close.setVisibility(View.VISIBLE);
        }
        offersViewHolder.tvFoodName.setText(foodModel.getProduct_name());
        offersViewHolder.tvNormalPrice
                .setText(foodModel.getProduct_price() + " " + mcontext.getResources().getString(R.string.Sar));
        if (foodModel.isProduct_have_offer() == true) {
            CommonUtil.setStrokInText(offersViewHolder.tvNormalPrice);
            offersViewHolder.tv_discount_price.setVisibility(View.VISIBLE);
            offersViewHolder.tv_discount_price.setText(foodModel.getProduct_offer_price() + mcontext.getResources().getString(R.string.Sar));
            offersViewHolder.iv_offer.setVisibility(View.VISIBLE);
            offersViewHolder.iv_offer
                    .setText((int) foodModel.getProduct_offer_percent() + "%");
        } else {
            offersViewHolder.tv_discount_price.setVisibility(View.GONE);
            offersViewHolder.iv_offer.setVisibility(View.GONE);
        }


        offersViewHolder.tvDecription.setText(foodModel.getProduct_disc());
        offersViewHolder.tvDiscountPrice.setText(mcontext.getResources().getString(R.string.haveOrdered) + foodModel.getProduct_order_count() + mcontext.getResources().getString(R.string.mara));

        offersViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (data.get(position).is_active) {
                    itemClickListener.onItemClick(view, position);
                }
            }
        });

        if (!mLanguagePrefManager.getAppLanguage() .equals("ar")) {
       //     offersViewHolder.type.setText(foodModel.product_type + "  " + foodModel.product_time);
        } else {
            if (foodModel.product_type.equals( "instant")) {
                type = "مسبق";
                offersViewHolder.type.setBackground(mcontext.getDrawable(R.drawable.instant2));
            } else {
                type = "فورى";
                offersViewHolder.type.setBackground(mcontext.getDrawable(R.drawable.prior2));
            }
          //  offersViewHolder.type.setText(type + "  " + foodModel.product_time);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return data.size();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    /**
     * Main list's content ViewHolder
     */

    protected class OffersViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.iv_food_image)
        ImageView ivFoodImage;

        @BindView(R.id.iv_offer)
        TextView iv_offer;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        @BindView(R.id.type)
        ImageView type;


        @BindView(R.id.tv_normal_price)
        TextView tvNormalPrice;

        @BindView(R.id.tv_cout_order)
        TextView tvDiscountPrice;
        @BindView(R.id.tv_discount_price)
        TextView tv_discount_price;

        @BindView(R.id.tv_decription)
        TextView tvDecription;
        @BindView(R.id.constrain)
        ConstraintLayout constrain;
        @BindView(R.id.not_ava)
        TextView not_ava;
        @BindView(R.id.close)
        ImageView close;

        public OffersViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


}
