package com.top.top12user.UI.Views

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.top.top12user.Models.CheckTransactionModel
import com.top.top12user.Network.RetroWeb
import com.top.top12user.Network.ServiceApi
import com.top.top12user.R
import com.top.top12user.UI.Activities.MainActivity
import com.top.top12user.UI.Activities.ShowOrderActivity
import com.top.top12user.Utils.CommonUtil
import com.top.top12user.Utils.DialogUtil
import kotlinx.android.synthetic.main.payment_dailog.*
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PaymentWebviewDialog(
        val activity: Activity, val url: String,val orderId:Int

) : Dialog(activity),
        View.OnClickListener {
    private var mProgressDialog: ProgressDialog? = null
    override fun onClick(v: View) {
        dismiss()
        EventBus.getDefault().post("return")
    }


    override fun onBackPressed() {
        dismiss()
        EventBus.getDefault().post("return")
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.payment_dailog)
        //   mProgressDialog = DialogUtil.showProgressDialog(activity,
        //  activity.getString(R.string.please_wait), false)
        //  showProgressDialog(activity.getString(R.string.please_wait))
        web_view.settings.javaScriptEnabled = true
        // web_view.webChromeClient = WebChromeClient()
        web_view.loadUrl(url)
        web_view.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return false
            }

            override fun onLoadResource(view: WebView?, url: String?) {
                super.onLoadResource(view, url)
                Log.e("test", url)
                if (url!!.contains("https://top12app.com/api/redirect_url")) {
                    Handler().postDelayed(Runnable { getPaymentStatus(orderId)
                    }, 5000)
                }
            }
        }
//        web_view.webViewClient = object : WebViewClient() {
//            override fun onPageFinished(view: WebView, url: String) {
//             dismiss()
//            }
//        }
        window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
        )

        window?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.decorView?.rootView?.setOnTouchListener { p0, p1 ->
            dismiss()
            true
        }

    }

    protected fun showProgressDialog(message: String?) {
        mProgressDialog = DialogUtil.showProgressDialog(activity, message, false)
    }

    protected fun hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog?.dismiss()
        }
    }
    fun getPaymentStatus(OrderId: Int) {
        RetroWeb.getClient().create(ServiceApi::class.java).getPaymentStatus( OrderId).
        enqueue(object : Callback<CheckTransactionModel?> {
            override fun onResponse(call: Call<CheckTransactionModel?>, response: Response<CheckTransactionModel?>) {
                // hideProgressDialog();
                if (response.isSuccessful)
                    if(response.body()?.transaction_status == "CAPTURED"){
                        val intent = Intent(activity, ShowOrderActivity::class.java)
                        intent.putExtra("id", orderId.toString() + "")
                        activity.startActivity(intent)
                        Toast.makeText(activity,"تم",Toast.LENGTH_LONG).show()
                        activity.finish()
                    }
                    else{
                        val intent = Intent(activity, MainActivity::class.java)
                        activity.startActivity(intent)
                        Toast.makeText(activity,"فشل",Toast.LENGTH_LONG).show()
                        activity.finish()
                    }


            }

            override fun onFailure(call: Call<CheckTransactionModel?>, t: Throwable) {
                CommonUtil.handleException(activity, t)
                t.printStackTrace()
            }
        })
    }
}