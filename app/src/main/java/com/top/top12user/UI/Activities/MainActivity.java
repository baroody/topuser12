package com.top.top12user.UI.Activities;

import android.content.Intent;
import android.os.Build;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.FCM.MyFirebaseMessagingService;
import com.top.top12user.Listeners.AddButtonClicked;
import com.top.top12user.Listeners.AddSearchClicked;
import com.top.top12user.Listeners.ChooseCategoryListner;
import com.top.top12user.Listeners.DrawerListner;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.CategoryModel;
import com.top.top12user.Models.CategoryResponse;
import com.top.top12user.Models.OrdersCountInProgressResponse;
import com.top.top12user.Network.ProviderOrdersProssingRequest;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;

import com.top.top12user.UI.Fragments.ChooseActivitiesDialogFragment;
import com.top.top12user.UI.Fragments.HomeFragment;
import com.top.top12user.UI.Fragments.NavigationFragment;
import com.top.top12user.UI.Fragments.NotificationFragment;
import com.top.top12user.UI.Fragments.OrdersFragment;
import com.top.top12user.Utils.BottomNavigationViewHelper;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Stack;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends ParentActivity
        implements DrawerListner, OnItemClickListener, BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener, ChooseCategoryListner {
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout app_bar;
    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.order_number)
    TextView order_number;

    @OnClick(R.id.chat)
    void onChatClick() {
        ChatsActivity.startActivity((AppCompatActivity) mContext);
    }

    NavigationFragment mNavigationFragment;

    HomeFragment mHomeFragment;

    OrdersFragment mOrdersFragment;

    NotificationFragment mOffersFragment;
    // ArrayList<ListModel1> mFamiliesModel;

    //  ListModel1 familyModel = null;

    int selectedTab = 0;

    private Stack<Integer> tabsStack = new Stack<>();

    private FragmentManager fragmentManager;

    private FragmentTransaction transaction;
    @BindView(R.id.iv_search)
    ImageView iv_search;
    @BindView(R.id.iv_filter)
    ImageView iv_filter;
    String id;
    String CategoriesList;
    private static View notificationBadge;

    TextView count;


//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        recreate();
//    }

    @Override
    protected void initializeComponents() {

        mNavigationFragment = NavigationFragment.newInstance();
        mNavigationFragment.setDrawerListner(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_view, mNavigationFragment).commit();
        initializeBottomNav();
        //  BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
//        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigation.getChildAt(0);
//        BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(2);
//        notificationBadge = LayoutInflater.from(this).inflate(R.layout.notification_lay,menuView,false);
//        itemView.addView(notificationBadge);
//        count = notificationBadge.findViewById(R.id.count);
//        if (MyFirebaseMessagingService.count==0){
//            count.setVisibility(View.GONE);
//        }else {
//            count.setVisibility(View.VISIBLE);
//            count.setText(MyFirebaseMessagingService.count + "");
//        }
    }

    public void initializeBottomNav() {

        mHomeFragment = HomeFragment.newInstance();
        mOrdersFragment = OrdersFragment.newInstance();
        mOffersFragment = NotificationFragment.newInstance();

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.home_fragment_container, mHomeFragment);
        transaction.add(R.id.home_fragment_container, mOrdersFragment);
        transaction.add(R.id.home_fragment_container, mOffersFragment);
        transaction.commit();
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        bottomNavigation.setOnNavigationItemReselectedListener(this);
        bottomNavigation.setSelectedItemId(R.id.nav_home);
        showHome(true);
    }

    @OnClick(R.id.menu)
    void onMenuClick() {
        this.OpenCloseDrawer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSharedPrefManager.getLoginStatus()) {
            CircleImageView rivImage = findViewById(R.id.riv_image);
            TextView tvDistance = findViewById(R.id.tv_distance);
            CircleImageView edit = findViewById(R.id.edit);
            edit.setOnClickListener(v -> startActivity(new Intent(mContext, ProfileActivity.class)));

            //
            Glide.with(mContext).load(mSharedPrefManager.getUserData().getAvatar()).placeholder(R.mipmap.bguser).into(rivImage);
            tvDistance.setText(mSharedPrefManager.getUserData().getName());
            providerOrdersProssing();
        }
    }

    private void showOffers(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mHomeFragment);
        transaction.hide(mOrdersFragment);
        transaction.show(mOffersFragment);
        transaction.commit();
        selectedTab = R.id.nav_notification;
        iv_filter.setVisibility(View.GONE);
        iv_search.setVisibility(View.GONE);
        tv_title.setText(R.string.notification);
        toolbar.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        showParElevation(true);

    }

    private void showOrders(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mOffersFragment);
        transaction.hide(mHomeFragment);
        transaction.show(mOrdersFragment);
        transaction.commit();
        selectedTab = R.id.nav_order;
        tv_title.setText(R.string.my_orders);
        iv_filter.setVisibility(View.GONE);
        iv_search.setVisibility(View.GONE);
        toolbar.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        showParElevation(true);

    }

    private void showHome(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mOrdersFragment);
        transaction.hide(mOffersFragment);
        transaction.show(mHomeFragment);
        transaction.commit();
        selectedTab = R.id.nav_home;
        iv_search.setVisibility(View.VISIBLE);
        tv_title.setText(R.string.home);
        iv_filter.setVisibility(View.VISIBLE);
        toolbar.setBackground(mContext.getResources().getDrawable(R.mipmap.bg));
        showParElevation(false);
    }

    public void showParElevation(boolean showHide) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (showHide) {
                app_bar.setElevation((float) 5.0);

            } else {
                app_bar.setElevation((float) 0.0);
            }
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onBackPressed() {
        if (tabsStack.size() > 0) {
            bottomNavigation.setOnNavigationItemSelectedListener(null);
            int selectedTab = tabsStack.pop();
            bottomNavigation.setSelectedItemId(selectedTab);
            switch (selectedTab) {
                case R.id.nav_home:
                    showHome(false);
                    break;
                case R.id.nav_order:
                    showOrders(false);
                    break;
                case R.id.nav_notification:
                    showOffers(false);
                    break;
            }
            bottomNavigation.setOnNavigationItemSelectedListener(this);
        } else {
            MainActivity.this.finish();
        }

    }


    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                showHome(true);
                break;
            case R.id.nav_order:
                showOrders(true);
                break;
            case R.id.nav_notification:
                showOffers(true);
                break;
        }
        return true;
    }

    @Override
    public void OpenCloseDrawer() {
        mNavigationFragment.setNavData();
        if (drawerLayout != null) {
            if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            } else {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @OnClick(R.id.iv_filter)
    void onFilterClick() {
        EventBus.getDefault().post(new AddButtonClicked());
    }

    @OnClick(R.id.iv_search)
    void onSearch() {
        EventBus.getDefault().post(new AddSearchClicked());
    }

    @Override
    public void ChooseCategories(ArrayList<CategoryModel> categoryModels) {
        setCategoryName(categoryModels);
    }

    private void getCategories() {
        showProgressDialog(getResources().getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategorys(mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<CategoryResponse>() {
                    @Override
                    public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus() == 1) {
                                if (response.body().getData().size() != 0) {
                                    showCategories(getSupportFragmentManager(), response.body().getData());
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });
    }

    private void providerOrdersProssing() {
        RetroWeb.getClient().create(ServiceApi.class).
                providerOrdersProssing(
                        new ProviderOrdersProssingRequest(mSharedPrefManager.getUserData().getUser_id()))
                .enqueue(new Callback<OrdersCountInProgressResponse>() {
                    @Override
                    public void onResponse(Call<OrdersCountInProgressResponse> call, Response<OrdersCountInProgressResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus() == 200) {
                                if(response.body().getOrdersCountInProgress().equals("0")){
                                    order_number.setVisibility(View.GONE);

                                }else{
                                    order_number.setVisibility(View.VISIBLE);
                                    order_number.setText(response.body().getOrdersCountInProgress());
                                }

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<OrdersCountInProgressResponse> call, Throwable t) {
                        CommonUtil.handleException(MainActivity.this, t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });
    }

    void setCategoryName(ArrayList<CategoryModel> categoryName) {
        id = "";
        int i = 0;
        for (CategoryModel categoryModel : categoryName) {
            i++;
            if (categoryModel.isCheckCategory()) {
                if (i != categoryName.size()) {
                    id = id + categoryModel.getId() + ",";
                } else {
                    id = id + categoryModel.getId() + "";

                }
            }
        }
        Log.e("id", id);
        CategoriesList = id;
        CommonUtil.PrintLogE("Categories" + CategoriesList);
    }

    void showCategories(FragmentManager mFragmentManager, ArrayList<CategoryModel> categoryModels) {
        CommonUtil.PrintLogE("Categories size : " + categoryModels.size());
        ChooseActivitiesDialogFragment chooseActivitiesDialogFragment = ChooseActivitiesDialogFragment
                .newInstance(categoryModels);
        chooseActivitiesDialogFragment.show(mFragmentManager, "Choose Categories");
        chooseActivitiesDialogFragment.setListner(this);
    }
}
