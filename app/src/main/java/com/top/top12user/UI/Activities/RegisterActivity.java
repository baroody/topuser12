package com.top.top12user.UI.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.top.top12user.App.Constant;
import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.ListModel;
import com.top.top12user.Models.ListResponse;
import com.top.top12user.Models.RegisterResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Views.ListDialog;
import com.top.top12user.Utils.CommonUtil;
import com.top.top12user.Utils.PermissionUtils;
import com.top.top12user.Utils.ProgressRequestBody;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.top.top12user.App.Constant.RequestPermission.REQUEST_IMAGES;

public class RegisterActivity extends ParentActivity implements OnItemClickListener, ProgressRequestBody.UploadCallbacks {

    @BindView(R.id.civ_profile)
    CircleImageView civ_profile;
    @BindView(R.id.ed_user_name)
            EditText ed_user_name;
    @BindView(R.id.ed_password)
            EditText ed_password;
    @BindView(R.id.ed_confirm_password)
            EditText ed_confirm_password;
    @BindView(R.id.ed_email)
            EditText ed_email;
    @BindView(R.id.ed_phone)
            EditText ed_phone;
    @BindView(R.id.txt_login)
    TextView txt_login;
    @BindView(R.id.ed_city)
            TextView ed_city;
    ListDialog listDialog;
    ArrayList<ListModel> listModels = new ArrayList<>();
    ListModel cityModel;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;



    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }
    @OnClick(R.id.txt_login)
    void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));
    }

    @OnClick(R.id.btn_register)
    void onRegisterClick(){
        if (Validate()){
            if (ImageBasePath==null){
            if (ed_email.getText().toString().trim().isEmpty()){
                Log.e("bbb","nnn");
                Register();
            }else {
                String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
                Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(ed_email.getText().toString());
                if (matcher.matches()==true){
                    Register1(ed_email.getText().toString());
                }else {
                    CommonUtil.makeToast(mContext,getString(R.string.email_error));
                }
            }}else if (ImageBasePath!=null){
                if (ed_email.getText().toString().trim().isEmpty()){
                    Log.e("bbb","nnn");
                    Register(ImageBasePath);
                }else {
                    String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
                    Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                    Matcher matcher = pattern.matcher(ed_email.getText().toString());
                    if (matcher.matches()==true){
                        Register(ImageBasePath,ed_email.getText().toString());
                    }else {
                        CommonUtil.makeToast(mContext,getString(R.string.email_error));
                    }
                }
            }
        }
       // startActivity(new Intent(getApplicationContext(),ConfirmCodeActivity.class));
    }



    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    boolean Validate(){
         if (ed_user_name.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.enter_name));
            return false;
        }else if (ed_password.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.enter_password));
            return false;
        }else if (!ed_password.getText().toString().equals(ed_confirm_password.getText().toString())){
            CommonUtil.makeToast(mContext,getString(R.string.not_match));
            return false;
        }else if (ed_phone.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.enter_phone));
            return false;
        }else if (ed_city.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.choose_city));
            return false;
        }
        return true;
    }


    @OnClick(R.id.ed_city)
    void onCityClick(){

        getCities();
    }
    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        listModels = response.body().getData();
                        listDialog = new ListDialog(mContext,RegisterActivity.this,listModels,getString(R.string.city));
                        listDialog.show();

                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(getApplicationContext(),t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.civ_profile)
    void onProvileClick(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        @SuppressLint("ResourceType") TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civ_profile.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(true)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        cityModel = listModels.get(position);
        ed_city.setText(cityModel.getName());
        CommonUtil.PrintLogE("City id : " + cityModel.getId());
    }
    private void Register(String PathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).registerwithoutemail(mLanguagePrefManager.getAppLanguage(),1,ed_user_name.getText().toString(),ed_password.getText().toString(),ed_phone.getText().toString(),cityModel.getId(),"0.0","0.0",filePart).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ConfirmCodeActivity.class);
                        intent.putExtra("user_id",response.body().getData().getUser_id()+"");
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void Register(){
        showProgressDialog(getString(R.string.please_wait));

        RetroWeb.getClient().create(ServiceApi.class).registerwithoutemailwithotimage(mLanguagePrefManager.getAppLanguage(),1,ed_user_name.getText().toString(),ed_password.getText().toString(),ed_phone.getText().toString(),cityModel.getId(),"0.0","0.0").enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ConfirmCodeActivity.class);
                        intent.putExtra("user_id",response.body().getData().getUser_id()+"");
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void Register(String PathFromImage,String email){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).register(mLanguagePrefManager.getAppLanguage(),
                1,ed_user_name.getText().toString(),ed_password.getText().toString(),email,ed_phone.getText().toString(),cityModel.getId(),"0.0","0.0",filePart).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ConfirmCodeActivity.class);
                        intent.putExtra("user_id",response.body().getData().getUser_id()+"");
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void Register1(String email){
        showProgressDialog(getString(R.string.please_wait));

        RetroWeb.getClient().create(ServiceApi.class).registerwitoutimage(mLanguagePrefManager.getAppLanguage(),1,ed_user_name.getText().toString(),ed_password.getText().toString(),email,ed_phone.getText().toString(),cityModel.getId(),"0.0","0.0").enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ConfirmCodeActivity.class);
                        intent.putExtra("user_id",response.body().getData().getUser_id()+"");
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
