package com.top.top12user.UI.Activities;

import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.CommentsModel;
import com.top.top12user.Models.RateResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.RateAdapter;
import com.top.top12user.Utils.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatesActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.rating_bar_rate)
    AppCompatRatingBar ratingBarRate;
    @BindView(R.id.cart)
    CardView cart;

    @BindView(R.id.ed_add_rate)
    EditText edAddRate;


    LinearLayoutManager linearLayoutManager;

    RateAdapter mRateAdapter;

    List<CommentsModel> mRateModels = new ArrayList<>();

    @BindView(R.id.coment_recycler)
    RecyclerView coment_recycler;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    String shop_id;
    String type;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.rate));
        shop_id = getIntent().getStringExtra("shop_id");
        type = getIntent().getStringExtra("type");
        if (type.equals("1")){
            cart.setVisibility(View.GONE);
        }else if (type.equals("2")){
            cart.setVisibility(View.VISIBLE);
        }
//        Log.e("shop",shop_id);
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        coment_recycler.setLayoutManager(linearLayoutManager);
        mRateAdapter = new RateAdapter(mContext, mRateModels, R.layout.recycle_rate_row_family);
        coment_recycler.setAdapter(mRateAdapter);
        getComments();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_rate;
    }
    @OnClick(R.id.btn_rate)
    void onRateClick(){
        if (edAddRate.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.write_your_comment));
        }else {
            Rate();
        }
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void Rate(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).rate(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(shop_id),(int)ratingBarRate.getRating(),edAddRate.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(RatesActivity.this,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getComments(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class)
                .getComments(mLanguagePrefManager.getAppLanguage()
                        ,mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(shop_id))
                .enqueue(new Callback<RateResponse>() {
            @Override
            public void onResponse(Call<RateResponse> call, Response<RateResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().getComments().size()==0){

                        }else {
                            mRateAdapter.updateAll(response.body().getData().getComments());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<RateResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
