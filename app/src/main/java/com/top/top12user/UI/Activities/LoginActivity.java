package com.top.top12user.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.widget.EditText;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.FCM.MyFirebaseInstanceIDService;
import com.top.top12user.Models.LoginResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ParentActivity {
    @BindView(R.id.txt_register)
    TextView register;
    @BindView(R.id.ed_user_name)
    EditText ed_user_name;
    @BindView(R.id.ed_password)
    EditText ed_password;

    @OnClick(R.id.txt_register)
    void onRegisterClick(){
        startActivity(new Intent(getApplicationContext(),RegisterActivity.class));
    }
    @OnClick(R.id.txt_forgotpass)
    void onForgotPassActivity(){
        startActivity(new Intent(getApplicationContext(),ForgotPasswordActivity.class));
    }
    @OnClick(R.id.btn_login)
    void onLoginClick(){
        if (ed_user_name.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.enter_phone));
        }else if (ed_password.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.enter_password));
        }else {
          //  visitor();
            login();
        }
    }
@OnClick(R.id.btn_provider)
void onProvider(){
    startActivity(new Intent(Intent.ACTION_VIEW,
            Uri.parse("https://play.google.com/store/apps/details?id=com.aait.top12provider")));
}
    @Override
    protected void initializeComponents() {

    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }



    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    private void login(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).login(
                mLanguagePrefManager.getAppLanguage(),ed_user_name.getText().toString()
                ,ed_password.getText().toString(),"2",
                MyFirebaseInstanceIDService.getToken(mContext)).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().getType().equals("client")) {
                            mSharedPrefManager.setUserData(response.body().getData());
                            mSharedPrefManager.setLoginStatus(true);
                            startActivity(new Intent(mContext, DetectLocationActivity.class));
                        }
                        else {
                            CommonUtil.makeToast(mContext,getString(R.string.you_not_client));
                        }
                    }else if (response.body().getStatus()==2){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ConfirmCodeActivity.class);
                        intent.putExtra("user_id",response.body().getData().getUser_id()+"");
                        startActivity(intent);
                        ed_password.setText("");
                        ed_user_name.setText("");
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LoginActivity.this.finish();
    }
}
