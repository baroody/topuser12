package com.top.top12user.UI.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.OrderProductsModel;
import com.top.top12user.R;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class OrderDetailsAdapter extends ParentRecyclerAdapter<OrderProductsModel> {

    public OrderDetailsAdapter(final Context context, final List<OrderProductsModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        OrderProductsModel foodModel = data.get(position);


        viewHolder.tvItemName.setText(foodModel.getProduct_name());
        viewHolder.tvItemPrice.setText(foodModel.getProduct_price()+mcontext.getResources().getString(R.string.Sar));



        // check if the food has discount or not .


        // if additions equal zero

//        holder.title.setText(mData.get(position).getTitle());
        viewHolder.mFamilyOrderSpecialAdditiveAdapter.setData(data.get(position).getProduct_additions()); // List of Strings
        viewHolder.mFamilyOrderSpecialAdditiveAdapter.setRowIndex(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        private FamilyOrderSpecialAdditiveAdapter1 mFamilyOrderSpecialAdditiveAdapter;



        @BindView(R.id.tv_item_price)
        TextView tvItemPrice;

        @BindView(R.id.tv_item_name)
        TextView tvItemName;


        @BindView(R.id.tv_recycle)
        RecyclerView tvRecycle;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            tvRecycle.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false));
            mFamilyOrderSpecialAdditiveAdapter = new FamilyOrderSpecialAdditiveAdapter1(mcontext);
            tvRecycle.setAdapter(mFamilyOrderSpecialAdditiveAdapter);
        }

    }
}
