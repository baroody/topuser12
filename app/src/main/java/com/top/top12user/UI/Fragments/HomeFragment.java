package com.top.top12user.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;

import com.top.top12user.Base.BaseFragment;
import com.top.top12user.Listeners.AddButtonClicked;
import com.top.top12user.Listeners.AddSearchClicked;
import com.top.top12user.Listeners.ChooseCategoryListner;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.CategoryModel;
import com.top.top12user.Models.CategoryResponse;
import com.top.top12user.Models.HeaderResponse;
import com.top.top12user.Models.ShopsModel;
import com.top.top12user.Models.ShopsResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Activities.ShopActivity;
import com.top.top12user.UI.Adapters.FooterApater;
import com.top.top12user.UI.Adapters.RecyclerPoupupAds;
import com.top.top12user.UI.Adapters.ShopsAdapter;
import com.top.top12user.UI.Views.CloseShopDialog;
import com.top.top12user.Utils.CommonUtil;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment implements OnItemClickListener, ChooseCategoryListner {
    @BindView(R.id.rv_recycle)
    RecyclerView osar_recycler;
    @BindView(R.id.ads_recycler)
    RecyclerView ads_recycler;
    @BindView(R.id.ads_lay)
    ConstraintLayout images;
    @BindView(R.id.viewpager1)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    InkPageIndicator indicator;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh;
    ArrayList<ShopsModel> shopsModels = new ArrayList<>();
    LinearLayoutManager gridLayoutManager;
    ShopsAdapter shopsAdapter;
    GridLayoutManager gridLayoutManager1;
    FooterApater footerApater;
    List<String> footer = new ArrayList<>();
    List<String> header = new ArrayList<>();
    RecyclerPoupupAds recyclerPoupupAds;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    String id;
    String CategoriesList = null;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    private AppCompatActivity activity;


    @Override
    public void onClick(View v) {
        startActivity(new Intent(getActivity(), ShopActivity.class));
    }

    private void getShops() {
        if(!swipe_refresh.isRefreshing())
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getShop(mLanguagePrefManager.getAppLanguage(),
                mSharedPrefManager.getUserData().getUser_id(), null).
                enqueue(new Callback<ShopsResponse>() {
                    @Override
                    public void onResponse(Call<ShopsResponse> call, final Response<ShopsResponse> response) {
                        hideProgressDialog();
                        swipe_refresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getStatus() == 1) {
                                if (response.body().getData().size() == 0) {
                                    CommonUtil.makeToast(mContext, getString(R.string.no_data));
                                } else {
                                    shopsModels = response.body().getData();
                                    shopsAdapter.updateAll(response.body().getData());
                                    shopsAdapter.setOnItemClickListener(new OnItemClickListener() {
                                        @Override
                                        public void onItemClick(View view, int position) {
                                            if (response.body().getData().get(position).shop_open) {
                                                Intent intent = new Intent(getActivity(), ShopActivity.class);
                                                intent.putExtra("shop_id", response.body().getData().get(position).getShop_id());
                                                startActivity(intent);
                                            } else {
                                                new CloseShopDialog(requireActivity()).show();

                                            }

                                        }
                                    });
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ShopsResponse> call, Throwable t) {
                        CommonUtil.handleException(getContext(), t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });


    }
//    @OnClick(R.id.iv_filter)
//    void onFilterClick(){
//        getCategories();
//    }
//    @OnClick(R.id.iv_search)
//    void onSearchClick(){
//        showProgressDialog(getString(R.string.please_wait));
//        RetroWeb.getClient().create(ServiceApi.class).getShop(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),CategoriesList).enqueue(new Callback<ShopsResponse>() {
//            @Override
//            public void onResponse(Call<ShopsResponse> call, final Response<ShopsResponse> response) {
//                hideProgressDialog();
//                if (response.isSuccessful()){
//                    if (response.body().getStatus()==1){
//                        if (response.body().getData().size()==0){
//
//                        }else {
//                            shopsModels =response.body().getData();
//                            shopsAdapter.updateAll(response.body().getData());
//                            shopsAdapter.setOnItemClickListener(new OnItemClickListener() {
//                                @Override
//                                public void onItemClick(View view, int position) {
//                                    Intent intent = new Intent(getActivity(),ShopActivity.class);
//                                    intent.putExtra("shop_id",response.body().getData().get(position).getShop_id());
//                                    startActivity(intent);
//                                }
//                            });
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ShopsResponse> call, Throwable t) {
//                CommonUtil.handleException(getContext(),t);
//                t.printStackTrace();
//                hideProgressDialog();
//
//            }
//        });
//    }

    private void getHeader() {
        RetroWeb.getClient().create(ServiceApi.class).getHeader().enqueue(new Callback<HeaderResponse>() {
            @Override
            public void onResponse(Call<HeaderResponse> call, Response<HeaderResponse> response) {
                if (!swipe_refresh.isRefreshing())
                    hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().size() == 0) {
                            CommonUtil.makeToast(mContext, getString(R.string.no_data));
                        } else {
                            header = response.body().getData();
                            recyclerPoupupAds = new RecyclerPoupupAds(getContext(), header);
                            viewPager.setAdapter(recyclerPoupupAds);

                            indicator.setViewPager(viewPager);

                            NUM_PAGES = recyclerPoupupAds.getCount();
                            final Handler handler = new Handler();
                            final Runnable update = () -> {
                                if (currentPage == NUM_PAGES) {
                                    currentPage = 0;
                                }
                                viewPager.setCurrentItem(currentPage++, true);
                            };
                            Timer swipeTimer = new Timer();
                            swipeTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handler.post(update);
                                }
                            }, 2500, 2500);

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<HeaderResponse> call, Throwable t) {
                CommonUtil.handleException(getContext(), t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    private void getFooter() {
        RetroWeb.getClient().create(ServiceApi.class).getFooter().enqueue(new Callback<HeaderResponse>() {
            @Override
            public void onResponse(Call<HeaderResponse> call, Response<HeaderResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        if (response.body().getData().size() == 0) {
                            CommonUtil.makeToast(mContext, getString(R.string.no_data));
                        } else {
                            footer = response.body().getData();
                            footerApater.updateAll(response.body().getData());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<HeaderResponse> call, Throwable t) {
                CommonUtil.handleException(getContext(), t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    void setCategoryName(ArrayList<CategoryModel> categoryName) {
        id = "";
        int i = 0;
        for (CategoryModel categoryModel : categoryName) {
            i++;
            if (categoryModel.isCheckCategory()) {
                if (i != categoryName.size()) {
                    id = id + categoryModel.getId() + ",";
                } else {
                    id = id + categoryModel.getId() + "";

                }
            }
        }
        Log.e("id", id);
        CategoriesList = id;
        CommonUtil.PrintLogE("Categories" + CategoriesList);
    }

    @Override
    public void ChooseCategories(ArrayList<CategoryModel> categoryModels) {
        setCategoryName(categoryModels);
    }

    private void getCategories() {
        if (!swipe_refresh.isRefreshing())
            showProgressDialog(getResources().getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategorys(mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<CategoryResponse>() {
                    @Override
                    public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus() == 1) {
                                if (response.body().getData().size() != 0) {
                                    showCategories(activity.getSupportFragmentManager(), response.body().getData());
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryResponse> call, Throwable t) {
                        CommonUtil.handleException(getContext(), t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });
    }






    void showCategories(FragmentManager mFragmentManager, ArrayList<CategoryModel> categoryModels) {
        CommonUtil.PrintLogE("Categories size : " + categoryModels.size());
        ChooseActivitiesDialogFragment chooseActivitiesDialogFragment = ChooseActivitiesDialogFragment
                .newInstance(categoryModels);
        chooseActivitiesDialogFragment.show(mFragmentManager, "Choose Categories");
        chooseActivitiesDialogFragment.setListner(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onButtonClicked(AddButtonClicked addButtonClicked) {
        getCategories();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSearchClicked(AddSearchClicked addSearchClicked) {
        searchShop();
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        searchShop();
//    }

    private void searchShop() {
        if (!swipe_refresh.isRefreshing())
            showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getShop(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getUser_id(), CategoriesList).enqueue(
                new Callback<ShopsResponse>() {
                    @Override
                    public void onResponse(Call<ShopsResponse> call, final Response<ShopsResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus() == 1) {
                                if (response.body().getData().size() == 0) {

                                } else {
                                    shopsAdapter.updateAll(response.body().getData());
                                    shopsAdapter.setOnItemClickListener(new OnItemClickListener() {
                                        @Override
                                        public void onItemClick(View view, int position) {
                                            if(response.body().getData().get(position).shop_open){
                                                Intent intent = new Intent(getActivity(), ShopActivity.class);
                                                intent.putExtra("shop_id", response.body().getData().get(position).getShop_id());
                                                startActivity(intent);
                                            }else{
                                                new CloseShopDialog(requireActivity()).show();
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ShopsResponse> call, Throwable t) {
                        CommonUtil.handleException(getContext(), t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                }
        );
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initializeComponents(View view) {

        activity = (AppCompatActivity) (HomeFragment.this).getActivity();
        gridLayoutManager = new LinearLayoutManager(getContext());
        shopsAdapter = new ShopsAdapter(getContext(), shopsModels, R.layout.recycler_shops);
        osar_recycler.setLayoutManager(gridLayoutManager);
        shopsAdapter.setOnItemClickListener1(this);
        osar_recycler.setNestedScrollingEnabled(false);
        osar_recycler.setAdapter(shopsAdapter);
        gridLayoutManager1 = new GridLayoutManager(getContext(), 3);
        footerApater = new FooterApater(getContext(), footer, R.layout.recycler_footer);
        ads_recycler.setLayoutManager(gridLayoutManager1);
        ads_recycler.setAdapter(footerApater);
        swipe_refresh.setOnRefreshListener(() -> {
               //     shopsModels.clear();
            shopsAdapter.getData().clear();
                    shopsAdapter.notifyDataSetChanged();

                //    this.requireActivity().recreate();
            getShops();
                }

        );
        getHeader();
        //  getFooter();
        getShops();
    }



    @Override
    protected boolean isRecycle() {
        return false;
    }
}
