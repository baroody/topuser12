package com.top.top12user.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.top.top12user.R;
import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.NavigationModel;

import java.util.List;

import butterknife.BindView;

/**
 *created by ahmed el_sayed 1/11/2018
 */
public class NavigationDrawerAdapter extends ParentRecyclerAdapter<NavigationModel> {

    public NavigationDrawerAdapter(final Context context, final List<NavigationModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setClickableRootView(holder.layNav);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        NavigationModel navigationModel = data.get(position);
        viewHolder.tvNavName.setText(navigationModel.getNavTitle());
        viewHolder.iv_menu_item_icon.setImageResource(navigationModel.getNatIcon());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_nav_name)
        TextView tvNavName;

        @BindView(R.id.iv_menu_item_icon)
        ImageView iv_menu_item_icon;

        @BindView(R.id.lay_nav)
        RelativeLayout layNav;

        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

