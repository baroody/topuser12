package com.top.top12user.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.ShopsModel;
import com.top.top12user.R;
import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;

public class ShopsAdapter extends ParentRecyclerAdapter<ShopsModel> {

    public ShopsAdapter(final Context context, final List<ShopsModel> data, final int layoutId) {
        super(context, data, layoutId);

    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;

        ShopsModel categoryModel = data.get(position);
        if(!categoryModel.shop_open){
            viewHolder.ad.setVisibility(View.VISIBLE);
            viewHolder.ad.setText(mcontext.getString(R.string.closed));
            viewHolder.ad.setBackgroundColor(ActivityCompat.getColor(mcontext,R.color.gray2));
        }


        if (categoryModel.shop_featured && categoryModel.shop_open){
            viewHolder.ad.setVisibility(View.VISIBLE);
            viewHolder.ad.setText(mcontext.getString(R.string.ad));
            viewHolder.ad.setBackgroundColor(ActivityCompat.getColor(mcontext,R.color.colorYellow));
        }




        viewHolder.tvCatName.setText(categoryModel.getShop_name());
        viewHolder.distance.setText(categoryModel.shop_distance.toString()+" "+ mcontext.getString(R.string.km));
        viewHolder.rate.setText(categoryModel.rate.toString());
        viewHolder.price.setText(categoryModel.price.toString()+" "+ mcontext.getString(R.string.rs));
       // viewHolder.tvCount.setText(MessageFormat.format("{0}",(categoryModel.getProducts_count()))+" "+mcontext.getResources().getString(R.string.product));
      if(categoryModel.category_name!=null) {
          viewHolder.tvCount.setText(categoryModel.category_name.trim().substring(0, categoryModel.category_name.length() - 2));
      }
       if(categoryModel.have_delegate){
           viewHolder.haveDriver.setText(mcontext.getString(R.string.have_driver));
           viewHolder.haveDriver.setCompoundDrawablePadding(10);
           viewHolder.haveDriver.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_car,0,0,0);
       }else{
           viewHolder.haveDriver.setText(mcontext.getString(R.string.delivery_family));
           viewHolder.haveDriver.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_double_chevron,0,0,0);
           viewHolder.haveDriver.setCompoundDrawablePadding(10);
       }

        Glide.with(mcontext).load(categoryModel.getShop_image()).placeholder(R.mipmap.bguser)
                .into(viewHolder.ivCatImage);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    double roundTwoDecimals(double d)
    {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.parseDouble(twoDForm.format(d));
    }
    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.iv_cat_image)
        ImageView ivCatImage;

        @BindView(R.id.tv_osra_name)
        TextView tvCatName;
        @BindView(R.id.tv_cout)
        TextView tvCount;
        @BindView(R.id.destance)
        TextView distance;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.rate)
        TextView rate;
        @BindView(R.id.textView2)
        TextView haveDriver;
        @BindView(R.id.ad)
        TextView ad;



        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}

