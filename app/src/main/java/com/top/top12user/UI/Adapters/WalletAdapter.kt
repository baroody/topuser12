package com.top.top12user.UI.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.top.top12user.Models.EWalletAction

import com.top.top12user.R
import kotlinx.android.synthetic.main.item_wallet.view.*

class WalletAdapter(private val mActivity: Activity, private val walletList: ArrayList<EWalletAction>) :
        RecyclerView.Adapter<WalletAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(mActivity).inflate(R.layout.item_wallet,
                    parent, false))


    override fun getItemCount(): Int = walletList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView14.text = walletList[position].orderId.toString()
        holder.textView15.text = walletList[position].createAt.toString()
        holder.textView16.text = walletList[position].transAction.toString()
        if (walletList[position].status == "pay_money") {
          //  holder.constrain.setBackgroundColor(ActivityCompat.getColor(mActivity, R.color.material_green_200))
            holder.textView17.text ="دفع"
        } else {
            holder.constrain.setBackgroundColor(ActivityCompat.getColor(mActivity, R.color.material_green_200))
            holder.textView17.text ="أسترجاع"
        }
//        if (walletList[position].isEbay) {
//            holder.textView17.text ="دفع الكترونى"
//        } else {
//            holder.textView17.text ="محفظة"
//        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView14: TextView = view.textView14
        val textView15: TextView = view.textView15
        val textView16: TextView = view.textView16
        val textView17: TextView = view.textView17
        val constrain: ConstraintLayout = view.constrain


    }
}