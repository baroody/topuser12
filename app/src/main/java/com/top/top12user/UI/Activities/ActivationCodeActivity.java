package com.top.top12user.UI.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.EditText;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Models.CheckResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivationCodeActivity extends ParentActivity {

    @BindView(R.id.ed_activation)
    EditText ed_activation;

    String user_id;

    @Override
    protected void initializeComponents() {
        user_id = getIntent().getStringExtra("user_id");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_activation_code;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }



    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }




    @OnClick(R.id.btn_activate)
    void onContinueClick(){
        if (ed_activation.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.activation));
        }else {
            check();
        }
    }
    private void check(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).check(mLanguagePrefManager.getAppLanguage(),
                Integer.parseInt(user_id),
                ed_activation.getText().toString()).enqueue(new Callback<CheckResponse>() {
            @Override
            public void onResponse(Call<CheckResponse> call, Response<CheckResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        Intent intent = new Intent(mContext,NewPasswordActivity.class);
                        intent.putExtra("user_id",response.body().getData().getUser_id()+"");
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
}
