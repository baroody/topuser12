package com.top.top12user.UI.Fragments;

import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.top.top12user.Base.BaseFragment;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.CategoryModel;
import com.top.top12user.Models.CategoryResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Views.CategoryDialog;
import com.top.top12user.Utils.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMain extends BaseFragment implements BottomNavigationView.OnNavigationItemSelectedListener,OnItemClickListener
         {
    @BindView(R.id.bottom_navigation)
    BottomNavigationView btm_navigation_view;
  //  TextView tv_title;
    androidx.appcompat.widget.Toolbar toolbar;
   // ImageView iv_filter;

   // ImageView iv_basket;

  //  ImageView iv_search;
    CategoryDialog categoryDialog;
    ArrayList<CategoryModel> categoryModels = new ArrayList<>();


    private AppCompatActivity activity;

    public FragmentMain() {
        // Required empty public constructor
    }






    private void setInitialHome() {
        HomeFragment fragment = new HomeFragment();
        //iv_search.setVisibility(View.VISIBLE);
//        iv_basket.setVisibility(View.VISIBLE);
//        tv_title.setText(R.string.nav_home_title);
        startFragment(fragment);
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_home:
                fragment = new HomeFragment();
//                tv_title.setText(getString(R.string.home));
//                iv_basket.setVisibility(View.GONE);
////                iv_basket.setVisibility(View.VISIBLE);
//                tv_title.setText(item.getTitle());
                break;

            case R.id.nav_order:
                fragment = new OrdersFragment();
//                tv_title.setText(getString(R.string.my_orders));
//                iv_basket.setVisibility(View.GONE);
//                iv_filter.setVisibility(View.GONE);
//                iv_search.setVisibility(View.GONE);
//                toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                iv_basket.setVisibility(View.VISIBLE);
//                tv_title.setText(item.getTitle());
                break;

            case R.id.nav_notification:
               fragment = new NotificationFragment();
//               tv_title.setText(getString(R.string.notification));
//               iv_filter.setVisibility(View.GONE);
//               iv_basket.setVisibility(View.GONE);
//               iv_search.setVisibility(View.GONE);
//                toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                iv_basket.setVisibility(View.VISIBLE);
//                tv_title.setText(item.getTitle());
                break;


        }
        return startFragment(fragment);
    }

    private boolean startFragment(Fragment fragment) {
        if (fragment != null) {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.home_fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

             private void getCategory(){
                 showProgressDialog(getString(R.string.please_wait));
                 RetroWeb.getClient().create(ServiceApi.class).getCategorys(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CategoryResponse>() {
                     @Override
                     public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                         hideProgressDialog();
                         if (response.isSuccessful()){
                             if (response.body().getStatus()==1){
                                 categoryModels = response.body().getData();
                                 categoryDialog = new CategoryDialog(getContext(),FragmentMain.this,categoryModels);
                                 categoryDialog.show();

                             }else {
                                 CommonUtil.makeToast(getContext(),response.body().getMsg());
                             }
                         }
                     }

                     @Override
                     public void onFailure(Call<CategoryResponse> call, Throwable t) {
                         CommonUtil.handleException(getContext(),t);
                         t.printStackTrace();
                         hideProgressDialog();

                     }
                 });

             }

             @Override
             public void onItemClick(View view, int position) {

             }

             @Override
             protected int getLayoutResource() {
                 return 0;
             }

             @Override
             protected void initializeComponents(View view) {

             }

             @Override
             protected boolean isRecycle() {
                 return false;
             }
         }
