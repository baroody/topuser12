package com.top.top12user.UI.Adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.NotificationModel;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;

import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationAdapter extends ParentRecyclerAdapter<NotificationModel> {
    public NotificationAdapter(Context context, List<NotificationModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        NotificationAdapter.NotificationViewHolder holder = new NotificationAdapter.NotificationViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final NotificationAdapter.NotificationViewHolder viewHolder = (NotificationAdapter.NotificationViewHolder) holder;
        final NotificationModel notificationModel = data.get(position);

        viewHolder.notify_text.setText(notificationModel.getMsg());
        viewHolder.date.setText(notificationModel.getDate());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               itemClickListener.onItemClick(v,position);

            }
        });
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           //     delete(notificationModel.getId());


            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    protected class NotificationViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.notify_text)
        TextView notify_text;
        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.date)
        TextView date;



        public NotificationViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    itemClickListener.onItemClick(view, getPosition());
                }
            });
        }


    }
    private void delete(int notify){

            RetroWeb.getClient().create(ServiceApi.class).deleteNotification(mSharedPrefManager.getUserData().getUser_id(),notify).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body().getStatus()==1){
                            CommonUtil.makeToast(mcontext,mcontext.getString(R.string.Done));

                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {

                    CommonUtil.handleException(mcontext,t);
                    t.printStackTrace();
                }
            });
        }


}
