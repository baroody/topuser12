package com.top.top12user.UI.Activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.MessagesAdapter;
import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.ChatModel;
import com.top.top12user.Models.GetChatResponse;
import com.top.top12user.Utils.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *created by ahmed el_sayed 1/11/2018
 */
public class ChatsActivity extends ParentActivity implements OnItemClickListener {



    LinearLayoutManager linearLayoutManager;
    @OnClick(R.id.act_back)
            void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    MessagesAdapter mAdapter;
    @BindView(R.id.chats)
    RecyclerView chats;
    ArrayList<ChatModel> chatModels = new ArrayList<>();

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ChatsActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        act_title.setText(mContext.getResources().getString(R.string.chats));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false);
        mAdapter = new MessagesAdapter(mContext,chatModels);
        chats.setLayoutManager(linearLayoutManager);
        chats.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);

            getChats(mSharedPrefManager.getUserData().getUser_id());


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_recycler;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(mContext,ChatActivity.class);
        intent.putExtra("order",chatModels.get(position).getOrder()+"");
        intent.putExtra("provider",chatModels.get(position).getUser()+"");
        startActivity(intent);
      //  ChatActivity.startActivity((AppCompatActivity)mContext,chatModels.get(position).getOrder(),chatModels.get(position).getUser());

    }

    private void getChats(int UserId){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAllChats(mLanguagePrefManager.getAppLanguage(),UserId).enqueue(new Callback<GetChatResponse>() {
            @Override
            public void onResponse(Call<GetChatResponse> call, Response<GetChatResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            CommonUtil.makeToast(mContext,getString(R.string.no_data));

                        }else {
                            mAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetChatResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();


            }
        });

    }
}
