package com.top.top12user.UI.Activities;

import android.content.Intent;
import android.widget.EditText;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Models.CommentsModel;
import com.top.top12user.Models.ForgotPassResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends ParentActivity {

    @BindView(R.id.ed_activation)
    EditText ed_activation;

    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @OnClick(R.id.btn_continue)
    void onContinueClick(){

        if (ed_activation.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.enter_phone));
        }else {
            forgot();
        }
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }



    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void forgot(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).forgot(mLanguagePrefManager.getAppLanguage(),ed_activation.getText().toString()).enqueue(new Callback<ForgotPassResponse>() {
            @Override
            public void onResponse(Call<ForgotPassResponse> call, Response<ForgotPassResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        Intent intent = new Intent(mContext,ActivationCodeActivity.class);
                        intent.putExtra("user_id",response.body().getData().getUser_id()+"");
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotPassResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }




}
