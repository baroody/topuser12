package com.top.top12user.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Activities.AboutAppActivity;

import com.top.top12user.UI.Activities.ComplaintListActivity;
import com.top.top12user.UI.Activities.MainActivity;
import com.top.top12user.UI.Activities.ProfileActivity;

import com.top.top12user.UI.Activities.ContactUsActivity;

import com.top.top12user.UI.Activities.NotificationActivity;
import com.top.top12user.UI.Activities.SettingsActivity;
import com.top.top12user.UI.Activities.SplashActivity;
import com.top.top12user.UI.Activities.TermsAndConditionsActivity;
import com.top.top12user.UI.Activities.WalletActivity;
import com.top.top12user.UI.Adapters.NavigationDrawerAdapter;
import com.top.top12user.Base.BaseFragment;
import com.top.top12user.FCM.MyFirebaseInstanceIDService;
import com.top.top12user.Listeners.DrawerListner;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.NavigationModel;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by ahmed el_sayed 1/11/2018
 */

public class NavigationFragment extends BaseFragment implements OnItemClickListener {

    @BindView(R.id.list)
    RecyclerView rvRecycle;


    @BindView(R.id.tv_distance)
    TextView tv_user_mobile;

    @BindView(R.id.riv_image)
    CircleImageView civ_user_image;


    ArrayList<NavigationModel> mNavigationModels;

    NavigationDrawerAdapter drawerAdapter;

    DrawerListner drawerListner;

    public static NavigationFragment newInstance() {
        Bundle args = new Bundle();
        NavigationFragment fragment = new NavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick(R.id.edit)
    void onEditClick() {
        startActivity(new Intent(mContext, ProfileActivity.class));
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_menu;
    }

    @Override
    protected void initializeComponents(View view) {
        setNavData();
        setMenuData();
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {

        switch (position) {
            case 0:
                startActivity(new Intent(mContext, MainActivity.class));
                break;
            case 1:
                startActivity(new Intent(mContext, WalletActivity.class));
                break;
            case 2:
                startActivity(new Intent(mContext, NotificationActivity.class));
                break;
            case 3:
                startActivity(new Intent(mContext, ContactUsActivity.class));
                break;
            case 4:
                startActivity(new Intent(mContext, SettingsActivity.class));
                break;
            case 5:
                startActivity(new Intent(mContext, AboutAppActivity.class));
                break;
            case 6:
                startActivity(new Intent(mContext, TermsAndConditionsActivity.class));
                break;
            case 7:
                startActivity(new Intent(mContext, ComplaintListActivity.class));
                break;
            case 8:
                CommonUtil.ShareApp(mContext);
                break;
            case 9:

               // Logout();
                        mSharedPrefManager.Logout();
                    //    mSharedPrefManager.setLoginStatus(false);
                        startActivity(new Intent(mContext, SplashActivity.class));

                break;


        }

        drawerListner.OpenCloseDrawer();
    }

    public void setDrawerListner(DrawerListner drawerListner) {
        this.drawerListner = drawerListner;
    }

    public void setMenuData() {
        mNavigationModels = new ArrayList<>();

        //if (mSharedPrefManager.getUserData().getType().equals("client")) {
        mNavigationModels.add(new NavigationModel(getString(R.string.main), R.mipmap.homemenu));
        mNavigationModels.add(new NavigationModel(getString(R.string.wallet), R.drawable.ic_wallet));
        mNavigationModels.add(new NavigationModel(getString(R.string.notification), R.mipmap.notificationmenu));
        mNavigationModels.add(new NavigationModel(getString(R.string.contact_us), R.mipmap.contactus));
        mNavigationModels.add(new NavigationModel(getString(R.string.settings), R.mipmap.setting));
        mNavigationModels.add(new NavigationModel(getString(R.string.about_app), R.mipmap.about));
        mNavigationModels.add(new NavigationModel(getString(R.string.conditions), R.mipmap.terms));
        mNavigationModels.add(new NavigationModel(getString(R.string.complaint), R.mipmap.terms));
        mNavigationModels.add(new NavigationModel(getString(R.string.share_app), R.mipmap.share));
        mNavigationModels.add(new NavigationModel(getString(R.string.logout), R.mipmap.logout));


        rvRecycle.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        drawerAdapter = new NavigationDrawerAdapter(mContext, mNavigationModels,
                R.layout.recycle_navigation_row_client);
        drawerAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(drawerAdapter);
    }

    public void setNavData() {
        if (mSharedPrefManager.getLoginStatus()) {
            tv_user_mobile.setText(mSharedPrefManager.getUserData().getName());
            Glide.with(mContext).load(mSharedPrefManager.getUserData().getAvatar()).
                    into(civ_user_image);
        } else {
            tv_user_mobile.setText(getString(R.string.app_name));
            Glide.with(mContext).load(R.mipmap.bguser).into(civ_user_image);
        }
    }

    private void Logout() {
        mSharedPrefManager.Logout();
        mSharedPrefManager.setLoginStatus(false);

        startActivity(new Intent(mContext, SplashActivity.class));
//        showProgressDialog(getString(R.string.please_wait));
//        RetroWeb.getClient().create(ServiceApi.class).logout(mSharedPrefManager.getUserData().getUser_id(), MyFirebaseInstanceIDService.getToken(mContext)).enqueue(new Callback<BaseResponse>() {
//            @Override
//            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
//                hideProgressDialog();
//                if (response.isSuccessful()) {
//                    if (response.body().getStatus() == 1) {
//                        mSharedPrefManager.Logout();
//                        mSharedPrefManager.setLoginStatus(false);
//                        startActivity(new Intent(mContext, SplashActivity.class));
//                    } else {
//                        CommonUtil.makeToast(mContext, response.body().getMsg());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<BaseResponse> call, Throwable t) {
//                CommonUtil.handleException(mContext, t);
//                t.printStackTrace();
//                hideProgressDialog();
//
//            }
//        });
    }


    @Override
    public void onClick(View v) {

    }
}
