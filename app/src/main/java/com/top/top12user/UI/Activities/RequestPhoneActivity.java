package com.top.top12user.UI.Activities;

import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Models.CheckCodeResponse;
import com.top.top12user.Models.CommonResponse;
import com.top.top12user.Models.LoginResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestPhoneActivity extends ParentActivity {
    @BindView(R.id.ed_activation)
    EditText ed_activation;
    @BindView(R.id.btn_activate)
    Button btn_activate;


    String shop_id;
    int product_id;
    String  user_id ;
    @Override
    protected void initializeComponents() {
        user_id = getIntent().getStringExtra("user_id");
        product_id = getIntent().getIntExtra("product_id",0);
        shop_id = getIntent().getStringExtra("shop_id");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_request_phone;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }



    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_activate)
    void onActivateClick(){
        if (ed_activation.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.activation));
        }else {
           updatePhone();
        }
    }
    private void updatePhone(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).UpdatePhone(
                mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()
                ,ed_activation.getText().toString()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mSharedPrefManager.setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ConfirmCodeActivity.class);
                        intent.putExtra("user_id",response.body().getData().getUser_id()+"");
                        intent.putExtra("shop_id",shop_id);
                        intent.putExtra("product_id", product_id);
                        intent.putExtra("mobile", ed_activation.getText().toString());
                        startActivity(intent);

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }








}
