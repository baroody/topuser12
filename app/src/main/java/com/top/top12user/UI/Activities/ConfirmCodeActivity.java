package com.top.top12user.UI.Activities;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Models.CheckCodeResponse;
import com.top.top12user.Models.CommonResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmCodeActivity extends ParentActivity {
    @BindView(R.id.ed_activation)
    EditText ed_activation;
    @BindView(R.id.btn_activate)
    Button btn_activate;
    @BindView(R.id.resend_code)
    TextView resend_code;

    @BindView(R.id.change_phone)
    Button change_phone;

    String  user_id ;
    String shop_id;
    String mobile;
   int product_id;
    @Override
    protected void initializeComponents() {
        user_id = getIntent().getStringExtra("user_id");
        product_id = getIntent().getIntExtra("product_id",0);
        shop_id = getIntent().getStringExtra("shop_id");
        mobile = getIntent().getStringExtra("mobile");

        resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendCode();
            }
        });
        change_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              finish();
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_confirm_code;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }



    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_activate)
    void onActivateClick(){
        if (ed_activation.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.activation));
        }else {
            check();
        }
    }
    private void check(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).checkCode(mLanguagePrefManager.getAppLanguage(),Integer.parseInt(user_id),
                ed_activation.getText().toString()).enqueue(new Callback<CheckCodeResponse>() {
            @Override
            public void onResponse(Call<CheckCodeResponse> call, Response<CheckCodeResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getData());
                      //  startActivity(new Intent(mContext,LoginActivity.class));


                        Intent intent = new Intent(mContext,ProductDetailsActivity.class);
                        intent.putExtra("shop_id",shop_id);
                        intent.putExtra("product_id",product_id);
                        startActivity(intent);

                        finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckCodeResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


    private void resendCode(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).resendCode("+966"+mobile).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                hideProgressDialog();
                if (response.body().getStatus()==200){
                    CommonUtil.makeToast(mContext,response.body().getMessage());
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

}
