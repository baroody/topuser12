package com.top.top12user.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.AdditionsModel;
import com.top.top12user.R;

import java.util.List;

import butterknife.BindView;

public class SpecialAdditiveAdapter extends ParentRecyclerAdapter<AdditionsModel> {

    public SpecialAdditiveAdapter(final Context context, final List<AdditionsModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        AdditionsModel specialAdditiveModel = data.get(position);
        viewHolder.tvPrice
                .setText(specialAdditiveModel.getPrice() + " " + mcontext.getResources().getString(R.string.Sar));
        viewHolder.tvSpecialAdditiveName.setText(specialAdditiveModel.getAddition_name());
        viewHolder.addition_lay.setVisibility(View.GONE);

        viewHolder.edOrderNumber.setText(specialAdditiveModel.getCount() + "");
        viewHolder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });
        viewHolder.ivMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.tv_special_additive_name)
        TextView tvSpecialAdditiveName;

        @BindView(R.id.iv_minus)
        ImageView ivMinus;

        @BindView(R.id.ed_order_number)
        EditText edOrderNumber;

        @BindView(R.id.iv_add)
        ImageView ivAdd;
        @BindView(R.id.addition_lay)
        LinearLayout addition_lay;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}

