package com.top.top12user.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.OrderProductAdditions;
import com.top.top12user.R;
import com.top.top12user.Models.OrderProductAdditions;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class FamilyOrderSpecialAdditiveAdapter1 extends ParentRecyclerAdapter<OrderProductAdditions> {

    private int mRowIndex = -1;

    public void setData(List<OrderProductAdditions> modelData) {
        if (data != modelData) {
            data = modelData;
            notifyDataSetChanged();
        }
    }

    public void setRowIndex(int index) {
        mRowIndex = index;
    }

    public FamilyOrderSpecialAdditiveAdapter1(final Context context) {
        super(context);
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext)
                .inflate(R.layout.recycler_order_additions, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        OrderProductAdditions specialAdditiveModel = data.get(position);
        viewHolder.tvAdditionPrice
                .setText(specialAdditiveModel.getAddition_price() + " " + mcontext.getResources().getString(R.string.Sar));
        viewHolder.tvAdditionName.setText(specialAdditiveModel.getAddition_name());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_price)
        TextView tvAdditionPrice;

        @BindView(R.id.tv_special_additive_name)
        TextView tvAdditionName;



        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
