package com.top.top12user.UI.Activities;

import android.content.Intent;
import android.widget.EditText;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.Utils.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPasswordActivity extends ParentActivity {

    @BindView(R.id.ed_password)
    EditText ed_password;
    @BindView(R.id.ed_confirm_password)
            EditText ed_confirm_password;

    String user_id;

    @Override
    protected void initializeComponents() {
        user_id = getIntent().getStringExtra("user_id");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_new_password;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }



    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_save)
    void onSave(){
        if (ed_password.getText().toString().equals("") ||ed_confirm_password.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.enter_password));
        }else if (!(ed_password.getText().toString().equals(ed_confirm_password.getText().toString()))){
            CommonUtil.makeToast(mContext,getString(R.string.not_match));
        }else {
            update();
        }
    }
    private void update(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).forgetPass(mLanguagePrefManager.getAppLanguage(),Integer.parseInt(user_id),ed_password.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        startActivity(new Intent(mContext,LoginActivity.class));
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }



}
