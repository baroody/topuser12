package com.top.top12user.UI.Adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.top.top12user.Models.ComplaintModel
import com.top.top12user.Models.EWalletAction

import com.top.top12user.R
import com.top.top12user.UI.Activities.ComplaintActivity
import kotlinx.android.synthetic.main.item_complaint.view.*


class ComplaintAdapter(private val mActivity: Activity, private val complaintList: ArrayList<ComplaintModel>) :
        RecyclerView.Adapter<ComplaintAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(mActivity).inflate(R.layout.item_complaint,
                    parent, false))


    override fun getItemCount(): Int = complaintList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.complaintId.text = "رقم الشكوي: " +" "+complaintList[position].complaintId.toString()
        holder.complaintStatus.text = "حالة الشكوي: " +" "+complaintList[position].status
        holder.complaintDate.text =  "تاريخ الشكوي: " +" "+complaintList[position].createdAt
        holder.complaintText.text =  "نص الشكوي: " +" "+complaintList[position].description
        holder.card.setOnClickListener {
            mActivity.startActivity(Intent(mActivity,ComplaintActivity::class.java).
            putExtra("complaint",complaintList[position]))
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val complaintId: TextView = view.complaint_id
        val complaintStatus: TextView = view.complaint_status
        val complaintDate: TextView = view.complaint_date
        val complaintText: TextView = view.complaint_text
        val card: CardView = view.card

    }
}