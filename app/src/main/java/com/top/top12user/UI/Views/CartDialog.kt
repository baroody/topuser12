package com.top.top12user.UI.Views

import android.app.Activity
import android.app.Dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.top.top12user.Listeners.AddCart

import com.top.top12user.R
import kotlinx.android.synthetic.main.dailog_cart.*
import kotlinx.android.synthetic.main.dailog_shop_close.*
import kotlinx.android.synthetic.main.dailog_shop_close.accept


class CartDialog(
        val activity: Activity

) : Dialog(activity),
        View.OnClickListener {
val addCart:AddCart=activity as AddCart
    override fun onClick(v: View) {
        dismiss()

    }


    override fun onBackPressed() {
        dismiss()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.dailog_cart)
        window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
        )
//        message_title.text =activity.getString(R.string.app_name)
//        alert_message.text =activity.getString(R.string.are_you_sure_want_to_exit)
        accept.setOnClickListener {
            addCart.addcart()
        }
        cancel.setOnClickListener {
            dismiss()
        }

        window?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.decorView?.rootView?.setOnTouchListener { p0, p1 ->
            dismiss()
            true
        }

    }
}