package com.top.top12user.UI.Adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.CategoryModel;
import com.top.top12user.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class CategoryChooseAdapter extends ParentRecyclerAdapter<CategoryModel> {



    public CategoryChooseAdapter(final Context context, final List<CategoryModel> data,final int layoutId) {
        super(context, data ,layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_category, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CategoryModel categoryModel = data.get(position);
        viewHolder.tvName.setText(categoryModel.getName());
        Glide.with(mcontext).load(categoryModel.getImage()).placeholder(R.mipmap.bguser).into(viewHolder.lay_item);


        viewHolder.lay_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int count = 0;
                if (count==0){
                    // viewHolder.lay_item.setBackgroundColor(ContextCompat.getColor(mcontext,R.color.colorRowPrimary));

                    itemClickListener.onItemClick(v, position);
                    count = 1;
                }else {

                    itemClickListener.onItemClick(v, position);
                }
                Log.e("cc",count+"");


            }
        });



//        if (position.isCheckCategory()) {
//        viewHolder.ivCheck.setVisibility(View.VISIBLE);
//        } else {
//        viewHolder.ivCheck.setVisibility(View.GONE);
//        }
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.cat_img)
        ImageView lay_item;



        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}


