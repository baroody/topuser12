package com.top.top12user.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.R;
import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.ChatModel;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessagesAdapter extends ParentRecyclerAdapter<ChatModel> {

    public MessagesAdapter(Context context, List<ChatModel> data) {
        super(context, data);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_message, parent, false);
        MessagesAdapter.ViewHolder viewHolder = new MessagesAdapter.ViewHolder(itemLayoutView);
        viewHolder.setOnItemClickListener(itemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ParentRecyclerViewHolder viewHolder, int position) {
        final MessagesAdapter.ViewHolder holder = (MessagesAdapter.ViewHolder) viewHolder;
        final ChatModel model = data.get(position);



        holder.title.setText(model.getUsername());
        //holder.subTitle.setText(model.getMessage());
        holder.time.setText(model.getSent_at());
        if (model.getType().equals("text")){
            holder.subTitle.setText(model.getMsg());
        }else if(model.getType().equals("image")){
            holder.subTitle.setText(mcontext.getResources().getString(R.string.image));
        }

        if (model.getAvatar()!=null){
            Glide.with(mcontext).load(model.getAvatar()).placeholder(R.mipmap.bguser).into(holder.image);
            //Picasso.with(mcontext).load(model.get()).into(holder.image);
        }



    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends ParentRecyclerViewHolder {
        TextView title, subTitle,time;
        ImageView more;
        CircleImageView image;
        public ViewHolder(View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.tv_msgusername);
            subTitle=itemView.findViewById(R.id.tv_msg);
            time=itemView.findViewById(R.id.tv_msgtime);
            //  more=itemView.findViewById(R.id.iv_more_message);
            image=itemView.findViewById(R.id.iv_msgimage);
        }
    }


}

