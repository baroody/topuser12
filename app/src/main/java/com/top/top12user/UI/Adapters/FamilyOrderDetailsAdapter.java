package com.top.top12user.UI.Adapters;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentRecyclerAdapter;
import com.top.top12user.Base.ParentRecyclerViewHolder;
import com.top.top12user.Models.CartProductsModel;
import com.top.top12user.R;
import com.top.top12user.UI.Activities.EditOrderItemActivity;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class FamilyOrderDetailsAdapter extends ParentRecyclerAdapter<CartProductsModel> {
    private int mRowIndex = -1;
    TextView ordersPrice;
    TextView payTotal;
    float total;
    public FamilyOrderDetailsAdapter(final Context context,TextView ordersPrice,TextView payTotal) {
        super(context);
        this.ordersPrice=ordersPrice;
        this.payTotal=payTotal;
    }
    public void setRowIndex(int index) {
        mRowIndex = index;
    }
    public void setData(List<CartProductsModel> products) {
        if (data != products) {
            data = products;
            notifyDataSetChanged();
        }
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_food_family, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        final CartProductsModel foodModel = data.get(position);


        viewHolder.tvItemPrice.setText(mcontext.getString(R.string.price)+" :" + foodModel.getTotal_product_price() + " " +mcontext.getString(R.string.rs));
        viewHolder.tvItemName.setText(foodModel.getProduct_name());
        total+=foodModel.getTotal_product_price();
        // check if the food has discount or not .

        viewHolder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               EditOrderItemActivity.startActivity((AppCompatActivity)mcontext,data.get(position));

            }
        });

        Glide.with(mcontext).load(data.get(position).getProduct_image()).placeholder(R.mipmap.bguser)
                .into(viewHolder.ivFoodImage);

        viewHolder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        itemClickListener.onItemClick(v,position);

            }
        });
        viewHolder.mFamilyOrderSpecialAdditiveAdapter.setData(data.get(position).getCart_additions()); // List of Strings
        viewHolder.mFamilyOrderSpecialAdditiveAdapter.setRowIndex(position);
        viewHolder.quantity.setText(data.get(position).getProduct_count()+"");
        viewHolder.itemDesc.setText(data.get(position).product_description+"");
//        viewHolder.quantity.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void afterTextChanged(Editable s) {}
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start,
//                                          int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start,
//                                      int before, int count) {
//                if(s.length() != 0) {
//                    total-= foodModel.getTotal_product_price();
//                  float result=  Integer.parseInt(s.toString()) *
//                          foodModel.getTotal_product_price();
//                    viewHolder.tvItemPrice.setText(mcontext.getString(R.string.price)+" :" +
//                            result +
//                            " " +mcontext.getString(R.string.rs));
//
//                    total+=result;
//                    ordersPrice.setText(mcontext.getString(R.string.price)+" :" +
//                            total+
//                            " " +mcontext.getString(R.string.rs));
//                }
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        private FamilyOrderSpecialAdditiveAdapter mFamilyOrderSpecialAdditiveAdapter;



        @BindView(R.id.tv_item_price)
        TextView tvItemPrice;

        @BindView(R.id.tv_item_name)
        TextView tvItemName;



        @BindView(R.id.tv_recycle)
        RecyclerView tvRecycle;
        @BindView(R.id.iv_edit)
        TextView iv_edit;
        @BindView(R.id.iv_delete)
        TextView iv_delete;
        @BindView(R.id.iv_food_image)
        ImageView ivFoodImage;
        @BindView(R.id.editText)
        EditText quantity;
        @BindView(R.id.tv_item_desc)
        TextView itemDesc;






        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            tvRecycle.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false));
            mFamilyOrderSpecialAdditiveAdapter = new FamilyOrderSpecialAdditiveAdapter(mcontext);
            tvRecycle.setAdapter(mFamilyOrderSpecialAdditiveAdapter);
        }

    }


}
