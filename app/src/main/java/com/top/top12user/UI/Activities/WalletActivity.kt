package com.top.top12user.UI.Activities

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.top.top12user.Base.ParentActivity
import com.top.top12user.Models.EWalletAction
import com.top.top12user.Models.WalletResponse
import com.top.top12user.Network.RetroWeb
import com.top.top12user.Network.ServiceApi
import com.top.top12user.R
import com.top.top12user.UI.Adapters.WalletAdapter
import com.top.top12user.Utils.CommonUtil
import kotlinx.android.synthetic.main.activity_wallet.*
import kotlinx.android.synthetic.main.app_bar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WalletActivity : ParentActivity() {
    val walletList: ArrayList<EWalletAction> = arrayListOf()
    var walletAdapter: WalletAdapter? = null
    var pastVisibleItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var page = 0
    var loadMore = true
    override fun initializeComponents() {
        val linearLayoutManager =
                LinearLayoutManager(this@WalletActivity,
                        LinearLayoutManager.VERTICAL, false)
        walletAdapter = WalletAdapter(this@WalletActivity, walletList)
        //    act_title.text = getString(R.string.bills)
        rv?.layoutManager = linearLayoutManager
        rv?.adapter = walletAdapter
        act_title.text = getString(R.string.wallet)
        getWallet()
        rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.childCount
                    totalItemCount = linearLayoutManager.itemCount
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                    if (loadMore) {
                        if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                            loadMore = false
                            page++
                            progressBar2.visibility = View.VISIBLE
                            getWallet()
                            //    orderAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }
        })

    }

    override fun isFullScreen(): Boolean {
        return false
    }

    override fun isEnableBack(): Boolean {
        return false
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_wallet;
    }

    override fun isRecycle(): Boolean {
        return false
    }

    override fun isEnableToolbar(): Boolean {
        return false
    }

    override fun hideInputType(): Boolean {
        return false
    }

    private fun getWallet() {
        RetroWeb.getClient().create(ServiceApi::class.java)
                .getWallet(page, mSharedPrefManager.userData.user_id).enqueue(
                        object : Callback<WalletResponse> {
                            override fun onResponse(call: Call<WalletResponse>, response: Response<WalletResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful) {
                                    if (response.body()!!.status == 200) {
                                        textView12.text =
                                                response.body()!!.wallet.toString() + " " + getString(R.string.rs)
                                        if (response.body()!!.eWalletAction.isNotEmpty()) {
                                            walletList.addAll(response.body()!!.eWalletAction)
                                            walletAdapter?.notifyDataSetChanged()
                                            progressBar2.visibility = View.GONE
                                            loadMore = true
                                        } else {
                                            // CommonUtil.makeToast(mContext, response.body()!!.msg)
                                            progressBar2.visibility = View.GONE
                                        }
                                    } else {
                                        progressBar2.visibility = View.GONE
                                    }
                                }
                            }

                            override fun onFailure(call: Call<WalletResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext, t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }
                        })
    }
}