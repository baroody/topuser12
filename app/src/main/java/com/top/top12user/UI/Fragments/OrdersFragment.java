package com.top.top12user.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.top.top12user.Base.BaseFragment;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.OrderModel;
import com.top.top12user.Models.OrderResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Activities.DeliveryPriceActivity;
import com.top.top12user.UI.Activities.ShowOrderActivity;
import com.top.top12user.UI.Adapters.NewOrderAdapter;
import com.top.top12user.Utils.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersFragment extends BaseFragment {
    @BindView(R.id.rv_recycle)
    RecyclerView orders_recycler;
    ArrayList<OrderModel> orderModels = new ArrayList<>();
    NewOrderAdapter newOrderAdapter;
    LinearLayoutManager linearLayoutManager;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;

    public static OrdersFragment newInstance() {
        Bundle args = new Bundle();
        OrdersFragment fragment = new OrdersFragment();
        fragment.setArguments(args);
        return fragment;
    }





    @Override
    public void onClick(View v) {

    }



    private void getOrder(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getOrders(mLanguagePrefManager.getAppLanguage(),
               mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, final Response<OrderResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if(response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }else {
                            newOrderAdapter.updateAll(response.body().getData());
                            newOrderAdapter.setOnItemClickListener(new OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    if ((response.body().getData().get(position).getOrder_status().equals("delegate accept")||response.body().getData().get(position).getOrder_status().equals("provider accept"))&&!(response.body().getData().get(position).getOrder_delegate_price().equals(""))){
                                        Intent intent1 = new Intent(getContext(), DeliveryPriceActivity.class);
                                        intent1.putExtra("id",response.body().getData().get(position).getOrder_id()+"");
                                        intent1.putExtra("provider",response.body().getData().get(position).getOrder_provider_id()+"");
                                        startActivity(intent1);

                                    }else {
                                        Intent intent = new Intent(getContext(), ShowOrderActivity.class);
                                        intent.putExtra("shop",response.body().getData().get(position).getShop_id()+"");
                                        intent.putExtra("provider",response.body().getData().get(position).getOrder_provider_id()+"");
                                        intent.putExtra("id", response.body().getData().get(position).getOrder_id() + "");
                                        startActivity(intent);
                                    }
                                }
                            });
                        }
                    }else {
                        CommonUtil.makeToast(getContext(),response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                CommonUtil.handleException(getContext(),t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_order;
    }

    @Override
    protected void initializeComponents(View view) {

        linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        newOrderAdapter = new NewOrderAdapter(getContext(),orderModels);
        orders_recycler.setLayoutManager(linearLayoutManager);
        orders_recycler.setAdapter(newOrderAdapter);
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrder();
            }});
        getOrder();
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }
}
