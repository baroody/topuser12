package com.top.top12user.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.top.top12user.Listeners.ChooseCategoryListner;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.CategoryModel;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.CategoryChooseAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryDialog extends Dialog {
    @BindView(R.id.recycler_category)
    RecyclerView recycler_category;
    @BindView(R.id.filter)
    Button filter;
    @BindView(R.id.clear)
    Button clear;
    Context context;
    GridLayoutManager gridLayoutManager;
    CategoryModel listModel;
    ArrayList<CategoryModel> category=new ArrayList<>();
    CategoryModel model;
    ArrayList<CategoryModel> categoryModel = new ArrayList<>();

    ArrayList<CategoryModel> listModels =new ArrayList<>();
    ArrayList<ArrayList<CategoryModel>> categories = new ArrayList<>();
    List<String > names = new ArrayList<>();
    List<String > ids = new ArrayList<>();

    CategoryChooseAdapter mCategoryChooseAdapter;
    OnItemClickListener onItemClickListener;
    ChooseCategoryListner mChooseCategoryListner;

    ArrayList<CategoryModel> mCategoryModels = new ArrayList<>();
    ArrayList<CategoryModel> userChooseCategries = new ArrayList<>();
       CategoryModel cateoryModel;
    public CategoryDialog(@NonNull Context context,OnItemClickListener onItemClickListener,ArrayList<CategoryModel> categoryModels) {
        super(context);
        this.context = context;
        this.mCategoryModels = categoryModels;
        this.onItemClickListener = onItemClickListener;

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_category);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(false);
        ButterKnife.bind(this);
        initializeComponents();
    }
    private void initializeComponents() {

//        gridLayoutManager = new GridLayoutManager(context, 5);
//        recycler_category.setLayoutManager(gridLayoutManager);
//        mCategoryChooseAdapter = new CategoryChooseAdapter(context, mCategoryModels);
//        mCategoryChooseAdapter.setOnItemClickListener(onItemClickListener);
//        recycler_category.setAdapter(mCategoryChooseAdapter);


    }

    @OnClick(R.id.clear)
    void onCloseClicked() {
        dismiss();
    }

//    @OnClick(R.id.filter)
//    void onConfirmClick() {
//        listModel =listModels.get();
//        categoryModel.add(listModel);
//        categories.add(categoryModel);
//
//        for (int i = 0;i<categories.size();i++){
//            model =new CategoryModel (categories.get(i).);
//            category.add(model);
//
//            for (int j =0;j<category.size();j++){
//
//                for (int w = 0;w<categories.size();w++) {
//
//                }
//
//            }
//
//
//        }
//        Log.e("name",category.get(category.size()-1).getListModels().get(categories.size()-1).getName()+",");
//        names.add(category.get(category.size()-1).getListModels().get(categories.size()-1).getName()+",");
//        ids.add(category.get(category.size()-1).getListModels().get(categories.size()-1).getId()+"");
//        Log.e("nnn",names+"");
//        Log.e("iii",ids+"");
//        LinkedHashSet<String> listToSet = new LinkedHashSet<String>(names);
//        List<String> listWithoutDuplicates = new ArrayList<String>(listToSet);
//        LinkedHashSet<String > listToSet1 = new LinkedHashSet<String>(ids);
//        List<String> listWithoutDuplicates1 = new ArrayList<String>(listToSet1);
//        Log.e("bbb",listWithoutDuplicates+"");
//        //String ID= ids.toString().replace("[","").replace("]","");
//        id = listWithoutDuplicates1.toString().replace("[","").replace("]","");
//        StringBuilder builder = new StringBuilder();
//        for (String value : listWithoutDuplicates) {
//            builder.append(value);
//        }
//        String text = builder.toString();
//        filterCorrectData();
//        mChooseCategoryListner.ChooseCategories(userChooseCategries);
//        dismiss();
//    }


    void filterCorrectData() {
        for (CategoryModel categoryModel : mCategoryModels) {
            if (categoryModel.isCheckCategory()) {
                userChooseCategries.add(categoryModel);
                Log.e("mmm",userChooseCategries.size()+"");
            }
        }
    }
}
