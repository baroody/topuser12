package com.top.top12user.UI.Views

import android.app.Activity
import android.app.Dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager

import com.top.top12user.R
import kotlinx.android.synthetic.main.dailog_shop_close.*


class CloseShopDialog(
        val activity: Activity

) : Dialog(activity),
        View.OnClickListener {

    override fun onClick(v: View) {
        dismiss()

    }


    override fun onBackPressed() {
        dismiss()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.dailog_shop_close)
        window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
        )
//        message_title.text =activity.getString(R.string.app_name)
//        alert_message.text =activity.getString(R.string.are_you_sure_want_to_exit)
        accept.setOnClickListener {
            dismiss()
        }


        window?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.decorView?.rootView?.setOnTouchListener { p0, p1 ->
            dismiss()
            true
        }

    }
}