package com.top.top12user.UI.Activities;

import android.content.Intent;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.top.top12user.Base.ParentActivity;
import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Models.BasketResponse;
import com.top.top12user.Models.CartPriceRsponse;
import com.top.top12user.Models.ShopProductsModel;
import com.top.top12user.Models.ShowShopModel;
import com.top.top12user.Models.ShowShopResponse;
import com.top.top12user.Network.RetroWeb;
import com.top.top12user.Network.ServiceApi;
import com.top.top12user.R;
import com.top.top12user.UI.Adapters.ProductAdapter;
import com.top.top12user.Utils.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.civ_image)
    ImageView civ_image;
    @BindView(R.id.tv_family_name)
    TextView tv_family_name;
    @BindView(R.id.tv_city)
    TextView tv_city;
    @BindView(R.id.rb_rating)
    AppCompatRatingBar rb_rating;
    @BindView(R.id.txt_count)
    TextView txt_count;
    @BindView(R.id.tv_low)
    TextView tv_low;
    @BindView(R.id.txt_rates)
    TextView txt_rates;
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.basket)
    TextView basket;
    @BindView(R.id.shop_time)
    TextView shop_time;
    @BindView(R.id.btn_basket)
    Button btn_basket;
    @BindView(R.id.product_recycler)
    RecyclerView product_recycler;
    @BindView(R.id.constrain)
    ConstraintLayout constrain;

    LinearLayoutManager gridLayoutManager;
    ProductAdapter productAdapter;
    ArrayList<ShopProductsModel> shopProductsModels = new ArrayList<>();
    ShowShopModel showShopModel;
    @OnClick(R.id.act_back)
    void onBackCkick(){
        onBackPressed();
    }
    @OnClick(R.id.basket)
    void onBasketClick(){
        startActivity(new Intent(mContext,BasketActivity.class));
    }
    @Override
    protected void initializeComponents() {

       int shop_id = getIntent().getIntExtra("shop_id",0);
       gridLayoutManager = new LinearLayoutManager(mContext);
       productAdapter = new ProductAdapter(mContext,shopProductsModels);
       product_recycler.setLayoutManager(gridLayoutManager);
       product_recycler.setAdapter(productAdapter);
       productAdapter.setOnItemClickListener(this);
     //  getPrice();
       showShop(mSharedPrefManager.getUserData().getUser_id(),shop_id);
       MyCart(mSharedPrefManager.getUserData().getUser_id());

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_shop;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.txt_rates)
    void onRateClick(){
        Intent intent = new Intent(mContext,RatesActivity.class);
        intent.putExtra("shop_id",showShopModel.getShop_id()+"");
        intent.putExtra("type","1");
        startActivity(intent);
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(mContext,ProductDetailsActivity.class);
        intent.putExtra("shop_id",showShopModel.getShop_id()+"");
        intent.putExtra("product_id",shopProductsModels.get(position).getProduct_id());
        startActivity(intent);

    }
    void SetData(ShowShopModel showShopModel){
        Glide.with(mContext).load(showShopModel.getUser_image()).placeholder(R.mipmap.bguser).into(civ_image);
        tv_family_name.setText(showShopModel.getShop_name());
        tv_low.setText(showShopModel.getShop_min_price()+getString(R.string.Sar));
        rb_rating.setRating(showShopModel.getShop_rate());
        txt_count.setText("("+showShopModel.getShop_order_count()+getString(R.string.Buy)+")");
        shop_time.setText(mContext.getResources().getString(R.string.shop_time)+showShopModel.getShop_work_at()+":"+showShopModel.getShop_work_to());
        for (int i = 0;i<showShopModel.getUser_categories().size();i++){
            String cat ="";
            tv_city.setText(cat+showShopModel.getUser_categories().get(i).getCategory_name()+",");
        }

    }
    @OnClick(R.id.btn_basket)
    void onBtnBasketClick(){
        startActivity(new Intent(mContext,BasketActivity.class));
    }
    private void showShop(int user_id,int Shop_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showShop(mLanguagePrefManager.getAppLanguage(),user_id,Shop_id).enqueue(new Callback<ShowShopResponse>() {
            @Override
            public void onResponse(Call<ShowShopResponse> call, Response<ShowShopResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        constrain.setVisibility(View.VISIBLE);
                        showShopModel = response.body().getData();
                        act_title.setText(response.body().getData().getShop_name());
                        SetData(response.body().getData());
                        if (response.body().getData().getProducts().size()==0){
                            CommonUtil.makeToast(mContext,getString(R.string.no_data));
                        }else {
                            productAdapter.updateAll(response.body().getData().getProducts());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShowShopResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getPrice(){
        RetroWeb.getClient().create(ServiceApi.class).getPrice(mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<CartPriceRsponse>() {
            @Override
            public void onResponse(Call<CartPriceRsponse> call, Response<CartPriceRsponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        btn_basket.setText(getString(R.string.show_basket)+"       "+response.body().getData().getAll_total_price()+getString(R.string.Sar));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CartPriceRsponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();



            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        basket.setText("");
        basket.setVisibility(View.GONE);
        MyCart(mSharedPrefManager.getUserData().getUser_id());
    }

    private void MyCart(int user_id){
     //  showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).myCart(mLanguagePrefManager.getAppLanguage()
                ,user_id,null).enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(Call<BasketResponse> call, final Response<BasketResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){


                        if (response.body().getData().getCarts().size()==0){

                            basket.setText("");
                            basket.setVisibility(View.GONE);
//                            btn_basket.setText(getString(R.string.show_basket)+"       "+
//                                    (response.body().getData().getCarts().get(0).getTotal_price()+
//                                            Math.round(response.body().getData().getCarts().get(0).total_delivery_price)
//                                            +getString(R.string.Sar)));
                        }else {
                            int x=0;
                            int y = 0;
                            for (int i =0;i<response.body().getData().getCarts().size();i++){
                                y =y+ response.body().getData().getCarts().get(i).getCart_products().size();
                                x = y;
                                Log.e("xx",y+"");
                            }

                            Log.e("yy",x+"");
                            basket.setText(y+"");
                            basket.setVisibility(View.VISIBLE);


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BasketResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
