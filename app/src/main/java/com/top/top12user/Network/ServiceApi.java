package com.top.top12user.Network;

import com.top.top12user.Models.AddOrderModel;
import com.top.top12user.Models.AppInfoResponse;
import com.top.top12user.Models.BaseResponse;
import com.top.top12user.Models.BasketResponse;
import com.top.top12user.Models.CartPriceRsponse;
import com.top.top12user.Models.CategoryResponse;
import com.top.top12user.Models.CheckCodeResponse;
import com.top.top12user.Models.CheckResponse;
import com.top.top12user.Models.CheckTransactionModel;
import com.top.top12user.Models.CommonResponse;
import com.top.top12user.Models.ComplaintResponse;
import com.top.top12user.Models.ConditionsResponse;
import com.top.top12user.Models.ForgotPassResponse;
import com.top.top12user.Models.GetChatResponse;
import com.top.top12user.Models.HeaderResponse;
import com.top.top12user.Models.ListResponse;
import com.top.top12user.Models.LoginResponse;
import com.top.top12user.Models.NotificationResponse;
import com.top.top12user.Models.OrderDetailsResponse;
import com.top.top12user.Models.OrderResponse;
import com.top.top12user.Models.OrdersCountInProgressResponse;
import com.top.top12user.Models.PaymentModel;
import com.top.top12user.Models.RateResponse;
import com.top.top12user.Models.RegisterResponse;
import com.top.top12user.Models.SendChatResponse;
import com.top.top12user.Models.ShopsResponse;
import com.top.top12user.Models.ShowOrderResponse;
import com.top.top12user.Models.ShowProductResponse;
import com.top.top12user.Models.ShowShopResponse;
import com.top.top12user.Models.WalletResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ServiceApi {

    @GET(Urls.Cities)
    Call<ListResponse> getCities(
            @Query("lang") String lang
    );


    @GET(Urls.Terms)
    Call<ConditionsResponse> getConditions(
            @Query("lang") String lang
    );

    @GET(Urls.About)
    Call<ConditionsResponse> getAbout(
            @Query("lang") String lang
    );

    @GET(Urls.Shops)
    Call<ShopsResponse> getShops(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );

    @GET(Urls.Foooter)
    Call<HeaderResponse> getFooter();

    @GET(Urls.Header)
    Call<HeaderResponse> getHeader();

    @POST(Urls.ShowShop)
    Call<ShowShopResponse> showShop(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id
    );

    @POST(Urls.CheckCode)
    Call<CheckCodeResponse> checkCode(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("code") String code
    );

    @POST(Urls.ResendCode)
    Call<CommonResponse> resendCode(
            @Query("phone") String phone
    );


    @POST(Urls.ClearCartProducts)
    Call<CommonResponse> clearCartProducts(
            @Query("user_id") int user_id,
            @Query("cart_id") int cart_id

    );


    @POST(Urls.ShowProduct)
    Call<ShowProductResponse> showProduct(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id
    );

    @POST(Urls.AddToCart)
    Call<BaseResponse> addToCart(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("product_count") int product_count,
            @Query("additions") String additions,
            @Query("status") String status,
            @Query("isEarly") String isEarly

    );

    @POST(Urls.MyCart)
    Call<BasketResponse> myCart(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("orderId") String orderId
    );

    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse> register(
            @Query("lang") String lang,
            @Query("type") int type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") int city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Part MultipartBody.Part avatar
    );

    @POST(Urls.Register)
    Call<RegisterResponse> registerwitoutimage(
            @Query("lang") String lang,
            @Query("type") int type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") int city_id,
            @Query("lat") String lat,
            @Query("lng") String lng

    );

    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse> registerwithoutemail(
            @Query("lang") String lang,
            @Query("type") int type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("phone") String phone,
            @Query("city_id") int city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Part MultipartBody.Part avatar
    );

    @POST(Urls.Register)
    Call<RegisterResponse> registerwithoutemailwithotimage(
            @Query("lang") String lang,
            @Query("type") int type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("phone") String phone,
            @Query("city_id") int city_id,
            @Query("lat") String lat,
            @Query("lng") String lng
    );

    @POST(Urls.Login)
    Call<LoginResponse> login(
            @Query("lang") String lang,
            @Query("phone") String phone,
            @Query("password") String password,
            @Query("device") String device,
            @Query("device_id") String device_id
    );

    @POST(Urls.Visitor)
    Call<LoginResponse> visitor(
            // @Query("lang") String lang,
            @Query("device") String device,
            @Query("device_id") String device_id,
            @Query("user_id") String user_id
    );

    @POST(Urls.Update)
    Call<LoginResponse> updateupdateLat(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("address") String address,
            @Query("description") String description,
            @Query("city_id") int city_id

    );

    @GET(Urls.Profile)
    Call<LoginResponse> getProfile(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );

    @POST(Urls.Update)
    Call<LoginResponse> UpdateProfile(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") int city_id
    );

    @POST(Urls.Update)
    Call<LoginResponse> UpdatePhone(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("phone") String phone

    );

    @Multipart
    @POST(Urls.Update)
    Call<LoginResponse> UpdateProfilewithAvatar(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") int city_id,
            @Part MultipartBody.Part avatar
    );

    @POST(Urls.RateShop)
    Call<BaseResponse> rate(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id,
            @Query("stars") int stars,
            @Query("comment") String comment
    );

    @POST(Urls.ShowComments)
    Call<RateResponse> getComments(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id
    );

    @GET(Urls.Categories)
    Call<CategoryResponse> getCategorys(
            @Query("lang") String lang
    );

    @POST(Urls.Orders)
    Call<OrderResponse> getOrders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );

    @POST(Urls.DoOrder)
    Call<AddOrderModel> doOrder(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("delivery_price") float delivery_price,
            @Query("payment_type") String payment_type,
            @Query("isWallet") Boolean isWallet


    );

    @POST(Urls.Logout)
    Call<BaseResponse> logout(
            @Query("user_id") int user_id,
            @Query("device_id") String device_id
    );

    @POST(Urls.forgot_password)
    Call<ForgotPassResponse> forgot(
            @Query("lang") String lang,
            @Query("phone") String phone
    );

    @POST(Urls.forgot_password_code)
    Call<CheckResponse> check(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("code") String code
    );

    @POST(Urls.update_password)
    Call<BaseResponse> forgetPass(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("password") String password
    );

    @GET(Urls.AppInfo)
    Call<AppInfoResponse> getAppInfo(
            @Query("lang") String lang
    );

    @POST(Urls.ShopSearch)
    Call<ShopsResponse> getShop(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("category_ids") String category_ids
    );

    @POST(Urls.Notification)
    Call<NotificationResponse> showNotification(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );

    @POST(Urls.ShowOrder)
    Call<ShowOrderResponse> getOrder(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("order_id") int order_id
    );

    @POST(Urls.OrderStatus)
    Call<BaseResponse> ChageStatus(@Query("user_id") int user_id,
                                   @Query("lang") String lang,
                                   @Query("order_id") int order_id,
                                   @Query("status") String status);

    @POST(Urls.OrderDetails)
    Call<OrderDetailsResponse> getOrderDetail(@Query("lang") String lang,
                                              @Query("user_id") int user_id,
                                              @Query("order_id") int order_id);

    @GET(Urls.CartPrice)
    Call<CartPriceRsponse> getPrice(@Query("user_id") int user_id);

    @POST(Urls.UpdateCart)
    Call<BaseResponse> updateCart(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("cart_product_id") int cart_product_id,
            @Query("count") int count,
            @Query("additions") String additions
    );

    @POST(Urls.DeleteCart)
    Call<BaseResponse> deleteCart(

            @Query("user_id") int user_id,
            @Query("cart_product_id") int cart_product_id
    );

    @POST(Urls.DeleteNotification)
    Call<BaseResponse> deleteNotification(
            @Query("user_id") int user_id,
            @Query("notify_id") int notify_id
    );

    @POST(Urls.SendChat)
    Call<SendChatResponse> sendText(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("order_id") int order_id,
            @Query("type") int type,
            @Query("msg") String msg
    );

    @Multipart
    @POST(Urls.SendChat)
    Call<SendChatResponse> sendImage(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("order_id") int order_id,
            @Query("type") int type,
            @Part MultipartBody.Part msg
    );

    @GET(Urls.GetChat)
    Call<GetChatResponse> getChat(
            @Query("lang") String lang,
            @Query("order_id") int order_id,
            @Query("user_id") int user_id
    );

    @GET(Urls.GetAllChats)
    Call<GetChatResponse> getAllChats(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );

    @GET(Urls.Charge)
    Call<PaymentModel> charge(
            @Query("amount") String amount,
            @Query("order_id") int order_id
    );

    @GET(Urls.getPaymentStatus)
    Call<CheckTransactionModel> getPaymentStatus(
            @Query("order_id") int order_id
    );

    @POST(Urls.getWallet)
    Call<WalletResponse> getWallet(
            @Query("page") int page,
            @Query("userId") int userId
    );

     @POST(Urls.getOrderComPlaint)
    Call<ComplaintResponse> getOrderComPlaint(
            @Query("page") int page,
            @Query("userId") int userId,
            @Query("language") String language

    );


    @POST(Urls.createOrderComplaint)
    Call<BaseResponse> createOrderComplaint(
          @Body CreateOrderComplaintRequest createOrderComplaintRequest

    );


    @POST(Urls.providerOrdersProssing)
    Call<OrdersCountInProgressResponse> providerOrdersProssing(
            @Body ProviderOrdersProssingRequest providerOrdersProssingRequest

    );

    @POST(Urls.completePayemntOrder)
    Call<BaseResponse> completePayment(
            @Query("order_id") int order_id,
            @Query("transaction_id") String transaction_id,
            @Query("transaction_status") String transaction_status,
            @Query("charge_id") String charge_id,
            @Query("live_mode") boolean live_mode,
            @Query("amount") double amount,
            @Query("currency") String currency
    );
}
