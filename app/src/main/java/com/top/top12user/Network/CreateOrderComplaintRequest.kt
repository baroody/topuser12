package com.top.top12user.Network

data class CreateOrderComplaintRequest(
    val description: String,
    val description_detail: String,
    val orderId: String,
    val order_complaint_details: ArrayList<OrderComplaintDetail>,
    val order_complaint_images: ArrayList<String>,
    val providerId: String,
    val userId: String
)