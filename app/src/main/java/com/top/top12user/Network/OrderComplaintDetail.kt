package com.top.top12user.Network

data class OrderComplaintDetail(
    val price: String,
    val product_id: Int
)