package com.top.top12user.Utils

import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.provider.MediaStore
import android.util.Base64
import android.widget.ImageView
import com.bumptech.glide.Glide
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.util.*


var imgPathBase64: String = ""
val imgList: ArrayList<String> = arrayListOf()


lateinit var imagePath: String

fun easyImageResult(url: String?,
                    imageView:ImageView?,image: (String) -> Unit) {
    imageView?.context?.let { Glide.with(it).load(url).into(imageView) }
    url.apply {
        this?.toSmallBase64Image(640, 480) { _, data ->
            imgPathBase64 = data
            image(data)
        }
    }
}



private fun Uri.getRealPath(context: Context): String {
    var path = ""
    var cursor: Cursor? = null
    try {
        val proj = arrayOf(MediaStore.MediaColumns.DATA)
        MediaStore.Images.Media.DATA
        cursor = context.contentResolver.query(
                this, proj, null, null,
                null
        )
        if (null != cursor && cursor.moveToFirst()) {
            val index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
            path = cursor.getString(index)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        cursor?.close()
    }
    return path
}
fun String.toSmallBase64Image(
        targetW: Int,
        targetH: Int,
        onFinish: (bitmap: Bitmap, data: String) -> Unit
) {
    val path = this
    var rotationAngle = 0
    GlobalScope.launch {
        val bmOptions = BitmapFactory.Options()
        BitmapFactory.decodeFile(path, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight
        var scaleFactor = 1
        if (targetW > 0 || targetH > 0) {
            scaleFactor = kotlin.math.min(photoW / targetW, photoH / targetH)
        }
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        val bit = BitmapFactory.decodeFile(path, bmOptions)
        val exif = ExifInterface(path)
        val orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION)
        val orientation =
                if (orientString != null)
                    Integer.parseInt(orientString)
                else ExifInterface.ORIENTATION_NORMAL

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            rotationAngle = 90
        }
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            rotationAngle = 180
        }
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            rotationAngle = 270
        }

        val matrix = Matrix()
        matrix.setRotate(
                rotationAngle.toFloat(),
                (bit.width / 2).toFloat(),
                (bit.height / 2).toFloat()
        )
        val rotatedBitmap = Bitmap.createBitmap(
                bit, 0, 0,
                bmOptions.outWidth, bmOptions.outHeight, matrix, true
        )

        val bos = ByteArrayOutputStream()

        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos)
        val data = Base64.encodeToString(bos.toByteArray(), 0)
        withContext(Dispatchers.Main) {
            onFinish(bit, data)
        }
    }
}

