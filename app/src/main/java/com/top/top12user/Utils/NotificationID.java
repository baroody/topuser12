package com.top.top12user.Utils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mahmoud on 11/6/17.
 */

public class NotificationID {

    private final static AtomicInteger c = new AtomicInteger(0);

    public static int getID() {
        return c.incrementAndGet();
    }
}
