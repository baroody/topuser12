package com.top.top12user.Utils


import android.content.Context
import pl.aprilapps.easyphotopicker.ChooserType
import pl.aprilapps.easyphotopicker.EasyImage

fun Context.chooseEasyImage ():EasyImage {
    return EasyImage.Builder(this)
        .setCopyImagesToPublicGalleryFolder(false)
        .setChooserType(ChooserType.CAMERA_AND_GALLERY)
        //.setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)
        .setFolderName("EasyImage sample")
        .allowMultiple(false)
        .build()
}


fun Context.chooseEasyImageFromCamera ():EasyImage {
    return EasyImage.Builder(this)
        .setCopyImagesToPublicGalleryFolder(false)
        .setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)
        .setFolderName("EasyImage sample")
        .allowMultiple(false)
        .build()
}