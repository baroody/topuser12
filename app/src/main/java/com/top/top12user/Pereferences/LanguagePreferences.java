package com.top.top12user.Pereferences;

import android.content.Context;
import android.content.SharedPreferences;

public class LanguagePreferences {
    private final static String SHARED_PREF_NAME = "Top12_pref";
    private final static String App_LANGUAGE = "Top12_language";

    private Context mContext;

    public LanguagePreferences(Context mContext) {
        this.mContext = mContext;
    }

    public String getAppLanguage() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        //return sharedPreferences.getString(App_LANGUAGE, Locale.getDefault().getLanguage());
        return sharedPreferences.getString(App_LANGUAGE, "ar");
    }

    public void setAppLanguage(String language) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(App_LANGUAGE, language);
        editor.apply();
    }
}
