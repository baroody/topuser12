package com.top.top12user.Pereferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.top.top12user.App.Constant;
import com.top.top12user.Models.UserModel;
import com.google.gson.Gson;

public class SharedPrefManager {
    Context mContext;

    SharedPreferences mSharedPreferences;

    SharedPreferences.Editor mEditor;

    public SharedPrefManager(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences("Top12_Preference", mContext.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
   }

    public Boolean getLoginStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.LOGIN_STATUS, false);
        return value;
    }

    public void setLoginStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.LOGIN_STATUS, status);
        mEditor.commit();
    }

    public Boolean getNotificationStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.NOTIFICATION, true);
        return value;
    }

    public void setNotificationStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.NOTIFICATION, status);
        mEditor.commit();
    }

    public int getSave_cart_shop_id() {
        int value = mSharedPreferences.getInt(Constant.SharedPrefKey.SetSaveCartShopId, 0);
        return value;
    }

    public void setSave_cart_shop_id(int status) {
        mEditor.putInt(Constant.SharedPrefKey.SetSaveCartShopId, status);
        mEditor.commit();
    }


    public void setUserData(UserModel userModel) {
        mEditor.putString(Constant.SharedPrefKey.USER, new Gson().toJson(userModel));
        mEditor.apply();
    }

    public UserModel getUserData() {
        Gson gson = new Gson();
        return gson.fromJson(mSharedPreferences.getString(Constant.SharedPrefKey.USER, null), UserModel.class);
    }


    public void Logout() {
        mEditor.clear();
        mEditor.apply();
    }
}
