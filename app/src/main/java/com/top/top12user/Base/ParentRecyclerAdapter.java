package com.top.top12user.Base;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import com.top.top12user.Pereferences.SharedPrefManager;

import com.top.top12user.Listeners.OnItemClickListener;
import com.top.top12user.Listeners.PaginationAdapterCallback;

import java.util.List;

public abstract class ParentRecyclerAdapter<Item> extends RecyclerView.Adapter<ParentRecyclerViewHolder> {
    protected Context mcontext;

    protected List<Item> data;

    protected int layoutId;
    protected RecyclerView recyclerView;
    protected BaseFragment itemClickListener1;

    protected boolean isLoadingAdded = false;

    protected boolean retryPageLoad = false;


    protected OnItemClickListener itemClickListener;

    protected PaginationAdapterCallback mPaginationAdapterCallback;

    protected SharedPrefManager mSharedPrefManager;



    public ParentRecyclerAdapter(Context context) {
        this.mcontext = context;
        mSharedPrefManager = new SharedPrefManager(context);

    }

    public ParentRecyclerAdapter(Context context, List<Item> data) {
        this.mcontext = context;
        this.data = data;
        mSharedPrefManager = new SharedPrefManager(context);

    }

    public ParentRecyclerAdapter(Context context, List<Item> data, int layoutId) {
        this.mcontext = context;
        this.data = data;
        this.layoutId = layoutId;
        mSharedPrefManager = new SharedPrefManager(context);
    }
    public ParentRecyclerAdapter(Context mcontext, List<Item> data, RecyclerView recyclerView) {
        this.mcontext = mcontext;
        this.data = data;
        this.recyclerView = recyclerView;
        mSharedPrefManager = new SharedPrefManager(mcontext);
    }


    public void setOnPaginationClickListener(PaginationAdapterCallback onPaginationClickListener) {
        this.mPaginationAdapterCallback = onPaginationClickListener;
    }




    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
    public void setOnItemClickListener1(BaseFragment itemClickListener) {
        this.itemClickListener1 = itemClickListener;
    }


    public void InsertAll(List<Item> items) {
        data.addAll(items);
        notifyDataSetChanged();
    }

    public void Insert(int position, Item item) {
        data.add(position, item);
        Log.e("Test_Test", position + "");
        notifyDataSetChanged();
    }

    public void Insert( Item item) {
        data.add(item);
        notifyDataSetChanged();
    }

    public void Delete(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    public void update(int position, Item item) {
        data.remove(position);
        data.add(position, item);
        notifyDataSetChanged();
    }

    public void updateAll(List<Item> items) {
        data.clear();
        data.addAll(items);
        notifyDataSetChanged();
    }

    public List<Item> getData() {
        return data;
    }

    public void addFooterProgress() {
        this.data.add(null);
        notifyItemInserted(data.size() - 1);
    }

    public void removeFooterProgress() {
        data.remove(data.size() - 1);
        notifyItemRemoved(data.size());
        Log.e("footer", "gone");
    }


    public void addLoadingFooter(Item item) {
        isLoadingAdded = true;
        data.add(item);
        notifyItemInserted(data.size() - 1);
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
        int position = data.size() - 1;
        data.remove(position);
        notifyItemRemoved(position);
    }


}
