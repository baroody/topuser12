package com.top.top12user.Listeners

import com.top.top12user.Models.OrderComplaintProduct

interface ItemComplaintChooser {
    fun itemComplaintChooser(orderComplaintProduct: OrderComplaintProduct,status:String)
}