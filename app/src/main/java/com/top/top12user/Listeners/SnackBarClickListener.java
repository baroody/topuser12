package com.top.top12user.Listeners;

/**
 * Created by Ramzy on 1/18/2018.
 */

public interface SnackBarClickListener {
    void onSnackBarActionClickListener();
}
