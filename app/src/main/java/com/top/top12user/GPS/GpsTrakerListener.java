package com.top.top12user.GPS;

public interface GpsTrakerListener {
    void onTrackerSuccess(Double lat, Double log);

    void onStartTracker();
}
